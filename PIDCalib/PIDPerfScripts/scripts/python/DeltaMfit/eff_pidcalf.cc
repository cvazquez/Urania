float eff_pidcal(TString write_directory, TString bincut, TString var, TString vcut, TString chain, int flag, TString fg){

  
  
  cout<<"entree eff_pidcal@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"<<endl;
  float eff=0;
   //measure sWeight efficiecny
 
    
    TString str1,str2;
   //cout<<"Bonjour"<<endl;

   //cout<<"pointeur "<<chain<<endl;
   
   TChain *cha;
 if(chain=="pionp_MU2015")cha=pionp_MU2015;
 if(chain=="pionp_MD2015")cha=pionp_MD2015;
 if(chain=="pionp_MU2016")cha=pionp_MU2016;
 if(chain=="pionp_MD2016")cha=pionp_MD2016;
 if(chain=="pionp_MU2017")cha=pionp_MU2017;
 if(chain=="pionp_MD2017")cha=pionp_MD2017;
 if(chain=="pionp_MU2018")cha=pionp_MU2018;
 if(chain=="pionp_MD2018")cha=pionp_MD2018;
 if(chain=="kaonm_MU2015")cha=kaonm_MU2015;
 if(chain=="kaonm_MD2015")cha=kaonm_MD2015;
 if(chain=="kaonm_MU2016")cha=kaonm_MU2016;
 if(chain=="kaonm_MD2016")cha=kaonm_MD2016;
 if(chain=="kaonm_MU2017")cha=kaonm_MU2017;
 if(chain=="kaonm_MD2017")cha=kaonm_MD2017;
 if(chain=="kaonm_MU2018")cha=kaonm_MU2018;
 if(chain=="kaonm_MD2018")cha=kaonm_MD2018;
 if(chain=="pionm_MU2015")cha=pionm_MU2015;
 if(chain=="pionm_MD2015")cha=pionm_MD2015;
 if(chain=="pionm_MU2016")cha=pionm_MU2016;
 if(chain=="pionm_MD2016")cha=pionm_MD2016;
 if(chain=="pionm_MU2017")cha=pionm_MU2017;
 if(chain=="pionm_MD2017")cha=pionm_MD2017;
 if(chain=="pionm_MU2018")cha=pionm_MU2018;
 if(chain=="pionm_MD2018")cha=pionm_MD2018;
 if(chain=="kaonp_MU2015")cha=kaonp_MU2015;
 if(chain=="kaonp_MD2015")cha=kaonp_MD2015;
 if(chain=="kaonp_MU2016")cha=kaonp_MU2016;
 if(chain=="kaonp_MD2016")cha=kaonp_MD2016;
 if(chain=="kaonp_MU2017")cha=kaonp_MU2017;
 if(chain=="kaonp_MD2017")cha=kaonp_MD2017;
 if(chain=="kaonp_MU2018")cha=kaonp_MU2018;
 if(chain=="kaonp_MD2018")cha=kaonp_MD2018;
 /* 
cha->SetBranchStatus("*",0);
cha->SetBranchStatus("Dst_M",1);
cha->SetBranchStatus("Dz_M",1);
 
   cha->SetBranchStatus("probe_sWeight",1);
     cha->SetBranchStatus("probe_P",1);
     cha->SetBranchStatus("probe_Brunel_MC15TuneV1_ProbNNpi",1);
 cha->SetBranchStatus("probe_Brunel_MC15TuneV1_ProbNNmu",1);
     cha->SetBranchStatus("probe_Brunel_MC15TuneV1_ProbNNk",1);
 cha->SetBranchStatus("probe_Brunel_MC15TuneV1_ProbNNp",1);
     cha->SetBranchStatus("probe_Brunel_MC15TuneV1_ProbNNe",1);
  
   cha->Draw("Dst_M-Dz_M>>hh(200,141,153)",str);
   float f1=hh->GetSum();
   str= "probe_sWeight*("+var+"&&"+vcut+")";
cha->Draw("Dst_M-Dz_M>>hh(200,141,153)",str);
   float f2=hh->GetSum();
   cout<<" PIDCalib results "<<f1<<" after "<<f2<<" and eff "<<f2/f1<<endl;
 */  
   //measure delta M efficicency
 //cout<<"histo file name"<<fg<<endl;
 
  cha->GetEntries();
  //s weight
  if(abs(flag)==0){
TString  str= "probe_sWeight*("+bincut+"&&"+var+")";
 TH1F *hh=new TH1F("hh","hh",200,141,153);
 
   cha->Draw("Dst_M-Dz_M>>hh",str);
   float f1=hh->GetSum();
   str= "probe_sWeight*("+bincut+"&&"+var+"&&"+vcut+")";
cha->Draw("Dst_M-Dz_M>>hh",str);
   float f2=hh->GetSum();
   return f2/f1;
  }
  
 if(abs(flag)==1)
  {
TFile *fdeltam= new TFile(fg,"RECREATE");
  TH1F *hh1=new TH1F("hh1","hh1",200,141,153);
  str1=bincut+"&&"+var;
  
    cha->Draw("Dst_M-Dz_M>>hh1",str1);
    
    str2=str1+"&&!("+vcut+")";
    if(flag==-1)str2=str1+"&&"+vcut;
    cout<<"cut eff "<<str2<<endl;
   
    
    TH1F *mm1=new TH1F("mm1","mm1",200,141,153);
 cha->Draw("Dst_M-Dz_M>>mm1",str2);
    hh1->Write();
    mm1->Write();
    
    fdeltam->Close();
    
  }
  if(abs(flag)==2)
  {
    //  TString out_file=write_directory+"/result.root";
    //  out_file=var+vcut+chain+".root"
    

    //cout<<"on ecrit "<<out_file<<endl;
     
     TFile *fdeltam= TFile::Open(fg);
     //cout<<"on ouvre toto "<<fg<<endl;
    
     
     TH1F* hh1 = (TH1F*)fdeltam->Get("hh1");
 hh1->Print();
    
     //cout<<"on va fitter"<<endl;
     TString ss=fg+"_before";
     float dm1=fit_dm_test(ss,hh1,write_directory);
     
       //(ss,h1,out_file);
  ss=fg+"_after";
  TH1F* mm1 = (TH1F*)fdeltam->Get("mm1");
  mm1->Print();
  
  
  float dm2=fit_deltam(ss,mm1,write_directory);
  
    //(ss,mm1,out_file);

     
     if(flag==2)    cout<<" DeltaM results "<<dm1<<" after "<<dm1-dm2<<" and eff "<<(dm1-dm2)/dm1<<endl;
    if(flag==-2)cout<<" DeltaM results "<<dm1<<" after "<<dm2<<" and eff "<<dm2/dm1<<endl;
    //cout<<" PIDCalib results "<<f1<<" after "<<f2<<" and eff "<<f2/f1<<endl;
    eff=dm2/dm1;
    
  }
  
 
  return eff;
  
 }

   
   
