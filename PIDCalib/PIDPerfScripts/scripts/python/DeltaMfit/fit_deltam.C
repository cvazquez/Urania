#include <iostream>
#include <stdlib.h>
#include <TROOT.h>
#include <TSystem.h>// brings us the gROOT object
#include <TStyle.h>// brings us the gStyle object
#include <TCanvas.h>
#include <TPaveText.h>
#include <TFile.h>
#include <TChain.h>
#include <TMinuit.h>
#include <TAxis.h>
#include <TCut.h>
#include <TLegend.h>
#include <RooFit.h>
#include <RooGlobalFunc.h>// brings us the Minos(), Save(), etc functions
#include <RooRealVar.h>
#include <RooArgList.h>
#include <RooArgSet.h> 
#include <RooGaussian.h>
#include <RooVoigtian.h>
#include <RooArgusBG.h>
#include <RooPolynomial.h>
#include <RooBreitWigner.h>
#include <RooNumConvPdf.h>
#include <RooCBShape.h>
#include <RooAbsData.h>
#include <RooDataSet.h>
#include <RooAddPdf.h>
#include <RooProdPdf.h>
#include <RooGenericPdf.h>
#include <RooExtendPdf.h>
#include <RooFitResult.h>
#include <RooPlot.h>
#include <RooRandom.h>
#include <RooCategory.h>
#include <RooSimultaneous.h>
using namespace std;

using namespace RooFit;


float fit_deltam(TString str, TH1F *mm ,TString out_dir) {

  //        gSystem->Load("libMathMore.so");
        gROOT->ProcessLine(".x lhcbStyle.C");

     

        RooRealVar x("x", "Mass", 139.5, 153);

        // RooDataSet* data = new RooDataSet("data", "data", RooArgSet(*x));
       RooDataHist* dh = new RooDataHist("dh","dh",RooArgList(x),mm);
       Long64_t Num;
       Num=mm->GetEntries();
       
       
        RooRealVar* mean = new RooRealVar("mean", "mean", 145.5, 144, 147);
        RooRealVar* sigma1 = new RooRealVar("sigma1", "sigma1", 0.5, 0.3, 0.6);
        RooRealVar* sigma2 = new RooRealVar("sigma2", "sigma2", 1, 0.8, 3.);
        //RooRealVar* sigma3 = new RooRealVar("sigma3", "sigma3", 2, 0., 10.);

        RooRealVar* m0 = new RooRealVar("m0", "m0", 139, 138, 141);
        RooRealVar* c = new RooRealVar("c", "c", 2.7, 0., 20.);
        RooRealVar* a = new RooRealVar("a", "a", 0.);
        RooRealVar* b = new RooRealVar("b", "b", 0.5, 0., 20.);
        cout<<"jusque la"<<endl;
        
        //RooGaussian* sig = new RooGaussian("sig", "Signal", x, *mean, *sigma);
        RooGaussian* sig1 = new RooGaussian("sig1", "Signal1", x, *mean, *sigma1);
        RooGaussian* sig2 = new RooGaussian("sig2", "Signal2", x, *mean, *sigma2);
        //RooGaussian* sig3 = new RooGaussian("sig3", "Signal3", x, *mean, *sigma3);
        RooRealVar* sig1frac = new RooRealVar("sig1frac", "fraction of sig1 in signal", 0.5, 0.0001, 1.0);
        //RooRealVar* sig2frac = new RooRealVar("sig2frac", "fraction of sig2 insignal", 0.36, 0.1, 1.0);
        //RooRealVar* a = new RooRealVar("a", "a", 2, 0.1, 10.);
        //RooRealVar* n = new RooRealVar("n", "n", 4, 0.10, 10.);
        //RooCBShape* sig2 = new RooCBShape("sig2", "CB", x, *mean, *sigma2, *a, *n);
        RooAddPdf* sig = new RooAddPdf("sig", "signal", RooArgList(*sig1, *sig2), *sig1frac);
        //RooAddPdf* sig = new RooAddPdf("sig", "Signal", RooArgList(*sig1, *sig2, *sig3), RooArgList(*sig1frac, *sig2frac));
        RooDstD0BG* bkg = new RooDstD0BG("bkg", "Background", x, *m0, *c, RooConst(0), RooConst(0));
        //RooDstD0BG* bkg = new RooDstD0BG("bkg", "Background", x, *m0, *c, *a*b);

        RooRealVar* nsig = new RooRealVar("nsig", "nsig", 0.5*Num, 0., Num);
        RooRealVar* nbkg = new RooRealVar("nbkg", "nbkg", 0.5*Num, 0., Num);

        RooAddPdf* model = new RooAddPdf("model", "model", RooArgList(*sig, *bkg), RooArgList(*nsig, *nbkg));

        RooPlot* frame_x = x.frame();

#if 1
        //        RooDataHist* dhist = data->binnedClone();
        RooFitResult* fitr = model->fitTo(*dh, Save(), Minos(true));
        dh->plotOn(frame_x, Name("data_fit"), MarkerSize(0.8));
#else
        RooFitResult* fitr = model->fitTo(*dh, NumCPU(4), Minos(true), Save())                                                ;
        dh->plotOn(frame_x, Name("data_fit"), MarkerSize(0.8));
#endif

        model->plotOn(frame_x, Components("sig"), FillColor(2), FillStyle(3005),VLines(), DrawOption("F"));
        model->plotOn(frame_x, Components("bkg"), LineColor(4), LineStyle(4));
        model->plotOn(frame_x, Name("data_all"), FillColor(3));

 
        RooHist* hpull_x = frame_x->pullHist();
        hpull_x->SetFillStyle(3001);
        RooPlot* pull_x = x.frame();
        pull_x->addPlotable(hpull_x,"l3");
        pull_x->SetTitle("");
        pull_x->GetYaxis()->SetLabelSize(0.20);
        pull_x->GetYaxis()->SetNdivisions(206);
        /*
        gROOT->Reset();
        TCanvas *cLog = new TCanvas("cLog","cLog");
        cLog->Divide(1,2,0,0,0);

        cLog->cd(2);
        gPad->SetLogy();
        gPad->SetTopMargin(0);
        gPad->SetLeftMargin(0.12);
        gPad->SetPad(0.03,0.02,0.97,0.77);
        frame_x->SetMinimum(0.5);
        frame_x->Draw();

        cLog->cd(1);
        gPad->SetBottomMargin(0);
        gPad->SetLeftMargin(0.12);
        gPad->SetPad(0.03,0.77,0.97,0.97);
        pull_x->Draw();

        cLog->Print("./mm_log.pdf");
        cLog->Print("./mm_log.eps");
        cLog->Print("./mm_log.png");
        */
        gROOT->Reset();

        TCanvas* cPol = new TCanvas("cPol","cPol");
        cPol->Divide(1,2,0,0,0);

        cPol->cd(2);
        gPad->SetTopMargin(0);
        gPad->SetLeftMargin(0.12);
        gPad->SetPad(0.03,0.02,0.97,0.77);
        frame_x->SetMinimum(0.5);
        frame_x->Draw() ;

        cPol->cd(1);
        gPad->SetBottomMargin(0);
        gPad->SetLeftMargin(0.12);
        gPad->SetPad(0.03,0.77,0.97,0.97);
        pull_x->Draw();

        cPol->Print(out_dir+"/"+str+".jpg");
        //      cPol->Print("./DstD0_2016.eps");
        //cPol->Print("./DstD0_2016.png");
        TString outfile=out_dir+"/"+str+"_results.root";
        
        TFile *ffit= TFile::Open(outfile,"UPDATE");
        
        fitr->Print();
 fitr->Write();

        ffit->Close();
        /*
#if 0
        cout <<"+++++++++++++++++++++++++++++++++++++++" << endl;
        cout << "chiSquare_NDOF(11)=" << frame_x -> chiSquare("data_all","data_fit",11) << endl;
        cout <<"+++++++++++++++++++++++++++++++++++++++" << endl;
#endif

        RooAbsReal* intBkg = bkg->createIntegral(x, NormSet(x));
        RooAbsReal* intSig = sig->createIntegral(x, NormSet(x));
        double Ib = intBkg->getVal();
        double Is = intSig->getVal();
        x.setRange("sign", 143, 148);
        intBkg = bkg->createIntegral(x, NormSet(x), Range("sign"));
        intSig = sig->createIntegral(x, NormSet(x), Range("sign"));
        double Ib1 = intBkg->getVal();
        double Is1 = intSig->getVal();

        cout << "Ib =" << Ib << endl;
        cout << "Ib1=" << Ib1 << endl;
        cout << "Is =" << Is << endl;
        cout << "Is1=" << Is1 << endl;

        return nsig;
        
 */
        return nsig->getVal();
        
}
