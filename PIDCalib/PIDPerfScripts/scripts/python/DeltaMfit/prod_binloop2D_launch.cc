void prod_binloop2D_launch(TString sample,TString histo_file,TString histo_name,TString write_directory,TString PIDcut,TString general_cut,TString flag_companion,TString work_directory,TString VariableX,TString VariableY,TString sw){
  //gROOT->ProcessLine(".L pidcalib.cc");
  //gROOT->ProcessLine(".L fit_dm_test.C");
  //gROOT->ProcessLine(".L fit_deltam.C");
  //gROOT->ProcessLine(".L eff_pidcal.cc");
  //crate a bin loop from an histo 1,2 or 3D
  //open file
  //  TFile *fbegin= TFile::Open("/afs/cern.ch/user/d/dhill/public/forGuy/PerfHists_K_Turbo15_MagUp_P_ETA_nTracks.root");
  TFile *fbegin= TFile::Open(histo_file);
  // initialize output directory
  //  TString work_directory="/eos/lhcb/wg/semileptonic/PIDCalib";
  
  //initialize output bin file
  TString outff=work_directory+"/results_"+sample+".root";
  
  TFile *fresult = TFile::Open(outff,"RECREATE");
  TFile *fdeltam, *ffit;
  
  //get histo from file
 Int_t ndim,nbinx,nbiny,nbinz;
 float lower_edgex,upper_edgex,lower_edgey,upper_edgey,lower_edgez,upper_edgez;
 //f.ls;
 //cout<<"file opened"<<endl;
 TH2F *h1;
 //THnD *h1;
 
  fbegin->GetObject(histo_name, h1);
 //f->GetObject("h1", h1);
  TH2F *hresults=(TH2F*)h1->Clone("hresults");
  hresults->Write();
 
  fresult->Close();
  
//get histo dimension
 ndim=h1->GetDimension();
 //cout<<"This histo is of dimension "<<ndim<<endl;
 h1->Print();
 
  //get bins from histo
  nbinx= h1->GetNbinsX();
 nbiny= h1->GetNbinsY();
 nbinz=1;
 
 //cout<<"this histo has "<<nbinx<<" binx,"<<nbiny<<" biny,"<<nbinz<<" binz"<<endl;
 //nbinz=6;
 // nbinx=1;
 //nbiny=1;
 //nbinz=4;
 
  //loop over bins
 for (int i=1;i<nbinx+1;i++)
 {for (int j=1;j<nbiny+1;j++)
 {for (int k=1;k<nbinz+1;k++)
   {//change the binnig for ntracks
    
     
     
     
  //get lower edge of the bin
   lower_edgex=h1->GetXaxis()->GetBinLowEdge(i);
    lower_edgey=h1->GetYaxis()->GetBinLowEdge(j);
    //     lower_edgez=h1->GetZaxis()->GetBinLowEdge(k);
  //get upper edge of the bin
  upper_edgex=lower_edgex+h1->GetXaxis()->GetBinWidth(i) ;
  upper_edgey=lower_edgey+h1->GetYaxis()->GetBinWidth(j) ;
  //  upper_edgez=lower_edgez+h1->GetZaxis()->GetBinWidth(k);
  //cout <<"bin number "<<i<<" "<<j<<" "<<k<<endl;
  TString comp;
  if (sample.Contains("pion")) comp="FriendTree.probe_Brunel_MC15TuneV1_ProbNNk>.6&&kmu_M>1750.";
  
  if (sample.Contains("kaon")) comp ="FriendTree.probe_Brunel_MC15TuneV1_ProbNNpi>.6&&kmupi_M>1750.";
  if(general_cut!="")comp=comp+"&&"+general_cut;
  
    
 if(flag_companion=="0")comp=general_cut;
 

 //create bincut
TString bincutx=VariableX+">"+to_string(lower_edgex)+"&&"+VariableX+"<"+to_string(upper_edgex);
TString bincuty=VariableY+">"+to_string(lower_edgey)+"&&"+VariableY+"<"+to_string(upper_edgey);
//TString bincutz=VariableZ+">"+to_string(lower_edgez)+"&&"+VariableY+"<"+to_string(upper_edgez);
 TString bincut=bincutx+"&&"+bincuty;
 //cout<< "coupure bin "<<i<<j<<k<<" "<<bincut<<endl;
 //call deltaM fit for that bin 
//call batch submission with condor for each bin
//prepare input file for condor submission
//rename the file with ij k
 TString ffd="bin_"+to_string(i)+"_"+to_string(j)+"_"+to_string(k)+".txt";
  TString ffsub="sub_"+to_string(i)+"_"+to_string(j)+"_"+to_string(k);
  TString binlop="binloop"+to_string(i)+"_"+to_string(j)+"_"+to_string(k);
 std::ofstream out(ffd);
 out << to_string(i)<<endl;
 out << to_string(j)<<endl;
 out << to_string(k)<<endl;
 out <<bincut<<endl;
 out<<comp<<endl;
 out<<PIDcut<<endl;
 
    out.close();
 //cout<<"fin rempliassage bin.txt"<<endl;
 // gROOT->ProcessLine(".!condor_submit submit.sub");
 //gROOT->ProcessLine(".!cat bin.txt");
//test relecture

 TString xc=".!./launch_condor.sh "+ffd+" "+ffsub+" "+binlop+" "+sample+" "+write_directory+" "+work_directory+" "+sw;
 //cout<<"launching condor.sh "<<xc<<endl;
 
gROOT->ProcessLine(xc);

 
 
 }//end k
 }//endj 
 }//end i
 
//cout<<"fini les amis"<<endl;
}
