
void collect(TString outfile, TString out_histoname, TString histofile, TString histoname, TString dim)
{ //open file
  TFile *fbegin= TFile::Open(histofile);
  //initialize output histo file

  TFile *fresult = TFile::Open(outfile,"RECREATE");

  
  //get histo from file
 

 cout<<"file opened"<<endl;
 if(dim=="1") 
 { TH1F *h1;
   int ndim=1;
   
 fbegin->GetObject(histoname, h1);
TH1F *out_histo=(TH1F*)h1->Clone("out_histo");
cout<<"This histo is of dimension "<<ndim<<endl;
 
 
  //get bins from histo
 int nbiny=1;
 int nbinz=1;
 
 int nbinx= out_histo->GetNbinsX();
 if(ndim>1)nbiny= out_histo->GetNbinsY();
 if(ndim>2)nbinz=out_histo->GetNbinsZ();

 cout<<"this histo has "<<nbinx<<" binx,"<<nbiny<<" biny,"<<nbinz<<" binz"<<endl;
 
 
  //loop over bins
 for (int i=1;i<nbinx+1;i++)
 {for (int j=1;j<nbiny+1;j++)
 {for (int k=1;k<nbinz+1;k++)
 {
   //read resol files
   TString ffd="resol_"+to_string(i)+"_"+to_string(j)+"_"+to_string(k)+".txt";
 std::ifstream in(ffd);
 TString line;
 
 in >> line;
 cout<<" bin "<<line;
 
 
 // if(stoi(line)!=i)cout<<"error bin i "<<i<<" "<<stoi(line)<<endl;
 in >> line;
 cout<<"-"<<line;
 
  // if(stoi(line)!=j)cout<<"error bin j "<<j<<" "<<stoi(line)<<endl;
  in >> line;
  cout<<"-"<<line;
  in >>line;
  in>>line;
  
  //  if(stoi(line)!=k)cout<<"error bin k "<<k<<" "<<stoi(line)<<endl;
  float eff;
  in>>eff;
  cout<<" resultat "<<eff<<endl;
  
  out_histo->SetBinContent(i,j,k,eff);
}//end k
 }//endj 
 }//end i

 out_histo->Write();
 }
 
 if(dim=="2"){
TH2F *h1;
 int ndim=2;
 
 fbegin->GetObject(histoname, h1);
TH2F *out_histo=(TH2F*)h1->Clone("out_histo");
cout<<"This histo is of dimension "<<ndim<<endl;
 
 
  //get bins from histo
 int nbiny=1;
 int nbinz=1;
 
 int nbinx= out_histo->GetNbinsX();
 if(ndim>1)nbiny= out_histo->GetNbinsY();
 if(ndim>2)nbinz=out_histo->GetNbinsZ();

 cout<<"this histo has "<<nbinx<<" binx,"<<nbiny<<" biny,"<<nbinz<<" binz"<<endl;
 
 
  //loop over bins
 for (int i=1;i<nbinx+1;i++)
 {for (int j=1;j<nbiny+1;j++)
 {for (int k=1;k<nbinz+1;k++)
 {
   //read resol files
   string ffd="resol_"+to_string(i)+"_"+to_string(j)+"_"+to_string(k)+".txt";
cout<<"file opened "<<ffd<<endl;
 std::ifstream in(ffd);
 string line;
 
 
 in >> line;
 cout<<" bin "<<line;
 
 
 // if(stoi(line)!=i)cout<<"error bin i "<<i<<" "<<stoi(line)<<endl;
 in >> line;
 cout<<"-"<<line;
 
  // if(stoi(line)!=j)cout<<"error bin j "<<j<<" "<<stoi(line)<<endl;
  in >> line;
  cout<<"-"<<line;
  in >>line; 
  // cout<<line<<endl;
  
   in >>line;
   // cout<<line<<endl;
   
  //  if(stoi(line)!=k)cout<<"error bin k "<<k<<" "<<stoi(line)<<endl;
  float eff;
  in>>eff;
  cout<<" resultat "<<eff<<endl;
  
  out_histo->SetBinContent(i,j,k,eff);
}//end k
 }//endj 
 }//end i

 out_histo->Write();
 }
 
 if(dim=="3")
 {
   int ndim=3;
   
TH3F *h1;
 fbegin->GetObject(histoname, h1);
  TH3F *out_histo=(TH3F*)h1->Clone("out_histo");
cout<<"This histo is of dimension "<<ndim<<endl;
 
 
  //get bins from histo
 int nbiny=1;
 int nbinz=1;
 
 int nbinx= out_histo->GetNbinsX();
 if(ndim>1)nbiny= out_histo->GetNbinsY();
 if(ndim>2)nbinz=out_histo->GetNbinsZ();

 cout<<"this histo has "<<nbinx<<" binx,"<<nbiny<<" biny,"<<nbinz<<" binz"<<endl;
 
 
  //loop over bins
 for (int i=1;i<nbinx+1;i++)
 {for (int j=1;j<nbiny+1;j++)
 {for (int k=1;k<nbinz+1;k++)
 {
   //read resol files
   TString ffd="resol_"+to_string(i)+"_"+to_string(j)+"_"+to_string(k)+".txt";
 std::ifstream in(ffd);
 TString line;
 
 in >> line;
 cout<<" bin "<<line;
 
 
 // if(stoi(line)!=i)cout<<"error bin i "<<i<<" "<<stoi(line)<<endl;
 in >> line;
 cout<<"-"<<line;
 
  // if(stoi(line)!=j)cout<<"error bin j "<<j<<" "<<stoi(line)<<endl;
  in >> line;
  cout<<"-"<<line;
  in >>line;
  in>>line;
  
  //  if(stoi(line)!=k)cout<<"error bin k "<<k<<" "<<stoi(line)<<endl;
  float eff;
  in>>eff;
  cout<<" resultat "<<eff<<endl;
  
  out_histo->SetBinContent(i,j,k,eff);
}//end k
 }//endj 
 }//end i

 out_histo->Write();
 }
 

 
 
 //f->GetObject("h1", h1);
  
 
fresult->Close();
 
cout<<"Everything done"<<endl;
}

 
