# The PIDCalib Binning Optimizer

The PIDCalib Binning Optimizer helps the user to choose a binning scheme
that accurately captures changes in the PID efficiency for a given cut in bins
of the PID binning variables.

An algorithm developed by the R(K*0) analysis was ported to python and PIDCalib to 
perform this task.

## Principle of the binning  algorithm

The binning algorithm determines binning schemes for the different binning variables independently. A trade-off between two main requirements to the binning scheme has to be made:

* Constant efficiency in each bin
* Sufficient number of events per bin

Therefore the binning algorithm work in the following way:

* Start with iso-populated or constant bin width binning scheme
* Merge adjacent bins from left to right, with a difference in efficiency < n standard deviations
* Iterate merging as long all differences between adjacent bins > n standard deviations

Additionaly ranges can be chosen where everything is merged by default to one bin. And the merging can be based on the requirement of a given absolute difference in the efficiencies or a combination of the absolute differences and standard deviations. More options are listed below.

## Calling the PID binning optimizer

The binning optimizer can be called by the following command:

    python  binningPID.py config.yml

Here, `config.yml` is a configuration file with all the needed options for the binning optimization.

## Options 

There are several options for the script, which are listed below with an example value.

`sampleVersion: "21"`: Sets the stripping version for Run I data, or the Turbo WGP production version for Run II.

`magnetPolarity: "MagDown"`: Sets the magnet polarity.

`particleName: "K"`: Sets the particle type.

`priorCut: "HasRich&&PT>250"`: Sets the list of cuts to apply to the calibration sample(s) prior to determine the PID efficiencies
#It is up to the user to ensure that their reference sample hast the same cuts applied.

`pidCut: "DLLK > -5 && MC12TuneV2_ProbNNK * (1 - MC12TuneV2_ProbNNp) > 0.05"`: Sets the PID cut.

`varName: "P"`: Sets the name of the binning variable.

`outputFile: "output/K/binning_P_K_2012_MD.py"`: Save the binning scheme file to the given directory. Default: current directory.

`minimum: 0`: Sets the minimum of the binning variable for the resulting binning scheme.

`maximum: 200000`: Sets the maximum of the binning variable for the resulting binning scheme.

`minimumBinWidth: 500`: Set the minimum bin witdth. Bins with a width below this are merged automatically. Possible values are `P:500`, `ETA:0.01`, `nTracks:1`, `nSPDHits:1`.

`delta: 1`: Set the minimum absolute difference of the pid efficiency where the two adjacent bins are merged. If left empty, the merging is performed only based on nSigma.

`nSigma: 5`: Set the minimum difference in units of standard deviations of the pid efficiency where the two adjacent bins are merged. If left empty, the merging is performed only based on delta.

`schemeName: "mybinning"`: Sets the name of the binning scheme.

`numberOfInitialBins: 100`: Sets the number of inital bins.

`startWithIsopopulatedBins: True`: Sets whether to start with iso-populated bins (`True`) or same-sized bins (`False`).

`minRun:` Sets the minimum run number to process (if applicable, Run 1 only).

`maxRun:` Sets the maximum run number to process (if applicable, Run 1 only).

`maxFiles: 5` #Sets the maximum number of calibration files to run over.

`mergeBelow: -1000000000`: Set boundary to merge everything below given value of the binning variable independent
from pid efficiency.

`mergeAbove: 1000000000`: Set boundary to merge everything above given value of the binning variable independent from pid efficiency.


