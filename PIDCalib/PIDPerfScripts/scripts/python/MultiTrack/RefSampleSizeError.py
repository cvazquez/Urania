#!/bin/env python
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function
from __future__ import division

from past.utils import old_div
from PIDPerfScripts.StartScreen import *
from PIDPerfScripts.Definitions import *

import ROOT
import sys
import argparse
import warnings
import time
import math
import os.path
from array import array

import numpy as np
import root_numpy


class ShowArgumentsParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n\n' % message)
        parser.print_usage(sys.stderr)
        sys.stderr.write('\n' + self.description)
        sys.exit(2)


if '__main__' == __name__:

    start()
    print("")

    parser = ShowArgumentsParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        prog=os.path.basename(sys.argv[0]),
        description=
        """Estimate the statistical uncertainty due to the size of the reference sample for a given:
        a) Output file from PerformMultiTrackCalib.py <multiTrackFileName>
        b) Number of bootstrap samples to create <nSamples>

The results will be stored in the ROOT file <resultsFileName>.

For a full list of arguments, do: 'python {0} -h'

e.g. python {0} \"/output/from/PerformMultiTrackCalib.root\" \"nSamples\" \"outputFileName.root\"


""".format(os.path.basename(sys.argv[0])))

    ## add the positional arguments
    parser.add_argument(
        'multiTrackFileName',
        metavar='<multiTrackFileName>',
        help='File containing the output of running PerformMultiTrackCalib.py')
    parser.add_argument(
        'nSamples',
        metavar='<nSamples>',
        type=int,
        default=100,
        help='Number of bootstrap samples to loop through')
    parser.add_argument(
        'resultsFileName',
        metavar='<resultsFileName>',
        help='Name of the output file')

    ## add the optional arguments
    parser.add_argument(
        '-t',
        '--inputTree',
        dest='inputTreeName',
        metavar='<multiTrackTreeName>',
        default='CalibTool_PIDCalibTree',
        help='The name of the TTree in <multiTrackFileName>')
    parser.add_argument(
        "-q",
        "--quiet",
        dest="verbose",
        action="store_false",
        default=True,
        help="suppresses the printing of verbose information")
    parser.add_argument(
        "-d",
        "--debug",
        dest="debug",
        action="store_true",
        default=False,
        help="print out some extra info")

    opts = parser.parse_args()

    if opts.verbose:
        print('====================================')
        print('Retrieving output of MultiTrack tool')
        print('from file: ', opts.multiTrackFileName)
        print('from TTree: ', opts.inputTreeName)
        print('====================================\n')

    #===========================================================================
    # Start the timer
    #===========================================================================
    start = time.time()
    print(time.asctime(time.localtime()))

    #===========================================================================
    # Retrieve the output from PerformMultiTrackCalib.py
    #===========================================================================
    inputFile = ROOT.TFile.Open(opts.multiTrackFileName)
    if not inputFile:
        raise IOError('Failed to open the file {}'.format(
            opts.multiTrackFileName))

    inputTree = inputFile.Get(opts.inputTreeName)
    if not inputTree:
        raise ValueError('Failed to get the TTree {} from file {}'.format(
            opts.inputTreeName, opts.multiTrackFileName))
    nEntries = inputTree.GetEntries()
    print('The tree {} has {} entries\n'.format(opts.inputTreeName, nEntries))

    #===========================================================================
    # Extract the per-event efficiencies from the TTree into a numpy array
    #===========================================================================
    eventEffs = root_numpy.tree2array(inputTree, branches='Event_PIDCalibEff')
    invalidEvents = np.logical_or(eventEffs < 0.0, eventEffs > 1.0)
    nInvalidEvents = invalidEvents.sum()
    if nInvalidEvents:
        print('There are {} events with unphysical efficiencies!'.format(
            nInvalidEvents))
        print(
            'This tool will ignore these events; You may want to check the binning used'
        )
    eventEffs = eventEffs[np.logical_not(invalidEvents)]
    nValidEvents = eventEffs.size

    #===========================================================================
    # Close the input file
    #===========================================================================
    del inputTree
    inputFile.Close()

    #===========================================================================
    # Loop through the requested number of bootstrapped samples
    #===========================================================================
    avgEffs = []
    for iSample in range(opts.nSamples):
        if opts.verbose:
            if iSample % (old_div(opts.nSamples, 20)) == 0:
                print(
                    'Looped through {} out of {} bootstrapped samples'.format(
                        iSample, opts.nSamples))

        bootstrappedSample = np.random.choice(
            eventEffs, size=nValidEvents, replace=True)
        avgEffs.append(bootstrappedSample.mean())

    avgEffs = np.array(avgEffs, dtype=np.float32)
    if opts.debug:
        print(avgEffs.size, type(avgEffs))
        print("The first ten smeared average event efficiencies are:")
        print(avgEffs[:10])
    print("The mean of all histograms is {:0.6}".format(avgEffs.mean()))

    #===========================================================================
    # Save things to the output file
    #===========================================================================
    print("Storing results in file: {0}".format(opts.resultsFileName))

    resultsFile = ROOT.TFile(opts.resultsFileName, "recreate")
    resultsTree = ROOT.TTree("Tree_RefSampleError", "Tree_RefSampleError")
    bstrapEff = array("d", [0])
    resultsTree.Branch("bstrapEff", bstrapEff, "bstrapEff/D")

    # Loop over all bootstrapped average efficiencies
    for eff in avgEffs:
        bstrapEff[0] = eff
        resultsTree.Fill()
    resultsTree.Write()
    print("TTree filled!")

    # Draw a histogram and fit a Gaussian to it
    print("Fitting a Gaussian to the results")
    c1 = ROOT.TCanvas('c1', 'c1', 800, 600)
    resultsTree.Draw("bstrapEff>>hist")
    hist = ROOT.gDirectory.Get("hist")
    hist.Fit("gaus")
    plotName = opts.resultsFileName.replace('.root', '.png')
    c1.SaveAs(plotName)

    print("File saved!")

    end = time.time()
    minutes, seconds = divmod(end - start, 60)
    print(time.asctime(time.localtime()))
    print("Time taken: {:0.0f} minutes and {:0.2f} seconds".format(
        int(minutes), seconds))
