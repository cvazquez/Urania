#!/bin/env python
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function
from __future__ import division

from past.utils import old_div
from PIDPerfScripts.StartScreen import *

import ROOT
import sys
import argparse
import warnings
import time
import math
import os.path
from array import array

import numpy as np


class ShowArgumentsParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n\n' % message)
        parser.print_usage(sys.stderr)
        sys.stderr.write('\n' + self.description)
        sys.exit(2)


if '__main__' == __name__:

    start()
    print("")

    parser = ShowArgumentsParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        prog=os.path.basename(sys.argv[0]),
        description=
        """Estimate the statistical uncertainty due to the size of the calibration sample for a given:
        a) Output file from PerformMultiTrackCalib.py <multiTrackFileName>
        b) Number of smeared histograms to create <nhists>

The results will be stored in the ROOT file <resultsFileName>.

For a full list of arguments, do: 'python {0} -h'

e.g. python {0} \"/output/from/PerformMultiTrackCalib.root\" \"nhists\" \"outputFileName.root\"


""".format(os.path.basename(sys.argv[0])))

    ## add the positional arguments
    parser.add_argument(
        'multiTrackFileName',
        metavar='<multiTrackFileName>',
        help='File containing the output of running PerformMultiTrackCalib.py')
    parser.add_argument(
        'nhists',
        metavar='<nhists>',
        type=int,
        default=100,
        help='Number of smeared histograms to loop through')
    parser.add_argument(
        'resultsFileName',
        metavar='<resultsFileName>',
        help='Name of the output file')

    ## add the optional arguments
    parser.add_argument(
        '-t',
        '--inputTree',
        dest='inputTreeName',
        metavar='<multiTrackTreeName>',
        default='CalibTool_PIDCalibTree',
        help='The name of the TTree in <multiTrackFileName>')
    parser.add_argument(
        "-q",
        "--quiet",
        dest="verbose",
        action="store_false",
        default=True,
        help="suppresses the printing of verbose information")
    parser.add_argument(
        "-d",
        "--debug",
        dest="debug",
        action="store_true",
        default=False,
        help="print out some extra info")

    opts = parser.parse_args()

    if opts.verbose:
        print('====================================')
        print('Retrieving output of MultiTrack tool')
        print('from file: ', opts.multiTrackFileName)
        print('from TTree: ', opts.inputTreeName)
        print('====================================\n')

    #===========================================================================
    # Start the timer
    #===========================================================================
    start = time.time()
    print(time.asctime(time.localtime()))

    #===========================================================================
    # Retrieve the output from PerformMultiTrackCalib.py
    #===========================================================================
    inputFile = ROOT.TFile.Open(opts.multiTrackFileName)
    if not inputFile:
        raise IOError('Failed to open the file {}'.format(
            opts.multiTrackFileName))

    inputTree = inputFile.Get(opts.inputTreeName)
    if not inputTree:
        raise ValueError('Failed to get the TTree {} from file {}'.format(
            opts.inputTreeName, opts.multiTrackFileName))
    if opts.debug:
        print('The tree has {} entries\n'.format(inputTree.GetEntries()))

    #===========================================================================
    # Extract the name of the different tracks in the sample
    #===========================================================================
    print('Extracting track names')
    allVarsList = [var.GetName() for var in inputTree.GetListOfLeaves()]
    if opts.debug:
        print(type(allVarsList[0]), allVarsList)

    trackList = set(var.split('_')[0] for var in allVarsList)
    trackList.remove('Event')
    trackList = list(trackList)
    print('Found the following tracks in TTree {}:'.format(opts.inputTreeName))
    print('(', ', '.join(trk for trk in trackList), ')\n')

    #===========================================================================
    # Set up the necessary branch addresses
    #===========================================================================
    print('Loading the required branches from the TTree')
    trackBinNumbers = {}
    trackPIDEffs = {}
    trackPIDErrors = {}
    for trk in trackList:
        trackBinNumbers[trk] = array('i', [0])
        inputTree.SetBranchAddress(trk + '_PIDCalibBinNumber',
                                   trackBinNumbers[trk])

        trackPIDEffs[trk] = array('f', [0])
        inputTree.SetBranchAddress(trk + '_PIDCalibEff', trackPIDEffs[trk])

        trackPIDErrors[trk] = array('f', [0])
        inputTree.SetBranchAddress(trk + '_PIDCalibEffError',
                                   trackPIDErrors[trk])
    if opts.debug:
        print('Check if values using addresses and GetLeaf().GetValue() match')
        trk = trackList[0]
        for i in (5, 17, 51):
            inputTree.GetEntry(i)
            leafVal = inputTree.GetLeaf(trk + '_PIDCalibEff').GetValue()
            print('Entry number', i)
            print('Value from GetLeaf(): ', leafVal)
            print('Value from address: ', trackPIDEffs[trk][0])
        print('')

    #===========================================================================
    # Loop through the events in inputTree.
    # For each track, we want the set of bin numbers used
    #===========================================================================
    nEntries = inputTree.GetEntries()
    allBinNumbers = {trk: set() for trk in trackList}
    smearedValues = {trk: {} for trk in trackList}
    avgEffs = np.zeros(
        opts.nhists, dtype=np.float64)  #Store the smeared average efficiencies
    for i in range(nEntries):

        if i % (old_div(nEntries, 20)) == 0:
            print('Looped through {} out of {} events'.format(i, nEntries))
            #duration = time.time()
            #minutes, seconds = divmod(duration-start, 60)
            #print "Time taken: {:0.0f} minutes and {:0.2f} seconds".format(int(minutes), seconds)

        inputTree.GetEntry(i)
        tempVals = np.ones(opts.nhists, dtype=np.float64)

        for trk in trackList:

            binNumber = trackBinNumbers[trk][0]

            if trackBinNumbers[trk][0] not in allBinNumbers[trk]:
                trkEff = trackPIDEffs[trk][0]
                trkError = trackPIDErrors[trk][0]
                if trkEff == 0 or trkError == 0:
                    print(trk, binNumber)
                allBinNumbers[trk].add(binNumber)
                smearedValues[trk][binNumber] = np.random.normal(
                    trkEff, trkError, opts.nhists)

            tempVals *= smearedValues[trk][binNumber]
        #end track loop
        avgEffs += tempVals

    avgEffs = old_div(avgEffs, nEntries)
    if opts.debug:
        print("The first ten smeared average event efficiencies are:")
        print(avgEffs[:10])
    print("The mean of all histograms is {:0.6}".format(avgEffs.mean()))

    #===========================================================================
    # Save things to the output file
    #===========================================================================
    print("Storing results in file: {0}".format(opts.resultsFileName))

    resultsFile = ROOT.TFile(opts.resultsFileName, "recreate")
    resultsTree = ROOT.TTree("Tree_CalibStatErr", "Tree_CalibStatError")
    smearedEff = array("d", [0])
    resultsTree.Branch("smearedEff", smearedEff, "smearedEff/D")

    # Loop over all smeared average efficiencies
    for eff in avgEffs:
        smearedEff[0] = eff
        resultsTree.Fill()
    resultsTree.Write()
    print("TTree filled!")

    # Draw a histogram and fit a Gaussian to it
    print("Fitting a Gaussian to the results")
    c1 = ROOT.TCanvas('c1', 'c1', 800, 600)
    resultsTree.Draw("smearedEff>>hist")
    hist = ROOT.gDirectory.Get("hist")
    hist.Fit("gaus")
    plotName = opts.resultsFileName.replace('.root', '.png')
    c1.SaveAs(plotName)

    print("File saved!")

    end = time.time()
    minutes, seconds = divmod(end - start, 60)
    print(time.asctime(time.localtime()))
    print("Time taken: {:0.0f} minutes and {:0.2f} seconds".format(
        int(minutes), seconds))
