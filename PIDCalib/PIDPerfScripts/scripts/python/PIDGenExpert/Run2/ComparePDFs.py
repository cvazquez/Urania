###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function
from __future__ import absolute_import

import os, sys
from .PlotPIDComparison import plotComparison

from .Config import *

config = None
if len(sys.argv) > 1:
    configname = sys.argv[1]
else:
    print("Usage: ComparePDFs.py [config] [datasets]")
    print("  configs are: ")
    for i in sorted(configs().keys()):
        print("    ", i)
    sys.exit(0)

config = configs()[configname]
bins = config["bins"]
syst = 1
stat = 10
if 'syst' in config: syst = config['syst']
if 'stat' in config: stat = config['stat']
limits = None
if 'limits' in config: limits = config['limits']

if len(sys.argv) > 2:
    dss = sys.argv[2].split(":")
else:
    dss = list(datasets.keys())

# Do not plot for now
syst = None
stat = None

outdir = eosrootdir + "/" + configname
#outdir = "."

filelist = []

for ds in dss:
    calibfile = "%s/%s.root" % (outdir, ds)
    name = ds
    filelist += [(calibfile, outdir + "/control/" + name + "_control.root",
                  "plots/" + configname + "_" + name)]
    if stat:
        for i in range(0, stat):
            name = "%s_stat_%d" % (ds, i)
            filelist += [(calibfile,
                          outdir + "/control/" + name + "_control.root",
                          "plots/" + configname + "_" + name)]
    if syst:
        for i in range(1, syst + 1):
            name = "%s_syst_%d" % (ds, i)
            filelist += [(calibfile,
                          outdir + "/control/" + name + "_control.root",
                          "plots/" + configname + "_" + name)]

for f in filelist:
    print(f)
    if limits:
        plotComparison(f[0], f[1], f[2], bins, limits[0], limits[1])
    else:
        plotComparison(f[0], f[1], f[2], bins)
#  os.system("eos cp %s %s/plots/%s" % (f[2], eosdir, ))
