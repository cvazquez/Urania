sample = {   'eta': 'probe_Brunel_ETA',
    'name': 'p_Lam0_VHPT_Brunel',
    'ntracks': 'nTracks',
    'pt': 'probe_Brunel_PT',
    'trees': ('Lam0_VHPT_PTuple/DecayTree', 'Lam0_VHPT_PbarTuple/DecayTree'),
    'weight': 'probe_sWeight'}
