sample = {   'eta': 'probe_Brunel_ETA',
    'name': 'p_Lam0_HPT_Brunel',
    'ntracks': 'nTracks',
    'pt': 'probe_Brunel_PT',
    'trees': ('Lam0_HPT_PTuple/DecayTree', 'Lam0_HPT_PbarTuple/DecayTree'),
    'weight': 'probe_sWeight'}
