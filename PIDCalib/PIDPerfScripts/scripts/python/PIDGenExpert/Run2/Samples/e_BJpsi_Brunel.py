sample = {   'eta': 'probe_Brunel_ETA',
    'name': 'e_BJpsi_Brunel',
    'ntracks': 'nTracks',
    'pt': 'probe_Brunel_PT',
    'trees': {
       "MagDown_2015" : ('B_Jpsi_EPTuple/DecayTree', 'B_Jpsi_EMTuple/DecayTree'),
       "MagUp_2015"   : ('B_Jpsi_EPTuple/DecayTree', 'B_Jpsi_EMTuple/DecayTree'),
       "MagDown_2016" : ('B_Jpsi_EPTuple/DecayTree', 'B_Jpsi_EMTuple/DecayTree'),
       "MagUp_2016"   : ('B_Jpsi_EPTuple/DecayTree', 'B_Jpsi_EMTuple/DecayTree'),
    }, 
    'weight': 'probe_sWeight'
}
