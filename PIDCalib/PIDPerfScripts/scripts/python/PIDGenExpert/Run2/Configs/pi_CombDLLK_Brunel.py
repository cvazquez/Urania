config = {   'bins': 100,
    'controlstat': 5000000,
    'gamma': 1.0,
    'limits': (-150.0, 50.0),
    'name': 'pi_CombDLLK_Brunel',
    'nbootstrap': 5,
    'sample': 'pi_DSt_Brunel',
    'scale_default': 0.1,
    'scale_pid': 1.0,
    'scale_syst': 0.15,
    'toystat': 10000000,
    'var': 'probe_Brunel_PIDK'}
