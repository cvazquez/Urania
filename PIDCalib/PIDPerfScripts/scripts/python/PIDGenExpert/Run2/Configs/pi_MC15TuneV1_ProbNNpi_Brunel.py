config = {   'bins': 100,
    'controlstat': 1000000,
    'gamma': -0.2,
    'name': 'pi_MC15TuneV1_ProbNNpi_Brunel',
    'nbootstrap': 5,
    'sample': 'pi_DSt_Brunel',
    'scale_default': 0.1,
    'scale_pid': 1.0,
    'scale_syst': 0.15,
    'toystat': 20000000,
    'var': 'probe_Brunel_MC15TuneV1_ProbNNpi'}
