###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function
from __future__ import absolute_import

import os, sys
from ROOT import TFile, TNtuple, gSystem, gROOT
from math import sqrt, log
import numpy as np
from root_numpy import array2root, root2array, rec2array
from copy import copy

from .Config import *


def convert_single_file(infile,
                        outfile,
                        treenames,
                        pidvar,
                        ptvar,
                        etavar,
                        ntracksvar,
                        weightvar,
                        transform,
                        sel_cut=None,
                        pol_cut=None):
    infile2 = infile
    if infile2.startswith("root://eoslhcb.cern.ch/"):
        infile2 = infile[len("root://eoslhcb.cern.ch/"):]
    if not os.path.isfile(infile2):
        print("No file", infile2)
        return False
    else:
        f = TFile(infile2)
        if f.IsZombie():
            f.Close()
            return False
        if f.TestBit(TFile.kRecovered):
            f.Close()
            return False
        f.Close()

    ok = False
    print("Open file", infile2)
    pidvar = pidvar.replace("i.", "")
    if sel_cut: sel_cut = sel_cut.replace("i.", "")
    variables = [pidvar, ptvar, etavar, ntracksvar, weightvar]
    cut = "(%s>0&&%s>0)" % (ptvar, ntracksvar)
    if sel_cut: cut = cut + "&&(" + sel_cut + ")"
    if pol_cut: cut = cut + "&&(" + pol_cut + ")"
    print(cut)
    if isinstance(transform, float):
        if transform < 0.:
            transform_func = lambda x: 1. - (1. - x)**(-transform)
            cut += ("&&(%s>=0.&&%s<=1.)" % (pidvar, pidvar))
        elif transform > 0.:
            print(transform)
            transform_func = lambda x: x**transform
            if transform != 1.0:
                cut += ("&&(%s>=0.&&%s<=1.)" % (pidvar, pidvar))
        else:
            transform_func = lambda x: x
    elif isinstance(transform, str):
        transform_func = lambda x: eval(transform)
    for treename in treenames:
        f = TFile(infile2)
        splitInTree = treename.split("/")
        if len(splitInTree) > 1 and f.GetListOfKeys().Contains(splitInTree[0]):
            f.cd(splitInTree[0])
            t = gROOT.FindObject(splitInTree[1])
            print(t)
            if t and t.GetEntries() > 1:
                f.Close()
                print(infile2)
            else:
                f.Close()
                continue
        try:
            array = root2array(
                infile,
                treename,
                branches=variables,
                selection=cut,
                warn_missing_tree=True)
        except:
            continue
        print("  Tree", treename, " Nentries=", len(array))
        if len(array) > 0:
            ok = True
            if transform:
                array[pidvar] = transform_func(array[pidvar])
            array[ptvar] = np.log(array[ptvar])
            array[ntracksvar] = np.log(array[ntracksvar])
            array.dtype.names = ("PID", "Pt", "Eta", "Ntracks", "w")
            arrlist = [
                array[v].astype(dtype=np.float32) for v in array.dtype.names
            ]
            for i in range(0, 5):
                print(len(arrlist[i]))
            recarray = np.rec.fromarrays(
                arrlist,
                dtype=[
                    ('PID', np.float32),
                    ('Pt', np.float32),
                    ('Eta', np.float32),
                    ('Ntracks', np.float32),
                    ('w', np.float32),
                ])
            array2root(recarray, outfile, treename="pid", mode="update")
    return ok


if len(sys.argv) > 1:
    # config name, e.g. "p_ProbNNp"
    configname = sys.argv[1]
else:
    print(
        "Usage: MakeTuples [config] [option1:option2:...] [dataset1:dataset2:]"
    )
    print("  configs are: ")
    for i in sorted(configs().keys()):
        print("    ", i)
    print("  options are: ")
    print(
        "    test     - run for just a single PIDCalib file rather than whole dataset"
    )
    print(
        "    polarity - create separate datasets for positive and negative track polarities"
    )
    print(
        "    brem     - create separate datasets for HasBremAdded=0 and 1 electrons"
    )
    print("  datasets can be, e.g. ")
    print("    MagDown_2012:MagUp_2011")
    print("    or leave empty to process all available datasets")
    sys.exit(0)

config = configs()[configname]
# sample name, e.g. "p" for proton
samplename = config['sample']
sample = samples()[samplename]

dslist = list(datasets.keys())
options = ""
if len(sys.argv) > 2: options = sys.argv[2].split(":")
if len(sys.argv) > 3: dslist = sys.argv[3].split(":")

# resampled variable name
var = configname

# other variable names
pidvar = config['var']
ptvar = sample['pt']
etavar = sample['eta']
ntracksvar = sample['ntracks']
weightvar = sample['weight']
treename = sample['trees']
treedict = copy(treename)
transform = None
if 'gamma' in list(config.keys()):
    transform = config['gamma']
elif 'transform_forward' in list(config.keys()):
    transform = config['transform_forward']

cut = None
if "cut" in list(config.keys()): cut = config["cut"]

# Create dictionary with the lists of PIDCalib datasets for each year and magnet polarity
dsdict = {}

if "datasets" in list(sample.keys()):
    dss = sample['datasets']
else:
    dss = datasets
for ds in dslist:
    if not isinstance(dss[ds], tuple):
        dsdict[ds] = [dss[ds]]
    else:
        dsdict[ds] = []
        n = dss[ds][1]
        for i in range(0, n):
            dsdict[ds] += [dss[ds][0] % i]

os.system("eos mkdir -p %s/%s" % (eosdir, configname))

# Loop over PIDCalib datasets
for pol, dss in dsdict.items():
    if isinstance(treedict, dict):
        treename = treedict[pol]

    if "polarity" in options:
        calibfile_p = tmpdir + "/" + pol + "_P.root"
        calibfile_m = tmpdir + "/" + pol + "_M.root"
        if os.path.isfile(calibfile_p): os.remove(calibfile_p)
        if os.path.isfile(calibfile_m): os.remove(calibfile_m)
    elif "brem" in options:
        calibfile_nb = tmpdir + "/" + pol + "_NoBrem.root"
        calibfile_b = tmpdir + "/" + pol + "_Brem.root"
        if os.path.isfile(calibfile_nb): os.remove(calibfile_nb)
        if os.path.isfile(calibfile_b): os.remove(calibfile_b)
    else:
        calibfile = tmpdir + "/" + pol + ".root"
        if os.path.isfile(calibfile): os.remove(calibfile)

    print("Polarity " + pol)

    ntracksvar2 = ntracksvar
    if isinstance(ntracksvar, dict):
        ntracksvar2 = ntracksvar[pol]

    nds = 0
    for ds in dss:
        ok = False
        if "polarity" in options:
            polarity_var = "probe_Brunel_charge"
            if "polarity" in sample: polarity_var = sample["polarity"]
            p_cut = "%s>0" % polarity_var
            m_cut = "%s<0" % polarity_var
            try:
                ok = convert_single_file(ds, calibfile_p, treename, pidvar,
                                         ptvar, etavar, ntracksvar2, weightvar,
                                         transform, cut, p_cut)
                ok = convert_single_file(ds, calibfile_m, treename, pidvar,
                                         ptvar, etavar, ntracksvar2, weightvar,
                                         transform, cut, m_cut)
            except:
                continue
        elif "brem" in options:
            brem_var = "probe_Brunel_HasBremAdded"
            nb_cut = "%s==0" % brem_var
            b_cut = "%s==1" % brem_var
            try:
                ok = convert_single_file(ds, calibfile_nb, treename, pidvar,
                                         ptvar, etavar, ntracksvar2, weightvar,
                                         transform, cut, nb_cut)
                ok = convert_single_file(ds, calibfile_b, treename, pidvar,
                                         ptvar, etavar, ntracksvar2, weightvar,
                                         transform, cut, b_cut)
            except:
                continue
        else:
            try:
                ok = convert_single_file(ds, calibfile, treename, pidvar,
                                         ptvar, etavar, ntracksvar2, weightvar,
                                         transform, cut)
            except:
                continue
        if ok: nds += 1
        if nds >= 2 and "test" in options:
            break

    if "polarity" in options:
        print("eos cp %s %s/%s/" % (calibfile_p, eosdir, configname))
        os.system("eos cp %s %s/%s/" % (calibfile_p, eosdir, configname))
        os.remove(calibfile_p)
        print("eos cp %s %s/%s/" % (calibfile_m, eosdir, configname))
        os.system("eos cp %s %s/%s/" % (calibfile_m, eosdir, configname))
        os.remove(calibfile_m)
    elif "brem" in options:
        os.system("eos cp %s %s/%s/" % (calibfile_nb, eosdir, configname))
        os.remove(calibfile_nb)
        os.system("eos cp %s %s/%s/" % (calibfile_b, eosdir, configname))
        os.remove(calibfile_b)
    else:
        os.system("eos cp %s %s/%s/" % (calibfile, eosdir, configname))
        os.remove(calibfile)

    if "test" in options:
        print("Now run:")
        print("root -l %s/%s/%s" % (eosdir, configname, calibfile))
