###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
conf = [
    #     "pi_CombDLLK",
    #     "pi_CombDLLp",
    #     "pi_CombDLLmu",
    #     "pi_V2ProbNNpi",
    #     "pi_V2ProbNNK",
    #     "pi_V2ProbNNp",
    #     "pi_V3ProbNNpi",
    #     "pi_V3ProbNNK",
    #     "pi_V3ProbNNp",

    #     "K_CombDLLK",
    #     "K_CombDLLp",
    #     "K_CombDLLmu",
    #     "K_V2ProbNNpi",
    #     "K_V2ProbNNK",
    #     "K_V2ProbNNp",
    #     "K_V3ProbNNpi",
    #     "K_V3ProbNNK",
    #     "K_V3ProbNNp",

    #     "p_CombDLLK",
    #     "p_CombDLLp",
    #     "p_CombDLLmu",
    #     "p_V2ProbNNpi",
    #     "p_V2ProbNNK",
    #     "p_V2ProbNNp",
    #     "p_V3ProbNNpi",
    #     "p_V3ProbNNK",
    #     "p_V3ProbNNp",

    #  "mu_V3ProbNNmu",

    #     "pi_V3ProbNNKNotpi",
    #     "pi_V3ProbNNpiNotK",

    #     "K_V3ProbNNKNotpi",
    #     "K_V3ProbNNpiNotK",
    "pi_V3ProbNNpiNotKp",
    "K_V3ProbNNKNotppi",
    "p_V3ProbNNpNotKpi",
]

import os

for i in conf:
    #os.system("python MakeTuples.py %s test MagUp_2012" % i )
    #os.system("python MakeTuples.py %s" % i )
    os.system("python CreatePIDPdf_condor.py %s continue" % i)
