conf = [
#     "pi_CombDLLK", 
     "pi_CombDLLp",
#     "pi_CombDLLmu",
     "pi_V2ProbNNpi", 
     "pi_V2ProbNNK", 
     "pi_V2ProbNNp", 
     "pi_V3ProbNNpi", 
     "pi_V3ProbNNK", 
     "pi_V3ProbNNp", 

     "K_CombDLLK", 
     "K_CombDLLp",
#     "K_CombDLLmu",
     "K_V2ProbNNpi", 
     "K_V2ProbNNK", 
     "K_V2ProbNNp", 
     "K_V3ProbNNpi", 
     "K_V3ProbNNK", 
     "K_V3ProbNNp", 

     "p_CombDLLK", 
     "p_CombDLLp",
#     "p_CombDLLmu",
     "p_V2ProbNNpi", 
     "p_V2ProbNNK", 
     "p_V2ProbNNp", 
     "p_V3ProbNNpi", 
     "p_V3ProbNNK", 
     "p_V3ProbNNp", 

]

import os

for i in conf : 
  os.system("python CreatePIDPdf_MC_condor.py sim09 %s dry:continue" % i )
