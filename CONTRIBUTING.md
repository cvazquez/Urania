# How to work with Urania

For development purposes of any of the packages hosted under Urania, please follow these [instructions](https://lhcb-core-doc.web.cern.ch/lhcb-core-doc/Development.html#satellite-projects).

<b>IMPORTANT:</b> do not accept MRs before allowing them to run in the nightlies. Please label your MR as ```all-slots``` (or with the label corresponding to the slot you want the MR to be ran on).

<b>IMPORTANT:</b> if you plan to add a new package, please send an e-mail first to [<b>lhcb-urania-developers@cern.ch</b>](mailto:lhcb-urania-developers@cern.ch) to let the maintainer know.

In any case, there are two important rules you must remember:

1. <b>Never clone the whole project for development purposes.</b> Please follow the instructions above.
2. <b>Always checkout the packages from the master branch.</b> If not, when asking for a Merge Request (MR), incompatibilities will appear and thus the MR will be closed. 

Apart from project maintainers, also certain package maintainers are allowed to accept and revert MR. These people are listed in the [CODEOWNERS](https://gitlab.cern.ch/lhcb/Urania/blob/master/CODEOWNERS) file. 

If you are a package developer and you think you should be granted these permissions as well, please send an email to the mailing list above.

