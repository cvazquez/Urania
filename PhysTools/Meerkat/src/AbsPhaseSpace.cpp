/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <stdio.h>
#include <vector>

#include "Meerkat/AbsPhaseSpace.hh"
#include <cstring>

AbsPhaseSpace::AbsPhaseSpace( const char* phaseSpaceName ) {
  strncpy( m_name, phaseSpaceName, 255 );
  m_name[255] = 0;
}

AbsPhaseSpace::~AbsPhaseSpace() {}
