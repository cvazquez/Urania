###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    from Lb2LcPiConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5200, 6200],
        "InputName": "lab0_MassFitConsD_M"
    }

    ### Weighting MC samples ###
    configdict["WeightingMassTemplates"] = {
        "PIDBachEff": {
            "2011": {
                "FileLabel": "#PIDK Pion 2011",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "Pi_DLLK<0_All"
            },
            "2012": {
                "FileLabel": "#PIDK Pion 2012",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "Pi_DLLK<0_All"
            },
            "2015": {
                "FileLabel": "#PIDK Pion 2015",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "Pi_DLLK<0_All"
            },
            "2016": {
                "FileLabel": "#PIDK Pion 2016",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "Pi_DLLK<0_All"
            },
            "2017": {
                "FileLabel": "#PIDK Pion 2016",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "Pi_DLLK<0_All"
            },
            "2018": {
                "FileLabel": "#PIDK Pion 2016",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "Pi_DLLK<0_All"
            },
        }
    }

    return configdict
