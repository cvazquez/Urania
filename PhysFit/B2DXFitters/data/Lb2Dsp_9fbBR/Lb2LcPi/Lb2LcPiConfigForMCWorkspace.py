###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    from Lb2LcPiConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5"
    }

    ### Weighting MC samples ###
    configdict["WeightingMassTemplates"] = {
        "PIDBachEff": {
            "2011": {
                "FileLabel": "#PIDK Pion 2011",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_DLLK<0.0&&IsMuon==0.0_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Pion 2012",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_DLLK<0.0&&IsMuon==0.0_All;"
            },
            "2015": {
                "FileLabel": "#PIDK Pion 2015",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_DLLK<0.0&&IsMuon==0.0_All;"
            },
            "2016": {
                "FileLabel": "#PIDK Pion 2016",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_DLLK<0.0&&IsMuon==0.0_All;"
            },
            "2017": {
                "FileLabel": "#PIDK Pion 2017",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_DLLK<0.0&&IsMuon==0.0_All;"
            },
            "2018": {
                "FileLabel": "#PIDK Pion 2018",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_DLLK<0.0&&IsMuon==0.0_All;"
            },
        },
        "PIDBachMisID": {
            "2011": {
                "FileLabel": "#PIDK Kaon 2011",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_DLLK<0.0&&IsMuon==0.0_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Kaon 2012",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_DLLK<0.0&&IsMuon==0.0_All;;"
            },
            "2015": {
                "FileLabel": "#PIDK Kaon 2015",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_DLLK<0.0&&IsMuon==0.0_All;"
            },
            "2016": {
                "FileLabel": "#PIDK Kaon 2016",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_DLLK<0.0&&IsMuon==0.0_All;"
            },
            "2017": {
                "FileLabel": "#PIDK Kaon 2017",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_DLLK<0.0&&IsMuon==0.0_All;"
            },
            "2018": {
                "FileLabel": "#PIDK Kaon 2018",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_DLLK<0.0&&IsMuon==0.0_All;"
            },
        },
        "PIDChild1Eff": {
            "2011": {
                "FileLabel": "#PIDK Proton 2011",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "P_TotLc_MC12TuneV2_ProbNNp>0.6&&IsMuon==0.0_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Proton 2012",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "P_TotLc_MC12TuneV2_ProbNNp>0.6&&IsMuon==0.0_All;"
            },
            "2015": {
                "FileLabel": "#PIDK Proton 2015",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "P_TotLc_MC15TuneV1_ProbNNp>0.6&&IsMuon==0.0_All;"
            },
            "2016": {
                "FileLabel": "#PIDK Proton 2016",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "P_TotLc_MC15TuneV1_ProbNNp>0.6&&IsMuon==0.0_All;"
            },
            "2017": {
                "FileLabel": "#PIDK Proton 2017",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "P_TotLc_MC15TuneV1_ProbNNp>0.6&&IsMuon==0.0_All;"
            },
            "2018": {
                "FileLabel": "#PIDK Proton 2018",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "P_TotLc_MC15TuneV1_ProbNNp>0.6&&IsMuon==0.0_All;"
            },
        },
        "PIDChild2Eff": {
            "2011": {
                "FileLabel": "#PIDK Kaon 2011",
                "Var": ["lab4_ETA", "lab4_P"],
                "HistName": "K_DLLK>0.0&&IsMuon==0.0_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Kaon 2012",
                "Var": ["lab4_ETA", "lab4_P"],
                "HistName": "K_DLLK>0.0&&IsMuon==0.0_All;"
            },
            "2015": {
                "FileLabel": "#PIDK Kaon 2015",
                "Var": ["lab4_ETA", "lab4_P"],
                "HistName": "K_DLLK>0.0&&IsMuon==0.0_All;"
            },
            "2016": {
                "FileLabel": "#PIDK Kaon 2016",
                "Var": ["lab4_ETA", "lab4_P"],
                "HistName": "K_DLLK>0.0&&IsMuon==0.0_All;"
            },
            "2017": {
                "FileLabel": "#PIDK Kaon 2017",
                "Var": ["lab4_ETA", "lab4_P"],
                "HistName": "K_DLLK>0.0&&IsMuon==0.0_All;"
            },
            "2018": {
                "FileLabel": "#PIDK Kaon 2018",
                "Var": ["lab4_ETA", "lab4_P"],
                "HistName": "K_DLLK>0.0&&IsMuon==0.0_All;"
            },
        },
        "PIDChild3Eff": {
            "2011": {
                "FileLabel": "#PIDK Pion 2011",
                "Var": ["lab5_ETA", "lab5_P"],
                "HistName": "Pi_DLLK<5.0&&IsMuon==0.0_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Pion 2012",
                "Var": ["lab5_ETA", "lab5_P"],
                "HistName": "Pi_DLLK<5.0&&IsMuon==0.0_All;"
            },
            "2015": {
                "FileLabel": "#PIDK Pion 2015",
                "Var": ["lab5_ETA", "lab5_P"],
                "HistName": "Pi_DLLK<5.0&&IsMuon==0.0_All;"
            },
            "2016": {
                "FileLabel": "#PIDK Pion 2016",
                "Var": ["lab5_ETA", "lab5_P"],
                "HistName": "Pi_DLLK<5.0&&IsMuon==0.0_All;"
            },
            "2017": {
                "FileLabel": "#PIDK Pion 2017",
                "Var": ["lab5_ETA", "lab5_P"],
                "HistName": "Pi_DLLK<5.0&&IsMuon==0.0_All;"
            },
            "2018": {
                "FileLabel": "#PIDK Pion 2018",
                "Var": ["lab5_ETA", "lab5_P"],
                "HistName": "Pi_DLLK<5.0&&IsMuon==0.0_All;"
            },
        },
        "PIDChildKaonProton": {
            "2011": {
                "FileLabel": "#PIDK Kaon 2011",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "K_MC12TuneV2_ProbNNp>0.6&&IsMuon==0.0_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Kaon 2012",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "K_MC12TuneV2_ProbNNp>0.6&&IsMuon==0.0_All;"
            },
            "2015": {
                "FileLabel": "#PIDK Kaon 2015",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "K_MC15TuneV1_ProbNNp>0.6&&IsMuon==0.0_All;"
            },
            "2016": {
                "FileLabel": "#PIDK Kaon 2016",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "K_MC15TuneV1_ProbNNp>0.6&&IsMuon==0.0_All;"
            },
            "2017": {
                "FileLabel": "#PIDK Kaon 2017",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "K_MC15TuneV1_ProbNNp>0.6&&IsMuon==0.0_All;"
            },
            "2018": {
                "FileLabel": "#PIDK Kaon 2018",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "K_MC15TuneV1_ProbNNp>0.6&&IsMuon==0.0_All;"
            },
        },
        "PIDChildPionProton": {
            "2011": {
                "FileLabel": "#PIDK Pion 2011",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "Pi_MC12TuneV2_ProbNNp>0.6&&IsMuon==0.0_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Pion 2012",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "Pi_MC12TuneV2_ProbNNp>0.6&&IsMuon==0.0_All;"
            },
            "2015": {
                "FileLabel": "#PIDK Pion 2015",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "Pi_MC15TuneV1_ProbNNp>0.6&&IsMuon==0.0_All;"
            },
            "2016": {
                "FileLabel": "#PIDK Pion 2016",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "Pi_MC15TuneV1_ProbNNp>0.6&&IsMuon==0.0_All;"
            },
            "2017": {
                "FileLabel": "#PIDK Pion 2017",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "Pi_MC15TuneV1_ProbNNp>0.6&&IsMuon==0.0_All;"
            },
            "2018": {
                "FileLabel": "#PIDK Pion 2018",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "Pi_MC15TuneV1_ProbNNp>0.6&&IsMuon==0.0_All;"
            },
        },
        "Shift": {
            "2011": {
                "BeautyMass": -2.0,
                "CharmMass": 0.0
            },
            "2012": {
                "BeautyMass": -2.0,
                "CharmMass": 0.0
            },
            "2015": {
                "BeautyMass": -1.0,
                "CharmMass": 0.0
            },
            "2016": {
                "BeautyMass": -1.1,
                "CharmMass": 0.0
            },
            "2017": {
                "BeautyMass": -0.95,
                "CharmMass": 0.0
            },
            "2018": {
                "BeautyMass": -1.65,
                "CharmMass": 0.0
            },
        }
    }

    return configdict
