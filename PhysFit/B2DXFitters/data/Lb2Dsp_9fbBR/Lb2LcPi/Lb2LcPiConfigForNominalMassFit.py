###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    configdict = {}

    from math import pi
    from math import log

    # considered decay mode
    configdict["Decay"] = "Lb2LcPi"
    configdict["CharmModes"] = {"pKPi"}
    configdict["Backgrounds"] = [
        "Bd2DPi", "Bs2DsPi", "Lb2LcK", "Lb2LcRho", "Lb2ScPi"
    ]

    # year of data taking
    configdict["YearOfDataTaking"] = {"2015", "2016", "2017", "2018"}

    # integrated luminosity in each year of data taking (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {
        "2011": {
            "Down": 0.5600,
            "Up": 0.4200
        },
        "2012": {
            "Down": 0.9912,
            "Up": 0.9988
        },
        "2015": {
            "Down": 0.18695,
            "Up": 0.14105
        },
        "2016": {
            "Down": 0.85996,
            "Up": 0.80504
        },
        "2017": {
            "Down": 0.87689,
            "Up": 0.83311
        },
        "2018": {
            "Down": 1.04846,
            "Up": 1.14154
        },
    }

    # stripping (necessary in case of PIDK shapes)
    configdict["dataName"] = "../data/Lb2Dsp_9fbBR/Lb2LcPi/config_Lb2LcPi.txt"
    #settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {
        "Directory": "PlotLb2LcPi",
        "Extension": "pdf"
    }

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5400, 6200],
        "InputName": "lab0_MassFitConsD_M"
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [2270, 2302],
        "InputName": "lab2_MM"
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [10000.0, 300000.0],
        "InputName": "lab1_P"
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-7.0, 5.0],
        "InputName": "lab1_PIDK"
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [400.0, 45000.0],
        "InputName": "lab1_PT"
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 1000.0],
        "InputName": "nTracks"
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID"
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.4, 1.0],
        "InputName": "BDTGResponse_3"
    }

    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data": "lab3_ProbNNp>0.6 && lab4_PIDK>0 && lab5_PIDK<5 &&lab1_PIDK<0",
        #"MC"  : "lab1_M<200&&lab1_PIDK !=-1000.0&&lab2_FD_ORIVX > 0.&&(lab0_LifetimeFit_Lambda_cplus_ctau[0]>0",
        "MCID": False,
        "MCTRUEID": False,
        "BKGCAT": False,
        "DsHypo": False
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: pKPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5"
    }

    #############################################################################################################
    #################################                FITTING                #####################################
    #############################################################################################################

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run1": {
            "All": 5620.
        },
        "Run2": {
            "All": 5620.
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "Run1": {
            "All": 19.8
        },
        "Run2": {
            "All": 25.3
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "Run1": {
            "All": 17.19
        },
        "Run2": {
            "All": 17.37
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "Run1": {
            "All": 0.0
        },
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "Run1": {
            "All": 0.0
        },
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "Run1": {
            "All": -2.11
        },
        "Run2": {
            "All": -2.11
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "Run1": {
            "All": 1.29
        },
        "Run2": {
            "All": 0.93
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "Run1": {
            "All": 1.86
        },
        "Run2": {
            "All": 1.40
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run1": {
            "All": 1.199
        },
        "Run2": {
            "All": 1.1346
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run1": {
            "All": 2.72
        },
        "Run2": {
            "All": 2.4284
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "Run1": {
            "All": -0.128
        },
        "Run2": {
            "All": -0.330
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "Run1": {
            "All": 0.384
        },
        "Run2": {
            "All": 0.423
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "Run1": {
            "All": 0.32
        },
        "Run2": {
            "All": 0.248
        },
        "Fixed": True
    }

    #configdict["CombBkgShape"] = {}
    #configdict["CombBkgShape"]["BeautyMass"] = {}
    #configdict["CombBkgShape"]["BeautyMass"]["type"]    = "DoubleExponential"
    #configdict["CombBkgShape"]["BeautyMass"]["cB1"]      = {"Run1": {"All" : -5.0279e-03},  "Run2":{"All":  -5.0279e-03},  "Fixed":False}
    #configdict["CombBkgShape"]["BeautyMass"]["cB2"]   = {"Run1": {"All":0.0},  "Run2":{"All":  0.0},  "Fixed":True}
    #configdict["CombBkgShape"]["BeautyMass"]["frac"]  = {"Run1": {"All":8.9591e-01},  "Run2":{"All":  8.9591e-01},  "Fixed":False}

    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "Exponential"
    configdict["CombBkgShape"]["BeautyMass"]["cB"] = {
        "Run1": {
            "All": -3.0e-03
        },
        "Run2": {
            "All": -4.4672e-3
        },
        "Fixed": False
    }

    #define yields
    configdict["Yields"] = {}

    configdict["Yields"]["Bd2DPi"] = {
        "2011": {
            "pKPi": 3812. / 2
        },
        "2012": {
            "pKPi": 3812. / 2
        },
        "2015": {
            "pKPi": 1.7670e+04 / 2
        },
        "2016": {
            "pKPi": 1.7670e+04 / 2
        },
        "Fixed": False
    }
    configdict["Yields"]["Bs2DsPi"] = {
        "2011": {
            "pKPi": 400.0
        },
        "2012": {
            "pKPi": 400.0
        },
        "2015": {
            "pKPi": 400.0
        },
        "2016": {
            "pKPi": 400.0
        },
        "Fixed": False
    }
    configdict["Yields"]["Lb2LcK"] = {
        "2011": {
            "pKPi": 2.9384e+02 / 2
        },
        "2012": {
            "pKPi": 2.9384e+02 / 2
        },
        "2015": {
            "pKPi": 3e3
        },
        "2016": {
            "pKPi": 3e3
        },
        "Fixed": False
    }
    configdict["Yields"]["Lb2LcRho"] = {
        "2011": {
            "pKPi": 1000.0
        },
        "2012": {
            "pKPi": 1000.0
        },
        "2015": {
            "pKPi": 8000.0
        },
        "2016": {
            "pKPi": 8000.0
        },
        "Fixed": False
    }
    configdict["Yields"]["Lb2ScPi"] = {
        "2011": {
            "pKPi": 12000.0
        },
        "2012": {
            "pKPi": 12000.0
        },
        "2015": {
            "pKPi": 20000.0
        },
        "2016": {
            "pKPi": 20000.0
        },
        "Fixed": False
    }
    configdict["Yields"]["Signal"] = {
        "2011": {
            "pKPi": 5e4
        },
        "2012": {
            "pKPi": 5e4
        },
        "2015": {
            "pKPi": 1.1e5
        },
        "2016": {
            "pKPi": 1.1e5
        },
        "Fixed": False
    }
    configdict["Yields"]["CombBkg"] = {
        "2011": {
            "pKPi": 20000.0
        },
        "2012": {
            "pKPi": 20000.0
        },
        "2015": {
            "pKPi": 28125.0
        },
        "2016": {
            "pKPi": 28125.0
        },
        "Fixed": False
    }

    #############################################################################################################
    ################################              Plotting              #########################################
    #############################################################################################################

    from ROOT import *
    configdict["PlotSettings"] = {}

    configdict["PlotSettings"]["components"] = [
        "Sig", "CombBkg", "Bd2DPi", "Bs2DsPi", "Lb2LcK", "Lb2LcRho", "Lb2ScPi"
    ]
    configdict["PlotSettings"]["colors"] = [
        kRed - 7, kBlue - 6, kOrange, kMagenta + 2, kBlue - 10, kYellow,
        kBlue + 2
    ]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.75, 0.9],
        "ScaleYSize": 1.2
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.7,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.35, 0.9],
        "ScaleYSize": 1.2
    }

    return configdict
