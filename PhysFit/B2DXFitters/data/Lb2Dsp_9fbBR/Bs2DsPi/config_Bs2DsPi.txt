#Bs2DsPi KKPi 2011
/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/Data/Lb2Dsp/FINAL/
B2DX_2011_Down_Bs2DsPi-Lb2Dsp_OFFLINE_KKPi.root
B2DX_2011_Up_Bs2DsPi-Lb2Dsp_OFFLINE_KKPi.root
DecayTree
DecayTree
###

#Bs2DsPi KKPi 2012
/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/Data/Lb2Dsp/FINAL/
B2DX_2012_Down_Bs2DsPi-Lb2Dsp_OFFLINE_KKPi.root
B2DX_2012_Up_Bs2DsPi-Lb2Dsp_OFFLINE_KKPi.root
DecayTree
DecayTree
###

#Bs2DsPi KKPi 2015
/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/Data/Lb2Dsp/FINAL/
B2DX_2015_Down_Bs2DsPi-Lb2Dsp_OFFLINE_KKPi.root
B2DX_2015_Up_Bs2DsPi-Lb2Dsp_OFFLINE_KKPi.root
DecayTree
DecayTree
###

#Bs2DsPi KKPi 2016
/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/Data/Lb2Dsp/FINAL/
B2DX_2016_Down_Bs2DsPi-Lb2Dsp_OFFLINE_KKPi.root
B2DX_2016_Up_Bs2DsPi-Lb2Dsp_OFFLINE_KKPi.root
DecayTree
DecayTree
###

#Bs2DsPi KKPi 2017
/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/Data/Lb2Dsp/FINAL/
B2DX_2017_Down_Bs2DsPi-Lb2Dsp_OFFLINE_KKPi.root
B2DX_2017_Up_Bs2DsPi-Lb2Dsp_OFFLINE_KKPi.root
DecayTree
DecayTree
###

#Bs2DsPi KKPi 2018
/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/Data/Lb2Dsp/FINAL/
B2DX_2018_Down_Bs2DsPi-Lb2Dsp_OFFLINE_KKPi.root
B2DX_2018_Up_Bs2DsPi-Lb2Dsp_OFFLINE_KKPi.root
DecayTree
DecayTree
###


#MC FileName KKPi MD 2011
{"Mode":"Bd2DPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping21_Sim08g/SELECTED/B2DX_MC_Bd_D-pi+_Kpipi_CPV_2012_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsRho",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping21_Sim08g/SELECTED/B2DX_MC_Bs_Dsrho_KKpi_2012_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstRho",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping21_Sim08g/SELECTED/B2DX_MC_Bs_Dsstrho_KKpi_2012_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping21_Sim08g/SELECTED/B2DX_MC_Bs_Dsstpi_KKpi_2012_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2LcPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping21_Sim08g/SELECTED/B2DX_MC_Lb_Lambdacpi_2012_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsK",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping21_Sim08g/SELECTED/B2DX_MC_Bs_DsK_KKpi_CPV_2012_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
###

#MC FileName KKPi MU 2011
{"Mode":"Bd2DPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping21_Sim08g/SELECTED/B2DX_MC_Bd_D-pi+_Kpipi_CPV_2012_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsRho",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping21_Sim08g/SELECTED/B2DX_MC_Bs_Dsrho_KKpi_2012_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstRho",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping21_Sim08g/SELECTED/B2DX_MC_Bs_Dsstrho_KKpi_2012_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping21_Sim08g/SELECTED/B2DX_MC_Bs_Dsstpi_KKpi_2012_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2LcPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping21_Sim08g/SELECTED/B2DX_MC_Lb_Lambdacpi_2012_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsK",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping21_Sim08g/SELECTED/B2DX_MC_Bs_DsK_KKpi_CPV_2012_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
###

#MC FileName KKPi MD 2012
{"Mode":"Bd2DPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping21_Sim08g/SELECTED/B2DX_MC_Bd_D-pi+_Kpipi_CPV_2012_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsRho",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping21_Sim08g/SELECTED/B2DX_MC_Bs_Dsrho_KKpi_2012_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstRho",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping21_Sim08g/SELECTED/B2DX_MC_Bs_Dsstrho_KKpi_2012_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping21_Sim08g/SELECTED/B2DX_MC_Bs_Dsstpi_KKpi_2012_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2LcPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping21_Sim08g/SELECTED/B2DX_MC_Lb_Lambdacpi_2012_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsK",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping21_Sim08g/SELECTED/B2DX_MC_Bs_DsK_KKpi_CPV_2012_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
###

#MC FileName KKPi MU 2012
{"Mode":"Bd2DPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping21_Sim08g/SELECTED/B2DX_MC_Bd_D-pi+_Kpipi_CPV_2012_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsRho",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping21_Sim08g/SELECTED/B2DX_MC_Bs_Dsrho_KKpi_2012_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstRho",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping21_Sim08g/SELECTED/B2DX_MC_Bs_Dsstrho_KKpi_2012_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping21_Sim08g/SELECTED/B2DX_MC_Bs_Dsstpi_KKpi_2012_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2LcPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping21_Sim08g/SELECTED/B2DX_MC_Lb_Lambdacpi_2012_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsK",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping21_Sim08g/SELECTED/B2DX_MC_Bs_DsK_KKpi_CPV_2012_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
###

#MC FileName KKPi MD 2015
{"Mode":"Bd2DPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping24r1_Sim09c/SELECTED/B2DX_MC_11264001_Bd_D-pi_2015_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsRho",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping24r1_Sim09c/SELECTED/B2DX_MC_13264421_Bs_Dsrho_KKpi_2015_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstRho",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping24r1_Sim09c/SELECTED/B2DX_MC_13266601_Bs_Dsstrho_Dsgam_KKpi_pi0pi_2015_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping24r1_Sim09c/SELECTED/B2DX_MC_13264221_Bs_Dsstpi_KKpi_2015_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2LcPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping24r1_Sim09c/SELECTED/B2DX_MC_15364010_Lb_Lcpi_pKpi_2015_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsK",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping24r1_Sim09c/SELECTED/B2DX_MC_13264031_Bs_DsK_KKpi_2015_dw_Bs_BDTG_Selected_KKPi.root"
 "TreeName":"DecayTree"}
###

#MC FileName KKPi MU 2015
{"Mode":"Bd2DPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping24r1_Sim09c/SELECTED/B2DX_MC_11264001_Bd_D-pi_2015_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsRho",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping24r1_Sim09c/SELECTED/B2DX_MC_13264421_Bs_Dsrho_KKpi_2015_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstRho",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping24r1_Sim09c/SELECTED/B2DX_MC_13266601_Bs_Dsstrho_Dsgam_KKpi_pi0pi_2015_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping24r1_Sim09c/SELECTED/B2DX_MC_13264221_Bs_Dsstpi_KKpi_2015_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2LcPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping24r1_Sim09c/SELECTED/B2DX_MC_15364010_Lb_Lcpi_pKpi_2015_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsK",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping24r1_Sim09c/SELECTED/B2DX_MC_13264031_Bs_DsK_KKpi_2015_up_Bs_BDTG_Selected_KKPi.root"
 "TreeName":"DecayTree"}
###

#MC FileName KKPi MD 2016
{"Mode":"Bd2DPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping28r1_Sim09c/SELECTED/B2DX_MC_11264001_Bd_D-pi_2016_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsRho",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping28r1_Sim09c/SELECTED/B2DX_MC_13264421_Bs_Dsrho_KKpi_2016_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstRho",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping28r1_Sim09c/SELECTED/B2DX_MC_13266601_Bs_Dsstrho_Dsgam_KKpi_pi0pi_2016_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping28r1_Sim09c/SELECTED/B2DX_MC_13264221_Bs_Dsstpi_KKpi_2016_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2LcPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping28r1_Sim09c/SELECTED/B2DX_MC_15364010_Lb_Lcpi_pKpi_2016_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsK",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping28r1_Sim09c/SELECTED/B2DX_MC_13264031_Bs_DsK_KKpi_2016_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
###

#MC FileName KKPi MU 2016
{"Mode":"Bd2DPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping28r1_Sim09c/SELECTED/B2DX_MC_11264001_Bd_D-pi_2016_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsRho",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping28r1_Sim09c/SELECTED/B2DX_MC_13264421_Bs_Dsrho_KKpi_2016_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstRho",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping28r1_Sim09c/SELECTED/B2DX_MC_13266601_Bs_Dsstrho_Dsgam_KKpi_pi0pi_2016_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping28r1_Sim09c/SELECTED/B2DX_MC_13264221_Bs_Dsstpi_KKpi_2016_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2LcPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping28r1_Sim09c/SELECTED/B2DX_MC_15364010_Lb_Lcpi_pKpi_2016_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsK",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping28r1_Sim09c/SELECTED/B2DX_MC_13264031_Bs_DsK_KKpi_2016_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
###



#MC FileName KKPi MD 2017
{"Mode":"Bd2DPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping29r2_Sim09g/SELECTED/B2DX_MC_11264001_Bd_D-pi_2017_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsRho",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping29r2_Sim09g/SELECTED/B2DX_MC_13264421_Bs_Dsrho_KKpi_2017_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstRho",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping29r2_Sim09g/SELECTED/B2DX_MC_13266601_Bs_Dsstrho_Dsgam_KKpi_pi0pi_2017_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping29r2_Sim09g/SELECTED/B2DX_MC_13264221_Bs_Dsstpi_KKpi_2017_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2LcPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping29r2_Sim09g/SELECTED/B2DX_MC_15364010_Lb_Lcpi_pKpi_2017_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsK",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping29r2_Sim09h/SELECTED/B2DX_MC_13264031_Bs_DsK_KKpi_2017_dw_Bs_BDTG_Selected_Small_KKPi.root",
 "TreeName":"DecayTree"}
###

#MC FileName KKPi MU 2017
{"Mode":"Bd2DPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping29r2_Sim09g/SELECTED/B2DX_MC_11264001_Bd_D-pi_2017_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsRho",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping29r2_Sim09g/SELECTED/B2DX_MC_13264421_Bs_Dsrho_KKpi_2017_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstRho",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping29r2_Sim09g/SELECTED/B2DX_MC_13266601_Bs_Dsstrho_Dsgam_KKpi_pi0pi_2017_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping29r2_Sim09g/SELECTED/B2DX_MC_13264221_Bs_Dsstpi_KKpi_2017_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2LcPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping29r2_Sim09g/SELECTED/B2DX_MC_15364010_Lb_Lcpi_pKpi_2017_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsK",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping29r2_Sim09h/SELECTED/B2DX_MC_13264031_Bs_DsK_KKpi_2017_up_Bs_BDTG_Selected_Small_KKPi.root",
 "TreeName":"DecayTree"}
###

#MC FileName KKPi MD 2018
{"Mode":"Bd2DPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping34_Sim09h/SELECTED/B2DX_MC_11264001_Bd_D-pi_2018_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsRho",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping34_Sim09h/SELECTED/B2DX_MC_13264421_Bs_Dsrho_KKpi_2018_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstRho",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping34_Sim09h/SELECTED/B2DX_MC_13266601_Bs_Dsstrho_Dsgam_KKpi_pi0pi_2018_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping34_Sim09h/SELECTED/B2DX_MC_13264221_Bs_Dsstpi_KKpi_2018_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2LcPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping34_Sim09h/SELECTED/B2DX_MC_15364010_Lb_Lcpi_pKpi_2018_dw_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsK",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping34_Sim09h/SELECTED/B2DX_MC_13264031_Bs_DsK_KKpi_2018_dw_Bs_BDTG_Selected_Small_KKPi.root",
 "TreeName":"DecayTree"}
###

#MC FileName KKPi MU 2018
{"Mode":"Bd2DPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping34_Sim09h/SELECTED/B2DX_MC_11264001_Bd_D-pi_2018_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsRho",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping34_Sim09h/SELECTED/B2DX_MC_13264421_Bs_Dsrho_KKpi_2018_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstRho",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping34_Sim09h/SELECTED/B2DX_MC_13266601_Bs_Dsstrho_Dsgam_KKpi_pi0pi_2018_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping34_Sim09h/SELECTED/B2DX_MC_13264221_Bs_Dsstpi_KKpi_2018_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2LcPi",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping34_Sim09h/SELECTED/B2DX_MC_15364010_Lb_Lcpi_pKpi_2018_up_Bs_BDTG_Selected_KKPi.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsK",
 "FileName":"/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/MC/Bs2DsH/Stripping34_Sim09h/SELECTED/B2DX_MC_13264031_Bs_DsK_KKpi_2018_up_Bs_BDTG_Selected_Small_KKPi.root",
 "TreeName":"DecayTree"}
###

#PIDK Kaon 2011
/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/PerfHists/
PerfHists_K_Strip21r1_MagDown_ETABin_ETA_P.root
PerfHists_K_Strip21r1_MagUp_ETABin_ETA_P.root
###

#PIDK Kaon 2012
/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/PerfHists/
PerfHists_K_Strip21_MagDown_ETABin_ETA_P.root
PerfHists_K_Strip21r1_MagUp_ETABin_ETA_P.root
###

#PIDK Kaon 2015
/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/PerfHists/
PerfHists_K_Turbo15_MagDown_ETABin_Brunel_ETA_Brunel_P.root
PerfHists_K_Turbo15_MagUp_ETABin_Brunel_ETA_Brunel_P.root
###

#PIDK Kaon 2016
/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/PerfHists/
PerfHists_K_Turbo16_MagDown_ETABin_Brunel_ETA_Brunel_P.root
PerfHists_K_Turbo16_MagUp_ETABin_Brunel_ETA_Brunel_P.root
###

#PIDK Kaon 2017
/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/PerfHists/
PerfHists_K_Turbo17_MagDown_ETABin_Brunel_ETA_Brunel_P.root
PerfHists_K_Turbo17_MagUp_ETABin_Brunel_ETA_Brunel_P.root
###

#PIDK Kaon 2018
/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/PerfHists/
PerfHists_K_Turbo18_MagDown_ETABin_Brunel_ETA_Brunel_P.root
PerfHists_K_Turbo18_MagUp_ETABin_Brunel_ETA_Brunel_P.root
###

#PIDK Pion 2011
/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/PerfHists/
PerfHists_Pi_Strip21r1_MagDown_ETABin_ETA_P.root
PerfHists_Pi_Strip21r1_MagUp_ETABin_ETA_P.root
###

#PIDK Pion 2012
/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/PerfHists/
PerfHists_Pi_Strip21_MagDown_ETABin_ETA_P.root
PerfHists_Pi_Strip21r1_MagUp_ETABin_ETA_P.root
###

#PIDK Pion 2015
/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/PerfHists/
PerfHists_Pi_Turbo15_MagDown_ETABin_Brunel_ETA_Brunel_P.root
PerfHists_Pi_Turbo15_MagUp_ETABin_Brunel_ETA_Brunel_P.root
###

#PIDK Pion 2016
/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/PerfHists/
PerfHists_Pi_Turbo16_MagDown_ETABin_Brunel_ETA_Brunel_P.root
PerfHists_Pi_Turbo16_MagUp_ETABin_Brunel_ETA_Brunel_P.root
###

#PIDK Pion 2017
/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/PerfHists/
PerfHists_Pi_Turbo17_MagDown_ETABin_Brunel_ETA_Brunel_P.root
PerfHists_Pi_Turbo17_MagUp_ETABin_Brunel_ETA_Brunel_P.root
###

#PIDK Pion 2018
/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/PerfHists/
PerfHists_Pi_Turbo18_MagDown_ETABin_Brunel_ETA_Brunel_P.root
PerfHists_Pi_Turbo18_MagUp_ETABin_Brunel_ETA_Brunel_P.root
###

#PIDK Proton 2011
/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/PerfHists/
PerfHists_P_TotLc_Strip21r1_MagDown_ETABin_ETA_P.root
PerfHists_P_TotLc_Strip21r1_MagUp_ETABin_ETA_P.root
###

#PIDK Proton 2012
/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/PerfHists/
PerfHists_P_TotLc_Strip21_MagDown_ETABin_ETA_P.root
PerfHists_P_TotLc_Strip21_MagUp_ETABin_ETA_P.root
###

#PIDK Proton 2015
/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/PerfHists/
PerfHists_P_TotLc_Turbo16_MagDown_ETABin_Brunel_ETA_Brunel_P.root
PerfHists_P_TotLc_Turbo16_MagUp_ETABin_Brunel_ETA_Brunel_P.root
###

#PIDK Proton 2016
/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/PerfHists/
PerfHists_P_TotLc_Turbo16_MagDown_ETABin_Brunel_ETA_Brunel_P.root
PerfHists_P_TotLc_Turbo16_MagUp_ETABin_Brunel_ETA_Brunel_P.root
###

#PIDK Proton 2017
/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/PerfHists/
PerfHists_P_TotLc_Turbo17_MagDown_ETABin_Brunel_ETA_Brunel_P.root
PerfHists_P_TotLc_Turbo17_MagUp_ETABin_Brunel_ETA_Brunel_P.root
###

#PIDK Proton 2018
/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/PerfHists/
PerfHists_P_TotLc_Turbo18_MagDown_ETABin_Brunel_ETA_Brunel_P.root
PerfHists_P_TotLc_Turbo18_MagUp_ETABin_Brunel_ETA_Brunel_P.root
###

#DataMC 2011
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/Bd2DPi_data2MC_weights/Run1/p_nTracks/
weights_DPi_2011_dw.root
weights_DPi_2011_up.root
###

#DataMC 2012
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/Bd2DPi_data2MC_weights/Run1/p_nTracks/
weights_DPi_2012_dw.root
weights_DPi_2012_up.root
###

#DataMC 2015
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/Bd2DPi_data2MC_weights/Run2/p_nTracks/Stripping24r1_Sim09c/
weights_DPi_2015_dw.root
weights_DPi_2015_up.root
###

#DataMC 2016
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/Bd2DPi_data2MC_weights/Run2/p_nTracks/Stripping28r1_Sim09c/
weights_DPi_2016_dw.root
weights_DPi_2016_up.root
###

#DataMC 2017
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/Bd2DPi_data2MC_weights/Run2/p_nTracks/Stripping29r2_Sim09g/
weights_DPi_2017_dw.root
weights_DPi_2017_up.root
###

#DataMC 2018
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/Bd2DPi_data2MC_weights/Run2/p_nTracks/Stripping34_Sim09h/
weights_DPi_2018_dw.root
weights_DPi_2018_up.root
###

