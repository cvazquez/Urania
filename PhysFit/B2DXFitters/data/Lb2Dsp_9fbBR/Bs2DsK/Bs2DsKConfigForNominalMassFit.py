###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

def getconfig():

    configdict = {}
    from math import pi
    from math import log

    # considered decay mode
    configdict["Decay"] = "Bs2DsK"
    configdict["CharmModes"] = {"KKPi"}
    configdict["Backgrounds"] = [
        "Bd2DPi", "Bd2DK", "Lb2LcK", "Lb2LcPi", "Bs2DsPi", "Bs2DsRho",
        "Bs2DsstPi", "Bd2DsK", "Lb2Dsp", "Lb2Dsstp"
    ]

    # year of data taking
    configdict["YearOfDataTaking"] = {
        ,"2015","2016","2017","2018"
    }
    # integrated luminosity in each year of data taking (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {
        "2011": {
            "Down": 0.5600,
            "Up": 0.4200
        },
        "2012": {
            "Down": 0.9912,
            "Up": 0.9988
        },
        "2015": {
            "Down": 0.18695,
            "Up": 0.14105
        },
        "2016": {
            "Down": 0.85996,
            "Up": 0.80504
        },
        "2017": {
            "Down": 0.87689,
            "Up": 0.83311
        },
        "2018": {
            "Down": 1.04846,
            "Up": 1.14154
        },
    }

    # stripping (necessary in case of PIDK shapes)
    configdict["dataName"] = "../data/Lb2Dsp_9fbBR/Bs2DsK/config_Bs2DsK.txt"
    #settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {
        "Directory": "PlotBs2DsK_for_Lb2Dsp",
        "Extension": "pdf"
    }

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5000, 6000],
        "InputName": "lab0_MassFitConsD_M"
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1950, 1990],
        "InputName": "lab2_MM"
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [log(10.0), 5.0],
        "InputName": "lab1_PIDK"
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [10000.0, 650000.0],
        "InputName": "lab1_P"
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [400.0, 45000.0],
        "InputName": "lab1_PT"
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 1000.0],
        "InputName": "nTracks"
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID"
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.4, 1.0],
        "InputName": "BDTGResponse_3"
    }

    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data":
        "(lab1_isMuon==0)&&(lab0_MassHypo_Dsp> 5000.0 && lab0_MassHypo_Dsp < 7000.0)",
        "MC":
        "lab1_M>200&&lab1_PIDK !=-1000.0&&(lab0_MassHypo_Dsp> 5000.0 && lab0_MassHypo_Dsp < 7000.0)&&(lab1_PT > 400)&&(lab1_P>10000)",
        "MCID":
        True,
        "MCTRUEID":
        True,
        "BKGCAT":
        False,
        "DsHypo":
        True
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5"
    }

    ### Weighting MC samples ###

    configdict["WeightingMassTemplates"] = {
        "PIDBachEff": {
            "2011": {
                "FileLabel": "#PIDK Kaon 2011",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_DLLK>10.0&&IsMuon==0.0_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Kaon 2012",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_DLLK>10.0&&IsMuon==0.0_All;"
            },
            "2015": {
                "FileLabel": "#PIDK Kaon 2015",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_DLLK>10.0&&IsMuon==0.0_All;"
            },
            "2016": {
                "FileLabel": "#PIDK Kaon 2016",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_DLLK>10.0&&IsMuon==0.0_All;"
            },
            "2017": {
                "FileLabel": "#PIDK Kaon 2017",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_DLLK>10.0&&IsMuon==0.0_All;"
            },
            "2018": {
                "FileLabel": "#PIDK Kaon 2018",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_DLLK>10.0&&IsMuon==0.0_All;"
            },
        },
        "PIDBachMisID": {
            "2011": {
                "FileLabel": "#PIDK Pion 2011",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_DLLK>10.0&&IsMuon==0.0_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Pion 2012",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_DLLK>10.0&&IsMuon==0.0_All;"
            },
            "2015": {
                "FileLabel": "#PIDK Pion 2015",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_DLLK>10.0&&IsMuon==0.0_All;"
            },
            "2016": {
                "FileLabel": "#PIDK Pion 2016",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_DLLK>10.0&&IsMuon==0.0_All;"
            },
            "2017": {
                "FileLabel": "#PIDK Pion 2017",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_DLLK>10.0&&IsMuon==0.0_All;"
            },
            "2018": {
                "FileLabel": "#PIDK Pion 2018",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_DLLK>10.0&&IsMuon==0.0_All;"
            },
        },
        "PIDBachProtonMisID": {
            "2011": {
                "FileLabel": "#PIDK Proton 2011",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "P_TotLc_DLLK>10.0&&IsMuon==0.0_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Proton 2012",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "P_TotLc_DLLK>10.0&&IsMuon==0.0_All;"
            },
            "2015": {
                "FileLabel": "#PIDK Proton 2015",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "P_TotLc_DLLK>10.0&&IsMuon==0.0_All;"
            },
            "2016": {
                "FileLabel": "#PIDK Proton 2016",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "P_TotLc_DLLK>10.0&&IsMuon==0.0_All;"
            },
            "2017": {
                "FileLabel": "#PIDK Proton 2017",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "P_TotLc_DLLK>10.0&&IsMuon==0.0_All;"
            },
            "2018": {
                "FileLabel": "#PIDK Proton 2018",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "P_TotLc_DLLK>10.0&&IsMuon==0.0_All;"
            },
        },
        "PIDChildKaonPionMisID": {
            "2011": {
                "FileLabel": "#PIDK Pion 2011",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "Pi_DLLK>10.0&&IsMuon==0.0_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Pion 2012",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "Pi_DLLK>10.0&&IsMuon==0.0_All;"
            },
            "2015": {
                "FileLabel": "#PIDK Pion 2015",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "Pi_DLLK>10.0&&IsMuon==0.0_All;"
            },
            "2016": {
                "FileLabel": "#PIDK Pion 2016",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "Pi_DLLK>10.0&&IsMuon==0.0_All;"
            },
            "2017": {
                "FileLabel": "#PIDK Pion 2017",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "Pi_DLLK>10.0&&IsMuon==0.0_All;"
            },
            "2018": {
                "FileLabel": "#PIDK Pion 2018",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "Pi_DLLK>10.0&&IsMuon==0.0_All;"
            },
        },
        "PIDChildProtonMisID": {
            "2011": {
                "FileLabel": "#PIDK Proton 2011",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName":
                "P_TotLc_DLLK>5.0&&(DLLK-DLLp>5.0)&&IsMuon==0.0_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Proton 2012",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName":
                "P_TotLc_DLLK>5.0&&(DLLK-DLLp>5.0)&&IsMuon==0.0_All;"
            },
            "2015": {
                "FileLabel": "#PIDK Proton 2015",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName":
                "P_TotLc_DLLK>5.0&&(DLLK-DLLp>5.0)&&IsMuon==0.0_All;"
            },
            "2016": {
                "FileLabel": "#PIDK Proton 2016",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName":
                "P_TotLc_DLLK>5.0&&(DLLK-DLLp>5.0)&&IsMuon==0.0_All;"
            },
            "2017": {
                "FileLabel": "#PIDK Proton 2017",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName":
                "P_TotLc_DLLK>5.0&&(DLLK-DLLp>5.0)&&IsMuon==0.0_All;"
            },
            "2018": {
                "FileLabel": "#PIDK Proton 2018",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName":
                "P_TotLc_DLLK>5.0&&(DLLK-DLLp>5.0)&&IsMuon==0.0_All;"
            },
        },
        "RatioDataMC": {
            "2011": {
                "FileLabel": "#DataMC 2011",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            },
            "2012": {
                "FileLabel": "#DataMC 2012",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            },
            "2015": {
                "FileLabel": "#DataMC 2015",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            },
            "2016": {
                "FileLabel": "#DataMC 2016",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            },
            "2017": {
                "FileLabel": "#DataMC 2017",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            },
            "2018": {
                "FileLabel": "#DataMC 2018",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            },
        },
        "Shift": {
            "2011": {
                "BeautyMass": 0.0,
                "CharmMass": 0.0
            },
            "2012": {
                "BeautyMass": 0.0,
                "CharmMass": 0.0
            },
            "2015": {
                "BeautyMass": -1.0,
                "CharmMass": 0.0
            },
            "2016": {
                "BeautyMass": -1.1,
                "CharmMass": 0.0
            },
            "2017": {
                "BeautyMass": -0.95,
                "CharmMass": 0.0
            },
            "2018": {
                "BeautyMass": -1.65,
                "CharmMass": 0.0
            },
        }
    }

    #############################################################################################################
    #################################                FITTING                #####################################
    #############################################################################################################

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"][
        "type"] = "DoubleCrystalBallWithWidthRatio"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run2": {
            "All": 5367.51
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["sigma1"] = {
        "Run2": {
            "All": 17.432
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["sigma2"] = {
        "Run2": {
            "All": 11.210
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["alpha1"] = {
        "Run2": {
            "All": -2.1016
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["alpha2"] = {
        "Run2": {
            "All": 2.3520
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run2": {
            "All": 2.7904
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run2": {
            "All": 0.61148
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["frac"] = {
        "Run2": {
            "All": 0.50878
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["R"] = {
        "Run2": {
            "All": 1.00000
        },
        "Fixed": False
    }

    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "DoubleExponential"
    configdict["CombBkgShape"]["BeautyMass"]["cB1"] = {
        "Run2": {
            "All": -3.5211e-02
        },
        "Fixed": False
    }
    configdict["CombBkgShape"]["BeautyMass"]["cB2"] = {
        "Run2": {
            "All": -3.5211e-02
        },
        "Fixed": False
    }
    configdict["CombBkgShape"]["BeautyMass"]["frac"] = {
        "Run2": {
            "All": 4.3067e-01
        },
        "Fixed": False
    }

    configdict["AdditionalParameters"] = {}
    configdict["AdditionalParameters"]["g1_f1_frac"] = {
        "Run1": {
            "All": {
                "Both": {
                    "CentralValue": 0.5,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.5,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }
    configdict["AdditionalParameters"]["g1_f2_frac"] = {
        "Run1": {
            "All": {
                "Both": {
                    "CentralValue": 0.5,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.5,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }
    configdict["AdditionalParameters"]["g1_f3_frac"] = {
        "Run1": {
            "All": {
                "Both": {
                    "CentralValue": 0.5,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.5,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }
    configdict["AdditionalParameters"]["g2_f1_frac"] = {
        "Run1": {
            "All": {
                "Both": {
                    "CentralValue": 0.6,
                    "Range": [0.5, 1.0]
                }
            }
        },
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.6,
                    "Range": [0.5, 1.0]
                }
            }
        },
        "Fixed": False
    }
    configdict["AdditionalParameters"]["g2_f2_frac"] = {
        "Run1": {
            "All": {
                "Both": {
                    "CentralValue": 0.5,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.5,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }
    configdict["AdditionalParameters"]["g2_f3_frac"] = {
        "Run1": {
            "All": {
                "Both": {
                    "CentralValue": 0.5,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.5,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }
    configdict["AdditionalParameters"]["g3_f1_frac"] = {
        "Run1": {
            "All": {
                "Both": {
                    "CentralValue": 0.75,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.75,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": True
    }
    configdict["AdditionalParameters"]["g5_f1_frac"] = {
        "Run1": {
            "All": {
                "Both": {
                    "CentralValue": 0.9,
                    "Range": [0.8, 1.0]
                }
            }
        },
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.9,
                    "Range": [0.8, 1.0]
                }
            }
        },
        "Fixed": False
    }

    configdict["Bd2DsKShape"] = {}
    configdict["Bd2DsKShape"]["BeautyMass"] = {}
    if configdict["SignalShape"]["BeautyMass"][
            "type"] == "IpatiaJohnsonSUWithWidthRatio":
        configdict["Bd2DsKShape"]["BeautyMass"][
            "type"] = "ShiftedSignalIpatiaJohnsonSU"
        configdict["Bd2DsKShape"]["BeautyMass"]["shift"] = {
            "Run2": {
                "All": -86.8
            },
            "Fixed": True
        }
    else:
        configdict["Bd2DsKShape"]["BeautyMass"]["type"] = "ShiftedSignal"
        configdict["Bd2DsKShape"]["BeautyMass"]["shift"] = {
            "Run2": {
                "All": 86.8
            },
            "Fixed": True
        }
    configdict["Bd2DsKShape"]["BeautyMass"]["scale1"] = {
        "Run2": {
            "All": 1.00808721452
        },
        "Fixed": True
    }
    configdict["Bd2DsKShape"]["BeautyMass"]["scale2"] = {
        "Run2": {
            "All": 1.03868673310
        },
        "Fixed": True
    }

    #define yields
    configdict["Yields"] = {}

    configdict["Yields"]["Bd2DPi"] = {
        "2011": {
            "KKPi": 0.0
        },
        "2012": {
            "KKPi": 0.0
        },
        "2015": {
            "KKPi": 0.0
        },
        "2016": {
            "KKPi": 0.0
        },
        "2017": {
            "KKPi": 0.0
        },
        "2018": {
            "KKPi": 0.0
        },
        "Fixed": True
    }
    configdict["Yields"]["Bd2DK"] = {
        "2011": {
            "KKPi": 0.0
        },
        "2012": {
            "KKPi": 0.0
        },
        "2015": {
            "KKPi": 0.0
        },
        "2016": {
            "KKPi": 0.0
        },
        "2017": {
            "KKPi": 0.0
        },
        "2018": {
            "KKPi": 0.0
        },
        "Fixed": True
    }
    configdict["Yields"]["Lb2LcPi"] = {
        "2011": {
            "KKPi": 0.0
        },
        "2012": {
            "KKPi": 0.0
        },
        "2015": {
            "KKPi": 0.0
        },
        "2016": {
            "KKPi": 0.0
        },
        "2017": {
            "KKPi": 0.0
        },
        "2018": {
            "KKPi": 0.0
        },
        "Fixed": True
    }
    configdict["Yields"]["Lb2LcK"] = {
        "2011": {
            "KKPi": 0.0
        },
        "2012": {
            "KKPi": 0.0
        },
        "2015": {
            "KKPi": 0.0
        },
        "2016": {
            "KKPi": 0.0
        },
        "2017": {
            "KKPi": 0.0
        },
        "2018": {
            "KKPi": 0.0
        },
        "Fixed": True
    }

    configdict["Yields"]["Bs2DsDsstKKst"] = {
        "2011": {
            "KKPi": 5000.0
        },
        "2012": {
            "KKPi": 5000.0
        },
        "2015": {
            "KKPi": 5000.0
        },
        "2016": {
            "KKPi": 5000.0
        },
        "2017": {
            "KKPi": 5000.0
        },
        "2018": {
            "KKPi": 5000.0
        },
        "Fixed": False
    }
    configdict["Yields"]["BsLb2DsDsstPPiRho"] = {
        "2011": {
            "KKPi": 5000.0
        },
        "2012": {
            "KKPi": 5000.0
        },
        "2015": {
            "KKPi": 5000.0
        },
        "2016": {
            "KKPi": 5000.0
        },
        "2017": {
            "KKPi": 5000.0
        },
        "2018": {
            "KKPi": 5000.0
        },
        "Fixed": False
    }

    configdict["Yields"]["Signal"] = {
        "2011": {
            "KKPi": 5000.0
        },
        "2012": {
            "KKPi": 5000.0
        },
        "2015": {
            "KKPi": 5000.0
        },
        "2016": {
            "KKPi": 5000.0
        },
        "2017": {
            "KKPi": 5000.0
        },
        "2018": {
            "KKPi": 5000.0
        },
        "Fixed": False
    }
    configdict["Yields"]["CombBkg"] = {
        "2011": {
            "KKPi": 5000.0
        },
        "2012": {
            "KKPi": 5000.0
        },
        "2015": {
            "KKPi": 5000.0
        },
        "2016": {
            "KKPi": 5000.0
        },
        "2017": {
            "KKPi": 5000.0
        },
        "2018": {
            "KKPi": 5000.0
        },
        "Fixed": False
    }

    #############################################################################################################
    ################################              Plotting              #########################################
    #############################################################################################################

    from ROOT import *
    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = {
        "EPDF": [
            "Sig", "CombBkg", "Lb2LcK", "Lb2LcPi", "Bd2DK", "Bd2DPi",
            "BsLb2DsDsstPPiRho", "Bs2DsDsstKKst"
        ],
        "PDF": [
            "Sig", "CombBkg", "Lb2LcK", "Lb2LcPi", "Lb2DsDsstP",
            "Bs2DsDsstPiRho", "Bd2DK", "Bd2DPi", "Bs2DsDsstKKst"
        ],
        "Legend": [
            "Sig", "CombBkg", "Lb2LcKPi", "Lb2DsDsstP", "Bs2DsDsstPiRho",
            "Bd2DKPi", "Bs2DsDsstKKst"
        ]
    }
    configdict["PlotSettings"]["colors"] = {
        "PDF": [
            kRed - 7, kMagenta - 2, kGreen - 3, kGreen - 3, kYellow - 9,
            kBlue - 6, kRed, kRed, kBlue - 10
        ],
        "Legend": [
            kRed - 7, kMagenta - 2, kGreen - 3, kYellow - 9, kBlue - 6, kRed,
            kBlue - 10
        ]
    }

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.75, 0.9]
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.5,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.5,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075
    }

    return configdict
