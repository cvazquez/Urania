###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    from Lb2DspConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5100, 6200],
        "InputName": "lab0_MassHypo_Dsp"
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5"
    }

    configdict["AdditionalCuts"]["All"] = {
        "Data":
        "lab2_FDCHI2_ORIVX > 2 && lab2_TAU>0 && (lab1_isMuon==0)&&(lab1_ProbNNp>0.7)"
        + HLTcut + kinCuts,
        "MC":
        "lab2_FDCHI2_ORIVX > 2 && lab2_TAU>0 && lab1_M<200" + HLTcut + kinCuts,
        "MCID":
        True,
        "MCTRUEID":
        True,
        "BKGCAT":
        False,
        "DsHypo":
        True
    }

    ### Weighting MC samples ###
    configdict["WeightingMassTemplates"] = {
        "PIDBachEff": {
            "2011": {
                "FileLabel": "#PIDK Proton 2011",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "P_MC12TuneV2_ProbNNp>0.7_All"
            },
            "2012": {
                "FileLabel": "#PIDK Proton 2012",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "P_MC12TuneV2_ProbNNp>0.7_All"
            },
            "2015": {
                "FileLabel": "#PIDK Proton 2015",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "P_MC15TuneV1_ProbNNp>0.9_All"
            },
            "2016": {
                "FileLabel": "#PIDK Proton 2016",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "P_MC15TuneV1_ProbNNp>0.9_All"
            },
            "2017": {
                "FileLabel": "#PIDK Proton 2016",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "P_MC15TuneV1_ProbNNp>0.9_All"
            },
            "2018": {
                "FileLabel": "#PIDK Proton 2016",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "P_MC15TuneV1_ProbNNp>0.9_All"
            },
        },
        "PIDBachPion": {
            "2011": {
                "FileLabel": "#PIDK Pion 2011",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "Pi_MC12TuneV2_ProbNNp>0.7_All"
            },
            "2012": {
                "FileLabel": "#PIDK Pion 2012",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "Pi_MC12TuneV2_ProbNNp>0.7_All"
            },
            "2015": {
                "FileLabel": "#PIDK Pion 2015",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "Pi_MC15TuneV1_ProbNNp>0.9_All"
            },
            "2016": {
                "FileLabel": "#PIDK Pion 2016",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "Pi_MC15TuneV1_ProbNNp>0.9_All"
            },
            "2017": {
                "FileLabel": "#PIDK Pion 2016",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "Pi_MC15TuneV1_ProbNNp>0.9_All"
            },
            "2018": {
                "FileLabel": "#PIDK Pion 2016",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "Pi_MC15TuneV1_ProbNNp>0.9_All"
            },
        },
        "PIDBachKaon": {
            "2011": {
                "FileLabel": "#PIDK Kaon 2011",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "K_MC12TuneV2_ProbNNp>0.7_All"
            },
            "2012": {
                "FileLabel": "#PIDK Kaon 2012",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "K_MC12TuneV2_ProbNNp>0.7_All"
            },
            "2015": {
                "FileLabel": "#PIDK Kaon 2015",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "K_MC15TuneV1_ProbNNp>0.9_All"
            },
            "2016": {
                "FileLabel": "#PIDK Kaon 2016",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "K_MC15TuneV1_ProbNNp>0.9_All"
            },
            "2017": {
                "FileLabel": "#PIDK Kaon 2016",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "K_MC15TuneV1_ProbNNp>0.9_All"
            },
            "2018": {
                "FileLabel": "#PIDK Kaon 2016",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "K_MC15TuneV1_ProbNNp>0.9_All"
            },
        },
        "RatioDataMC": {
            "2011": {
                "FileLabel": "#DataMC 2011",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            },
            "2012": {
                "FileLabel": "#DataMC 2012",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            },
            "2015": {
                "FileLabel": "#DataMC 2015",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            },
            "2016": {
                "FileLabel": "#DataMC 2016",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            },
            "2017": {
                "FileLabel": "#DataMC 2017",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            },
            "2018": {
                "FileLabel": "#DataMC 2018",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            },
        },
        "Shift": {
            "2011": {
                "BeautyMass": -2.0,
                "CharmMass": 0.0
            },
            "2012": {
                "BeautyMass": -2.0,
                "CharmMass": 0.0
            },
            "2015": {
                "BeautyMass": -1.0,
                "CharmMass": 0.0
            },
            "2016": {
                "BeautyMass": -1.1,
                "CharmMass": 0.0
            },
            "2017": {
                "BeautyMass": -0.95,
                "CharmMass": 0.0
            },
            "2018": {
                "BeautyMass": -1.65,
                "CharmMass": 0.0
            },
        }
    }

    #############################################################################################################
    #################################                FITTING                #####################################
    #############################################################################################################

    # Bs signal shapes
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run1": {
            "All": 5619.58
        },
        "Run2": {
            "All": 5619.58
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "Run1": {
            "All": 25.370
        },
        "Run2": {
            "All": 25.370
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "Run1": {
            "All": 15.878
        },
        "Run2": {
            "All": 15.878
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "Run1": {
            "All": 1.1415
        },
        "Run2": {
            "All": 1.1415
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "Run1": {
            "All": 2.4306
        },
        "Run2": {
            "All": 2.4306
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run1": {
            "All": 2.4306
        },
        "Run2": {
            "All": 2.4306
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run1": {
            "All": 2.0552
        },
        "Run2": {
            "All": 2.0552
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "Run1": {
            "All": 0.0
        },
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "Run1": {
            "All": 0.0
        },
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "Run1": {
            "All": -1.6158
        },
        "Run2": {
            "All": -1.6158
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "Run1": {
            "All": 0.42996
        },
        "Run2": {
            "All": 0.42996
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "Run1": {
            "All": -0.35069
        },
        "Run2": {
            "All": -0.35069
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "Run1": {
            "All": 0.37615
        },
        "Run2": {
            "All": 0.37615
        },
        "Fixed": False
    }

    #define yields
    configdict["Yields"] = {}
    configdict["Yields"]["Signal"] = {
        "2011": {
            "KKPi": 5000.0
        },
        "2012": {
            "KKPi": 5000.0
        },
        "2015": {
            "KKPi": 5000.0
        },
        "2016": {
            "KKPi": 5000.0
        },
        "2017": {
            "KKPi": 5000.0
        },
        "2018": {
            "KKPi": 5000.0
        },
        "Fixed": False
    }

    #############################################################################################################
    ################################              Plotting              #########################################
    #############################################################################################################

    from ROOT import *
    configdict["PlotSettings"] = {}
    # Standard
    #configdict["PlotSettings"]["components"] = ["Sig", "CombBkg", "Lb2Dsstp", "Bs2DsstKKstPiRho"]
    #configdict["PlotSettings"]["colors"] = [kRed-7, kBlue-6, kOrange, kRed, kBlue-10, kYellow, kBlue+2]

    configdict["PlotSettings"]["components"] = {
        "EPDF": ["Sig", "CombBkg", "Lb2Dsstp", "Bs2DsDsstKKstPiRho"],
        "PDF":
        ["Sig", "CombBkg", "Lb2Dsstp", "Bs2DsDsstPiRho", "Bs2DsDsstKKst"],
        "Legend":
        ["Sig", "CombBkg", "Lb2Dsstp", "Bs2DsDsstPiRho", "Bs2DsDsstKKst"]
    }
    configdict["PlotSettings"]["colors"] = {
        "PDF": [
            kRed - 7, kBlue - 6, kOrange, kMagenta + 2, kBlue - 10, kYellow,
            kBlue + 2
        ],
        "Legend": [
            kRed - 7, kBlue - 6, kOrange, kMagenta + 2, kBlue - 10, kYellow,
            kBlue + 2
        ]
    }

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.35, 0.9],
        "ScaleYSize": 1.2
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.7,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.35, 0.9],
        "ScaleYSize": 1.2
    }

    return configdict
