###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    configdict = {}

    from math import pi
    from math import log

    # considered decay mode
    configdict["Decay"] = "Lb2Dsp"
    configdict["CharmModes"] = {"KKPi"}
    configdict["Backgrounds"] = [
        "Bs2DsPi", "Bs2DsstPi", "Bs2DsRho", "Bs2DsK", "Bs2DsstK", "Bs2DsKst",
        "Lb2Dsstp"
    ]

    # year of data taking
    configdict["YearOfDataTaking"] = {"2015", "2016", "2017", "2018"}
    # integrated luminosity in each year of data taking (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {
        "2011": {
            "Down": 0.5600,
            "Up": 0.4200
        },
        "2012": {
            "Down": 0.9912,
            "Up": 0.9988
        },
        "2015": {
            "Down": 0.18695,
            "Up": 0.14105
        },
        "2016": {
            "Down": 0.85996,
            "Up": 0.80504
        },
        "2017": {
            "Down": 0.87689,
            "Up": 0.83311
        },
        "2018": {
            "Down": 1.04846,
            "Up": 1.14154
        },
    }

    # stripping (necessary in case of PIDK shapes)
    configdict["dataName"] = "../data/Lb2Dsp_9fbBR/Lb2Dsp/config_Lb2Dsp.txt"
    #settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {
        "Directory": "PlotLb2Dsp",
        "Extension": "pdf"
    }

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5200, 6200],
        "InputName": "lab0_MassHypo_Dsp"
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1950, 1990],
        "InputName": "lab2_MM"
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-150.0, 150.0],
        "InputName": "lab1_ProbNNp"
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [10000.0, 300000.0],
        "InputName": "lab1_P"
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [400.0, 45000.0],
        "InputName": "lab1_PT"
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 500.0],
        "InputName": "nTracks"
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID"
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.4, 1.0],
        "InputName": "BDTGResponse_3"
    }

    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data": "(lab1_ProbNNp>0.9)",
        #"MC"  : "lab1_M<200&&lab2_FD_ORIVX > 0.&&(lab0_LifetimeFit_Dplus_ctau[0]>0)",
        "MCID": True,
        "MCTRUEID": True,
        "BKGCAT": False,
        "DsHypo": True
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5"
    }

    #############################################################################################################
    #################################                FITTING                #####################################
    #############################################################################################################

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"][
        "type"] = "DoubleCrystalBallWithWidthRatio"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run1": {
            "All": 5619.58
        },
        "Run2": {
            "All": 5619.58
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["sigma1"] = {
        "Run1": {
            "All": 16.3671
        },
        "Run2": {
            "All": 16.3671
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["sigma2"] = {
        "Run1": {
            "All": 10.8735
        },
        "Run2": {
            "All": 10.8735
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run1": {
            "All": 2.82686
        },
        "Run2": {
            "All": 2.82686
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run1": {
            "All": 0.40732
        },
        "Run2": {
            "All": 0.40732
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["alpha1"] = {
        "Run1": {
            "All": -2.25801
        },
        "Run2": {
            "All": -2.25801
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["alpha2"] = {
        "Run1": {
            "All": 2.34818
        },
        "Run2": {
            "All": 2.34818
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["frac"] = {
        "Run1": {
            "All": 0.5
        },
        "Run2": {
            "All": 0.5
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["R"] = {
        "Run1": {
            "All": 1.06
        },
        "Run2": {
            "All": 1.06
        },
        "Fixed": False
    }

    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "Exponential"
    configdict["CombBkgShape"]["BeautyMass"]["cB"] = {
        "Run1": {
            "All": -8.5211e-03
        },
        "Run2": {
            "All": -8.5211e-03
        },
        "Fixed": False
    }

    configdict["AdditionalParameters"] = {}
    configdict["AdditionalParameters"]["g1_f1_frac"] = {
        "Run1": {
            "All": {
                "Both": {
                    "CentralValue": 1.0,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 1.0,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }
    configdict["AdditionalParameters"]["g1_f2_frac"] = {
        "Run1": {
            "All": {
                "Both": {
                    "CentralValue": 1.0,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 1.0,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }
    configdict["AdditionalParameters"]["g2_f1_frac"] = {
        "Run1": {
            "All": {
                "Both": {
                    "CentralValue": 1.0,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 1.0,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }
    configdict["AdditionalParameters"]["g2_f2_frac"] = {
        "Run1": {
            "All": {
                "Both": {
                    "CentralValue": 1.0,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 1.0,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }
    configdict["AdditionalParameters"]["g3_f1_frac"] = {
        "Run1": {
            "All": {
                "Both": {
                    "CentralValue": 1.0,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 1.0,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }

    #define yields
    configdict["Yields"] = {}

    configdict["Yields"]["Bs2DsDsstKKstPiRho"] = {
        "2011": {
            "KKPi": 5000.0
        },
        "2012": {
            "KKPi": 5000.0
        },
        "2015": {
            "KKPi": 5000.0
        },
        "2016": {
            "KKPi": 5000.0
        },
        "2017": {
            "KKPi": 5000.0
        },
        "2018": {
            "KKPi": 5000.0
        },
        "Fixed": False
    }
    configdict["Yields"]["Lb2Dsstp"] = {
        "2011": {
            "KKPi": 5000.0
        },
        "2012": {
            "KKPi": 5000.0
        },
        "2015": {
            "KKPi": 5000.0
        },
        "2016": {
            "KKPi": 5000.0
        },
        "2017": {
            "KKPi": 5000.0
        },
        "2018": {
            "KKPi": 5000.0
        },
        "Fixed": False
    }
    #configdict["Yields"]["Bs2DsPi"]    = {"2011": {"KKPi":5000.0}, "2012": {"KKPi":5000.0},
    #                                      "2015": {"KKPi":5000.0}, "2016": {"KKPi":5000.0}, "Fixed":False}
    #configdict["Yields"]["Bs2DsstPi"]  = {"2011": {"KKPi":50.0}, "2012": {"KKPi":50.0},
    #                                      "2015": {"KKPi":5000.0}, "2016": {"KKPi":5000.0}, "Fixed":False}
    #configdict["Yields"]["Bs2DsK"]     = {"2011": {"KKPi":5000.0}, "2012": {"KKPi":5000.0},
    #                                      "2015": {"KKPi":5000.0}, "2016": {"KKPi":5000.0}, "Fixed":False}
    configdict["Yields"]["Signal"] = {
        "2011": {
            "KKPi": 5000.0
        },
        "2012": {
            "KKPi": 5000.0
        },
        "2015": {
            "KKPi": 5000.0
        },
        "2016": {
            "KKPi": 5000.0
        },
        "2017": {
            "KKPi": 5000.0
        },
        "2018": {
            "KKPi": 5000.0
        },
        "Fixed": False
    }
    configdict["Yields"]["CombBkg"] = {
        "2011": {
            "KKPi": 5000.0
        },
        "2012": {
            "KKPi": 5000.0
        },
        "2015": {
            "KKPi": 5000.0
        },
        "2016": {
            "KKPi": 5000.0
        },
        "2017": {
            "KKPi": 5000.0
        },
        "2018": {
            "KKPi": 5000.0
        },
        "Fixed": False
    }

    #############################################################################################################
    ################################              Plotting              #########################################
    #############################################################################################################

    from ROOT import *
    configdict["PlotSettings"] = {}
    # Standard
    #configdict["PlotSettings"]["components"] = ["Sig", "CombBkg", "Lb2Dsstp", "Bs2DsstKKstPiRho"]
    #configdict["PlotSettings"]["colors"] = [kRed-7, kBlue-6, kOrange, kRed, kBlue-10, kYellow, kBlue+2]

    configdict["PlotSettings"]["components"] = {
        "EPDF": ["Sig", "CombBkg", "Lb2Dsstp", "Bs2DsDsstKKstPiRho"],
        "PDF":
        ["Sig", "CombBkg", "Lb2Dsstp", "Bs2DsDsstPiRho", "Bs2DsDsstKKst"],
        "Legend":
        ["Sig", "CombBkg", "Lb2Dsstp", "Bs2DsDsstPiRho", "Bs2DsDsstKKst"]
    }
    configdict["PlotSettings"]["colors"] = {
        "PDF": [
            kRed - 7, kBlue - 6, kOrange, kMagenta + 2, kBlue - 10, kYellow,
            kBlue + 2
        ],
        "Legend": [
            kRed - 7, kBlue - 6, kOrange, kMagenta + 2, kBlue - 10, kYellow,
            kBlue + 2
        ]
    }

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.75, 0.9],
        "ScaleYSize": 1.2
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.7,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.35, 0.9],
        "ScaleYSize": 1.2
    }

    return configdict
