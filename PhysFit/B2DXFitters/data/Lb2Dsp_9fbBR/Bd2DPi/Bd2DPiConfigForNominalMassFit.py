def getconfig() :

    configdict = {}
    
    from math import pi
    from math import log

    # considered decay mode                                                       
    configdict["Decay"] = "Bd2DPi"
    configdict["CharmModes"] = {"KPiPi"}
    configdict["Backgrounds"] = ["Bd2DK","Bd2DRho","Bd2DstPi","Lb2LcPi","Bs2DsPi"]
    # year of data taking                                                                                                                          
    configdict["YearOfDataTaking"] = {"2015", "2016"}
    # stripping (necessary in case of PIDK shapes)                                                                                              
  #  configdict["Stripping"] = {"2012":"21", "2011":"21r1"}
    # integrated luminosity in each year of data taking (necessary in case of PIDK shapes)                                                                  
    configdict["IntegratedLuminosity"] = {"2011": {"Down": 0.5600, "Up": 0.4200},
                                          "2012": {"Down": 0.9912, "Up": 0.9988},
                                          "2015": {"Down": 0.1540, "Up": 0.1740}, #to be checked, total 0.328, not sure about polarity split                                          
                                          "2016": {"Down": 0.8325, "Up": 0.8325}  #to be checked, total 1.665, not sure about polarity split                                        
                                          }

    # file name with paths to MC/data samples                                                                                                       
    configdict["dataName"]   = "../data/Lb2Dsp_5fbBR/Bd2DPi/config_Bd2DPi.txt"
    #settings for control plots                                                                                                                                                           
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = { "Directory": "PlotBd2DPi", "Extension":"pdf"}

    # basic variables                                                                                        
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"]    = { "Range" : [5000,    6000    ], "InputName" : "lab0_MassFitConsD_M"}
    #configdict["BasicVariables"]["BeautyMass"]    = { "Range" : [5200,    6000    ], "InputName" : "lab0_MassFitConsD_M"} #Stefano: narrow
    configdict["BasicVariables"]["CharmMass"]     = { "Range" : [1830,    1920    ], "InputName" : "lab2_MM"}
    configdict["BasicVariables"]["BeautyTime"]    = { "Range" : [0.4,     15.0    ], "InputName" : "lab0_LifetimeFit_ctau"}
    configdict["BasicVariables"]["BacP"]          = { "Range" : [3000.0,  650000.0], "InputName" : "lab1_P"}
    configdict["BasicVariables"]["BacPT"]         = { "Range" : [400.0,   45000.0 ], "InputName" : "lab1_PT"}
    configdict["BasicVariables"]["BacPIDK"]       = { "Range" : [-150.0,  150.0   ], "InputName" : "lab1_PIDK"}
    configdict["BasicVariables"]["nTracks"]       = { "Range" : [15.0,    1000.0  ], "InputName" : "nTracks"}
    configdict["BasicVariables"]["BeautyTimeErr"] = { "Range" : [0.01,    0.1     ], "InputName" : "lab0_LifetimeFit_ctauErr"}
    configdict["BasicVariables"]["BacCharge"]     = { "Range" : [-1000.0, 1000.0  ], "InputName" : "lab1_ID"}
    configdict["BasicVariables"]["BDTG"]          = { "Range" : [0.5,     1.0     ], "InputName" : "BDTGResponse_3"} #Stefano (use new BDTG)

    LbVeto1 = "!(fabs(lab2_MassHypo_Lambda_pi1 - 2286.4) < 30. && lab4_PIDp > 0)"
    LbVeto2 = "!(fabs(lab2_MassHypo_Lambda_pi2 - 2286.4) < 30. && lab5_PIDp > 0)"
    DsVeto1 = "(lab2_MassHypo_Ds_pi1 < 1949. || lab2_MassHypo_Ds_pi1 > 2029. || lab4_PIDK < 0.)"
    DsVeto2 = "(lab2_MassHypo_Ds_pi2 < 1949. || lab2_MassHypo_Ds_pi2 > 2029. || lab5_PIDK < 0.)"
    D0KPiVeto1 = "!(abs(sqrt(pow(sqrt(pow(493.67,2)+pow(lab1_P,2))+sqrt(pow(lab5_M,2)+pow(lab5_P,2)),2)-pow(lab1_PX+lab5_PX,2)-pow(lab1_PY+lab5_PY,2)-pow(lab1_PZ+lab5_PZ,2))-1870)<20)";
    DoKPiVeto2 = "!(abs(sqrt(pow(sqrt(pow(493.67,2)+pow(lab1_P,2))+sqrt(pow(lab4_M,2)+pow(lab4_P,2)),2)-pow(lab1_PX+lab4_PX,2)-pow(lab1_PY+lab4_PY,2)-pow(lab1_PZ+lab4_PZ,2))-1870)<20)"; 
    DstarPiVeto = "(lab2_MM-lab34_MM > 200) && (lab2_MM-lab35_MM > 200)";
    app = "&&"
    AllVetoes = LbVeto1 + app + LbVeto2 + app + DsVeto1 + app +DsVeto2 + app + D0KPiVeto1 + app + DoKPiVeto2 + app + DstarPiVeto

    # additional cuts applied to data sets                                                                                    
    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"]    = { "Data": "lab2_TAU>0"+app+AllVetoes, "MC" : "lab2_TAU>0&&lab1_M<200", "MCID":True, "MCTRUEID":True, "BKGCAT":True, "DsHypo":True}
    configdict["AdditionalCuts"]["KPiPi"]  = { "Data": "lab2_FDCHI2_ORIVX > 9", "MC" : "lab2_FDCHI2_ORIVX > 9"}

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts                                                                                                              
    # order of particles: KKPi, KPiPi, PiPiPi                                                                                                         
    configdict["DsChildrenPrefix"] = {"Child1":"lab3","Child2":"lab4","Child3": "lab5"} #lab3 = K, lab4, lab5 = pi

    # weighting templates by PID eff/misID                                                                                                                                            

    LbVeto1 = "!(fabs(lab2_MassHypo_Lambda_pi1 - 2286.4) < 30. && lab4_PIDp > 0)"
    LbVeto2 = "!(fabs(lab2_MassHypo_Lambda_pi2 - 2286.4) < 30. && lab5_PIDp > 0)"
#srednia swiatowa 5279.32
#5.2804e+03
#    configdict["WeightingMassTemplates"] = {"Shift":{ "BeautyMass": (5279.63-5278.6188), "CharmMass": 0.0} } # Stefano (PID MC-reweight not applied)


    configdict["WeightingMassTemplates"] = { "RatioDataMC":{"2015":  {"FileLabel": "#Ratio_2015", "Var":["lab1_P", "nTracks"],"HistName":"ratio"}, "2016":  {"FileLabel": "#Ratio_2016", "Var":["lab1_P", "nTracks"],"HistName":"ratio"} }, "Shift": { "2015": {"BeautyMass": (5278.9-(5280.4+2.0)), "CharmMass": 0.0},"2016":{"BeautyMass": (5278.8-((5.2808e+03)+2.0)), "CharmMass": 0.0}}}

#h_ratio_p_up_2016.root
#5.2805e+03  z makro
#5.2808e+03 srednia z pol
#    configdict["WeightingMassTemplates"] = { "RatioDataMC":{"2015":  {"FileLabel": "#Ratio_2015", "Var":["lab1_P", #"nTracks"],"HistName":"ratio"}, "2016":  {"FileLabel": "#Ratio_2016", "Var":["lab1_P", "nTracks"],"HistName":"ratio"} }, "Shift": { "2015": #{"BeautyMass": {(5278.9-(5280.4+2.0))}},"2016":{"Up": {(5278.8-(5281.0+2))}, "Down": {(5278.8-(5281.0+2.0))} }}, "CharmMass": 0.0} }
 #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    ###                                                               MDfit fitting settings
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    # Stefano: updated to 2012 MC
    # Bs signal shapes 
    # up + dw            #DoubleCrystalBallWithWidthRatio       

    configdict["BsSignalShape"] = {} 
    configdict["BsSignalShape"]["type"]    = "DoubleCrystalBallWithWidthRatio"
    configdict["BsSignalShape"]["mean"]    = {"2015":{"All": 5.2804e+03}, 	"2016": {"All": {"Down": 5.2806e+03, "Up": 5.2806e+03}},  "Fixed": False}
    configdict["BsSignalShape"]["sigma1"]  = {"2015":{"KPiPi": 1.7908e+01}, 	"2016": {"KPiPi": {"Down":1.6454e+01, "Up": 1.6454e+01}},  "Fixed": True} 
    configdict["BsSignalShape"]["sigma2"]  = {"2015":{"KPiPi": 1.2079e+01}, 	"2016": {"KPiPi": {"Down":9.9247e+00, "Up": 9.9247e+00}},  "Fixed": True} 
    configdict["BsSignalShape"]["alpha1"]  = {"2015":{"KPiPi": -1.8073e+00}, 	"2016": {"KPiPi": {"Down":-1.9007e+00, "Up": -1.9007e+00}}, "Fixed": True}
    configdict["BsSignalShape"]["alpha2"]  = {"2015":{"KPiPi": 1.5441e+00}, 	"2016": {"KPiPi": {"Down":9.4300e-01 , "Up": 9.4300e-01}}, "Fixed": True}
    configdict["BsSignalShape"]["n1"]      = {"2015":{"KPiPi": 4.1222e+00}, 	"2016": {"KPiPi": {"Down":3.6250e+00, "Up": 3.6250e+00}}, "Fixed": True}
    configdict["BsSignalShape"]["n2"]      = {"2015":{"KPiPi": 1.4521e+00}, 	"2016": {"KPiPi":{"Down":1.9022e+00, "Up": 1.9022e+00}}, "Fixed": True}
    configdict["BsSignalShape"]["frac"]    = {"2015":{"KPiPi": 0.5}, 	"2016": {"KPiPi":{"Down":7.0000e-01, "Up": 7.0000e-01}}, "Fixed": True}
    configdict["BsSignalShape"]["R"]       = {"2015":{"KPiPi":1.0}, 	"2016": {"KPiPi":{"Down":1.0 , "Up":1.0}},   "Fixed": False}










#2015 frac = 5

#alpha1   -2.0000e+00   -1.8073e+00 +/-  5.72e-02  <none>
#                alpha2    2.0000e+00    1.5441e+00 +/-  4.46e-02  <none>
   #              mean1    5.2600e+03    5.2804e+03 +/-  6.33e-02  <none>
   #                 n1    2.0000/e+00    4.1222e+00 +/-  3.79e-01  <none>
   #                 n2    2.0000e-01    1.4521e+00 +/-  7.73e-02  <none>
   #             sigma1    1.5000e+01    1.7908e+01 +/-  2.61e-01  <none>
   #             sigma2    1.0000e+01    1.2079e+01 +/-  1.53e-01  <none>





                                                                                                               
#    configdict["BsSignalShape"] = {} 
#    configdict["BsSignalShape"]["type"]    = "DoubleGaussianWithWidthRatio"
#    configdict["BsSignalShape"]["mean"]    = {"Run2": {"All":5280.0},    "Fixed": False}
#   configdict["BsSignalShape"]["sigma1"]  = {"Run2": {"KPiPi":12.763},  "Fixed": True} 
#    configdict["BsSignalShape"]["sigma2"]  = {"Run2": {"KPiPi":21.389},  "Fixed": True} 
#    configdict["BsSignalShape"]["alpha1"]  = {"Run2": {"KPiPi":1.9990},  "Fixed": True}
#    configdict["BsSignalShape"]["alpha2"]  = {"Run2": {"KPiPi":-2.1618}, "Fixed": True}
#    configdict["BsSignalShape"]["n1"]      = {"Run2": {"KPiPi":1.0928},  "Fixed": True}
#    configdict["BsSignalShape"]["n2"]      = {"Run2": {"KPiPi":2.4010},  "Fixed": True}
#    configdict["BsSignalShape"]["frac"]    = {"Run2": {"KPiPi":0.68930}, "Fixed": True}
#    configdict["BsSignalShape"]["R"]       = {"Run2": {"KPiPi":1.0 },    "Fixed": False}

    # MC smeared (5000-5600 MeV)
    #configdict["BsSignalShape"]["mean"]    = {"Run1": {"All":5280.},     "Fixed": False}
    #configdict["BsSignalShape"]["sigma1"]  = {"Run1": {"KPiPi":15.483},  "Fixed": True} 
    #configdict["BsSignalShape"]["sigma2"]  = {"Run1": {"KPiPi":18.624},  "Fixed": True} 
    #configdict["BsSignalShape"]["alpha1"]  = {"Run1": {"KPiPi":1.3330},  "Fixed": True}
    #configdict["BsSignalShape"]["alpha2"]  = {"Run1": {"KPiPi":-1.2472}, "Fixed": True}
    #configdict["BsSignalShape"]["n1"]      = {"Run1": {"KPiPi":1.9093},  "Fixed": True}
    #configdict["BsSignalShape"]["n2"]      = {"Run1": {"KPiPi":5.1967},  "Fixed": True}
    #configdict["BsSignalShape"]["frac"]    = {"Run1": {"KPiPi":0.57147}, "Fixed": True}


    configdict["Bd2DstPiShape"] = {}                                                                                                                                                                           
    configdict["Bd2DstPiShape"] ["BeautyMass"]= {}
    configdict["Bd2DstPiShape"] ["BeautyMass"]["type"]  = "DoubleGaussianSeparatedMean"                                                                                                                                       
    configdict["Bd2DstPiShape"] ["BeautyMass"]["sigma1"]  = {"2016": {"KPiPi":15.0},          "Fixed":False}                                                                                                                  
    configdict["Bd2DstPiShape"] ["BeautyMass"]["sigma2"]  = {"2016": {"KPiPi":15.0},          "Fixed":False}                                                                                                                  
    configdict["Bd2DstPiShape"] ["BeautyMass"]["mean"]    = {"2016": {"KPiPi":5030.0},        "Fixed":False}                                                                                                                  
    configdict["Bd2DstPiShape"] ["BeautyMass"]["mean2"]   = {"2016": {"KPiPi":5110.0},        "Fixed":False}                                                                                                                  
    configdict["Bd2DstPiShape"] ["BeautyMass"]["frac"]   = {"2016": {"KPiPi":0.5},        "Fixed":False}


    # Ds signal shapes  
    # up + dw                                                                                                                                     
    configdict["DsSignalShape"] = {}
    configdict["DsSignalShape"]["type"]    = "DoubleCrystalBallWithWidthRatio"
    configdict["DsSignalShape"]["mean"]    = {"Run2": {"All":1869.8},       "Fixed": False}
    configdict["DsSignalShape"]["sigma1"]  = {"Run2": {"KPiPi":11.501},     "Fixed": True}
    configdict["DsSignalShape"]["sigma2"]  = {"Run2": {"KPiPi":6.1237},     "Fixed": True}
    configdict["DsSignalShape"]["alpha1"]  = {"Run2": {"KPiPi":1.6382},     "Fixed": True}
    configdict["DsSignalShape"]["alpha2"]  = {"Run2": {"KPiPi":-3.4683},    "Fixed": True}
    configdict["DsSignalShape"]["n1"]      = {"Run2": {"KPiPi":4.8678},     "Fixed": True}
    configdict["DsSignalShape"]["n2"]      = {"Run2": {"KPiPi":4.3285e-06}, "Fixed": True}
    configdict["DsSignalShape"]["frac"]    = {"Run2": {"KPiPi":0.38916},    "Fixed": True}
    configdict["DsSignalShape"]["R"]       = {"Run2": {"KPiPi":1.0 },       "Fixed": False}

    # combinatorial background






                                                                                                                              
    configdict["BsCombinatorialShape"] = {}
    configdict["BsCombinatorialShape"]["type"]  = "DoubleExponential"
    configdict["BsCombinatorialShape"]["cB1"]   = {"Run2": {"KPiPi":-1.0361e-03}, "Fixed": False}
    configdict["BsCombinatorialShape"]["cB2"]   = {"Run2": {"KPiPi":-0.01},       "Fixed": False}
    configdict["BsCombinatorialShape"]["frac"]  = {"Run2": {"KPiPi":0.5},         "Fixed": False}

    configdict["DsCombinatorialShape"] = {}
    configdict["DsCombinatorialShape"] = {}
    configdict["DsCombinatorialShape"]["type"]  = "ExponentialPlusDoubleCrystalBallWithWidthRatioSharedMean"
    configdict["DsCombinatorialShape"]["sigma1"]  = {"Run2": {"KPiPi":11.501},          "Fixed":True}
    configdict["DsCombinatorialShape"]["sigma2"]  = {"Run2": {"KPiPi":6.1237},          "Fixed":True}
    configdict["DsCombinatorialShape"]["alpha1"]  = {"Run2": {"KPiPi":1.6382},          "Fixed":True}
    configdict["DsCombinatorialShape"]["alpha2"]  = {"Run2": {"KPiPi":-3.4683},         "Fixed":True}
    configdict["DsCombinatorialShape"]["n1"]      = {"Run2": {"KPiPi":4.8678},          "Fixed":True}
    configdict["DsCombinatorialShape"]["n2"]      = {"Run2": {"KPiPi":4.3285e-06},      "Fixed":True}
    configdict["DsCombinatorialShape"]["frac"]    = {"Run2": {"KPiPi":0.38916},         "Fixed":True}
    configdict["DsCombinatorialShape"]["R"]       = {"Run2": {"KPiPi":1.5},             "Fixed":False}
    configdict["DsCombinatorialShape"]["cB"]      = {"Run2": {"KPiPi":-1.9193e-03},     "Fixed":False}
    configdict["DsCombinatorialShape"]["fracD"]   = {"Run2": {"KPiPi":0.5},             "Fixed":False}

    #configdict["DsCombinatorialShape"]["type"]  = "ExponentialPlusSignal" 
    #configdict["DsCombinatorialShape"]["type"]  = "ExponentialPlusSignal" 
    #configdict["DsCombinatorialShape"]["cD"]    = {"Run1": {"KPiPi":-3e-03},      "Fixed": False}
    #configdict["DsCombinatorialShape"]["fracD"] = {"Run1": {"KPiPi":0.5},         "Fixed": False}

    # Che cosa e'???
    configdict["AdditionalParameters"] = {}
    configdict["AdditionalParameters"]["g1_f1_frac"] = {"Run2":{"All":{"Both":{"CentralValue":0.5, "Range":[0.0,1.0]}}}, "Fixed": False} 
    configdict["AdditionalParameters"]["g1_f2_frac"] = {"Run2":{"All":{"Both":{"CentralValue":0.5, "Range":[0.0,1.0]}}}, "Fixed": False}

    # expected yields #Stefano: fix Bs2DsPi & Lb2LcPi 
    configdict["Yields"] = {}
    configdict["Yields"]["Bd2DK"]     = {"2015": {"KPiPi":15000.0},  "2016": {"KPiPi":30000.0},  "Fixed": False} 
    configdict["Yields"]["Bd2DRho"]   = {"2015": {"KPiPi":80000.0},  "2016": {"KPiPi":160000.0}, "Fixed": False}
    configdict["Yields"]["Bd2DstPi"]  = {"2015": {"KPiPi":60000.0},  "2016": {"KPiPi":120000.0}, "Fixed": False}
    configdict["Yields"]["Bs2DsPi"]   = {"2015": {"KPiPi":1000.0},   "2016": {"KPiPi":3000.0},   "Fixed": True}
    configdict["Yields"]["Lb2LcPi"]   = {"2015": {"KPiPi":250.0},    "2016": {"KPiPi":400.0},    "Fixed": True}
    configdict["Yields"]["CombBkg"]   = {"2015": {"KPiPi":30000.0},  "2016": {"KPiPi":60000.0},  "Fixed": False}
    configdict["Yields"]["Signal"]    = {"2015": {"KPiPi":150000.0}, "2016": {"KPiPi":300000.0}, "Fixed": False}


    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#    
    ###                                                               MDfit plotting settings                                                             
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#               
    from ROOT import *
    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = ["Sig", "CombBkg", "Bd2DK", "Lb2LcPi", "Bs2DsPi", "Bd2DRho", "Bd2DstPi"] 
    configdict["PlotSettings"]["colors"] = [kRed-7, kBlue-6, kOrange, kRed, kBlue-10, kYellow, kBlue+2]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {"Position":[0.53, 0.45, 0.90, 0.91], "TextSize": 0.05, "LHCbText":[0.35,0.9], "ScaleYSize":1.2}
    configdict["LegendSettings"]["CharmMass"]  = {"Position":[0.20, 0.69, 0.93, 0.93], "TextSize": 0.05, "LHCbText":[0.8,0.66],
                                                  "ScaleYSize":1.7, "SetLegendColumns":2, "LHCbTextSize":0.075 }
    configdict["LegendSettings"]["BacPIDK"]    = {"Position":[0.53, 0.45, 0.90, 0.91], "TextSize": 0.05, "LHCbText":[0.35,0.9], "ScaleYSize":1.2}

    return configdict
