###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import sys
import Bs2DsPiConfigForNominalDMSFitSplinesFloatAccSimFit


def getconfig():
    configdict = Bs2DsPiConfigForNominalDMSFitSplinesFloatAccSimFit.getconfig()

    configdict["SWeightCorrection"] = "Read"

    # Adjust only difference: FT Calibration
    configdict["TaggingCalibration"] = {
        "OS": {
            y: {
                "p0": 0.3467,
                "dp0": 0.0,
                "p1": 1.0,
                "dp1": 0.0,
                "average": 0.3467,
                "tagEff": 0.4106,
                "aTagEff": 0.0,
                "use": True,
            }
            for y in configdict["Labels"]
        },
        "SS": {
            y: {
                "p0": 0.4161,
                "dp0": 0.0,
                "p1": -0.21 + 1,
                "dp1": 0.0,
                "average": 0.4161,
                "tagEff": 0.6960,
                "aTagEff": 0.0,
                "use": True,
            }
            for y in configdict["Labels"]
        },
    }
    configdict["TaggingCalibration"]["OS"]["20152016"]["tagEff"] = 0.4126
    configdict["TaggingCalibration"]["OS"]["2017"]["tagEff"] = 0.4084
    configdict["TaggingCalibration"]["OS"]["2018"]["tagEff"] = 0.4123
    configdict["TaggingCalibration"]["SS"]["20152016"]["tagEff"] = 0.6918
    configdict["TaggingCalibration"]["SS"]["2017"]["tagEff"] = 0.6992
    configdict["TaggingCalibration"]["SS"]["2018"]["tagEff"] = 0.6973

    configdict["TaggingCalibration"]["OS"]["20152016"]["average"] = 0.3562
    configdict["TaggingCalibration"]["OS"]["2017"]["average"] = 0.3463
    configdict["TaggingCalibration"]["OS"]["2018"]["average"] = 0.3464
    configdict["TaggingCalibration"]["SS"]["20152016"]["average"] = 0.4162
    configdict["TaggingCalibration"]["SS"]["2017"]["average"] = 0.4164
    configdict["TaggingCalibration"]["SS"]["2018"]["average"] = 0.4156

    # float all asymmetries
    configdict["constParams"] = [
        c for c in configdict["constParams"]
        if not ("aprod" in c or "adet" in c)
    ]

    return configdict


if __name__ == '__main__':
    sys.path.insert(0, os.path.join(os.path.dirname(__file__), 'Toys'))
    from configutils import main
    main(getconfig())
