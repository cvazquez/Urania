###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
def getconfig():

    configdict = {}

    from math import pi
    from math import log

    # considered decay mode
    configdict["Decay"] = "Bs2DsPi"
    configdict["CharmModes"] = {"KstK", "NonRes", "PhiPi", "PiPiPi"}
    configdict["Backgrounds"] = [
    ]  #"Bd2DPi","Lb2LcPi","Bs2DsRho","Bs2DsstPi","Bd2DsPi","Bs2DsK"]
    # year of data taking
    configdict["YearOfDataTaking"] = {
        "2015", "2016", "2017", "2018"
    }  #,"2016"} #, "2017","2018"} #,"2017","2018"}
    # stripping (necessary in case of PIDK shapes)
    configdict["Stripping"] = {"2012": "21", "2011": "21r1"}
    # integrated luminosity in each year of data taking (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {#"2011": {"Down": 0.56,   "Up": 0.42},
                                          #"2012": {"Down": 0.9912, "Up": 0.9988},
        "2015": {"Down": 0.18695, "Up": 0.14105},
        "2016": {"Down": 0.85996, "Up": 0.80504},
        "2017": {"Down": 0.87689, "Up": 0.83311},
        "2018": {"Down": 1.04846, "Up": 1.14154},
                                          }
    # file name with paths to MC/data samples
    configdict[
        "dataName"] = "../data/Bs2DsK_Run2CPV/Bs2DsPi/config_Bs2DsPi.txt"
    #settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {
        "Directory": "PlotBs2DsPi_Nominal",
        "Extension": "pdf"
    }

    configdict["MoreVariables"] = True

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5600, 6800],
        "InputName": "lab0_MassFitConsD_M"
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1930, 2015],
        "InputName": "lab2_MM"
    }
    configdict["BasicVariables"]["BeautyTime"] = {
        "Range": [0.4, 15.0],
        "InputName": "lab0_LifetimeFit_ctau"
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [3000.0, 650000.0],
        "InputName": "lab1_P"
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [400.0, 45000.0],
        "InputName": "lab1_PT"
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-7.0, 5.0],
        "InputName": "lab1_PIDK"
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 1000.0],
        "InputName": "nTracks"
    }
    configdict["BasicVariables"]["BeautyTimeErr"] = {
        "Range": [0.01, 0.1],
        "InputName": "lab0_LifetimeFit_ctauErr"
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID"
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.475, 1.0],
        "InputName": "BDTGResponse_XGB_1"
    }
    configdict["BasicVariables"]["TagDecOS"] = {
        "Range": [-1.0, 1.0],
        "InputName": "OS_Combination_DEC"
    }
    configdict["BasicVariables"]["TagDecSS"] = {
        "Range": [-1.0, 1.0],
        "InputName": "lab0_SSKaonLatest_TAGDEC"
    }
    configdict["BasicVariables"]["MistagOS"] = {
        "Range": [-3.0, 1.0],
        "InputName": "OS_Combination_ETA"
    }
    configdict["BasicVariables"]["MistagSS"] = {
        "Range": [-3.0, 1.0],
        "InputName": "lab0_SSKaonLatest_TAGETA"
    }

    # tagging calibration
    #configdict["TaggingCalibration"] = {}
    #configdict["TaggingCalibration"]["OS"] = {"p0": 0.375,  "p1": 0.982, "average": 0.3688}
    #configdict["TaggingCalibration"]["SS"] = {"p0": 0.4429, "p1": 0.977, "average": 0.4377}

    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {"Data": "lab1_isMuon==0"}

    pidp = "lab5_PIDp<10"
    D0kkpi = "lab34_MM<1800"
    pide3 = "lab3_PIDe<5"
    t = "&&"
    thigh = "lab3_PIDK<2.0 &&lab4_PIDK < 2. && lab5_PIDK < 2.&& lab3_PIDp < 5. && lab4_PIDp < 5. && lab5_PIDp < 5."
    lab13D0 = "fabs(sqrt(pow(sqrt(pow(493.7,2)+pow(lab1_P,2))+sqrt(pow(lab3_M,2)+pow(lab3_P,2)),2)-pow(lab1_PX+lab3_PX,2)-pow(lab1_PY+lab3_PY,2)-pow(lab1_PZ+lab3_PZ,2))-1864)>30.0"
    lab15D0 = "fabs(sqrt(pow(sqrt(pow(493.7,2)+pow(lab1_P,2))+sqrt(pow(lab5_M,2)+pow(lab5_P,2)),2)-pow(lab1_PX+lab5_PX,2)-pow(lab1_PY+lab5_PY,2)-pow(lab1_PZ+lab5_PZ,2))-1864)>30.0"

    configdict["AdditionalCuts"]["KKPi"] = {"Data": pidp + t + D0kkpi}
    configdict["AdditionalCuts"]["PiPiPi"] = {
        "Data": thigh + t + lab13D0 + t + lab15D0 + t + pide3
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5"
    }

    # additional variables in data sets
    if configdict["MoreVariables"] == True:
        configdict["AdditionalVariables"] = {}
        configdict["AdditionalVariables"]["lab0_OSCharm_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSCharm_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_OSElectronLatest_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSElectronLatest_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_OSKaonLatest_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSKaonLatest_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_OSMuonLatest_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSMuonLatest_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_OSVtxCh_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSVtxCh_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_SSPion_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_SSPion_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_SSProton_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_SSProton_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_OSCharm_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSCharm_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_OSElectronLatest_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSElectronLatest_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_OSKaonLatest_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSKaonLatest_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_OSMuonLatest_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSMuonLatest_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_OSVtxCh_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSVtxCh_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_SSPion_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_SSPion_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_SSProton_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_SSProton_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_P"] = {
            "Range": [0.0, 1600000.0],
            "InputName": "lab0_P"
        }
        configdict["AdditionalVariables"]["lab0_PT"] = {
            "Range": [0.0, 40000.0],
            "InputName": "lab0_PT"
        }
        configdict["AdditionalVariables"]["lab0_ETA"] = {
            "Range": [0.0, 6.0],
            "InputName": "lab0_ETA"
        }
        configdict["AdditionalVariables"]["nCandidate"] = {
            "Range": [0.0, 500.0],
            "InputName": "nCandidate"
        }
        configdict["AdditionalVariables"]["totCandidates"] = {
            "Range": [0.0, 500.0],
            "InputName": "totCandidates"
        }
        configdict["AdditionalVariables"]["runNumber"] = {
            "Range": [0.0, 1000000.0],
            "InputName": "runNumber"
        }
        configdict["AdditionalVariables"]["eventNumber"] = {
            "Range": [0.0, 10000000000.0],
            "InputName": "eventNumber"
        }
        configdict["AdditionalVariables"]["BDTGResponse_3"] = {
            "Range": [-1.0, 1.0],
            "InputName": "BDTGResponse_3"
        }
        configdict["AdditionalVariables"]["nSPDHits"] = {
            "Range": [0.0, 1400.0],
            "InputName": "nSPDHits"
        }
        configdict["AdditionalVariables"]["nTracksLinear"] = {
            "Range": [15.0, 1000.0],
            "InputName": "clone_nTracks"
        }

    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    ###                                                               MDfit fitting settings
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#

    # Bs signal shapes
    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"][
        "type"] = "IpatiaJohnsonSUWithWidthRatio"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run2": {
            "All": 5367.51
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "Run2": {
            "All": 21.259
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "Run2": {
            "All": 14.582
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "Run2": {
            "All": 1.0886
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "Run2": {
            "All": 1.4644
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run2": {
            "All": 1.4891
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run2": {
            "All": 4.2422
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "Run2": {
            "All": 0.00000
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "Run2": {
            "All": 0.00000
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "Run2": {
            "All": -2.4782
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "Run2": {
            "All": -0.23173
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "Run2": {
            "All": 0.38464
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "Run2": {
            "All": 0.26563
        },
        "Fixed": True
    }

    #    configdict["SignalShape"]["BeautyMass"]["sigmaI"]  = {"Run2": {"All" : 19.016},   "Fixed" : True}
    #    configdict["SignalShape"]["BeautyMass"]["sigmaJ"]  = {"Run2": {"All" : 14.749},   "Fixed" : True}
    #    configdict["SignalShape"]["BeautyMass"]["a1"]      = {"Run2": {"All" : 1.1872},   "Fixed" : True}
    #    configdict["SignalShape"]["BeautyMass"]["a2"]      = {"Run2": {"All" : 1.4838},   "Fixed" : True}
    #    configdict["SignalShape"]["BeautyMass"]["n1"]      = {"Run2": {"All" : 1.3542},   "Fixed" : True}
    #    configdict["SignalShape"]["BeautyMass"]["n2"]      = {"Run2": {"All" : 3.7132},   "Fixed" : True}
    #    configdict["SignalShape"]["BeautyMass"]["fb"]      = {"Run2": {"All" : 0.00000},   "Fixed" : True}
    #    configdict["SignalShape"]["BeautyMass"]["zeta"]    = {"Run2": {"All" : 0.00000},   "Fixed" : True}
    ##    configdict["SignalShape"]["BeautyMass"]["l"]       = {"Run2": {"All" : -4.3998},   "Fixed" : True}
    #    configdict["SignalShape"]["BeautyMass"]["nu"]      = {"Run2": {"All" : -0.18713}, "Fixed" : True}
    #    configdict["SignalShape"]["BeautyMass"]["tau"]     = {"Run2": {"All" : 0.40595},  "Fixed" : True}
    #    configdict["SignalShape"]["BeautyMass"]["fracI"]   = {"Run2": {"All" : 0.24061},  "Fixed" : True}

    configdict["SignalShape"]["BeautyMass"]["R"] = {
        "Run2": {
            "NonRes": 1.00,
            "PhiPi": 1.0,
            "Kstk": 1.0,
            "KPiPi": 1.0,
            "PiPiPi": 1.0
        },
        "Fixed": False
    }

    #Ds signal shapes
    configdict["SignalShape"]["CharmMass"] = {}
    configdict["SignalShape"]["CharmMass"]["type"] = "IpatiaPlusGaussian"
    #    configdict["SignalShape"]["CharmMass"]["mean"]    = {"Run2": {"All":1968.49}, "Fixed":True}
    configdict["SignalShape"]["CharmMass"]["mean"] = {
        "20152016": {
            "All": 1968.5
        },
        "2017": {
            "All": 1968.5
        },
        "2018": {
            "All": 1968.6
        },
        "Fixed": True
    }

    configdict["SignalShape"]["CharmMass"]["a1"] = {
        "Run2": {
            "NonRes": 2.1573,
            "PhiPi": 1.5177,
            "KstK": 2.2462,
            "PiPiPi": 0.77130
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["a2"] = {
        "Run2": {
            "NonRes": 3.3609,
            "PhiPi": 3.1516,
            "KstK": 2.4383,
            "PiPiPi": 0.92250
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["n1"] = {
        "Run2": {
            "NonRes": 3.7069,
            "PhiPi": 5.2263,
            "KstK": 2.7880,
            "PiPiPi": 49.930
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["n2"] = {
        "Run2": {
            "NonRes": 2.1238,
            "PhiPi": 2.7229,
            "KstK": 2.9748,
            "PiPiPi": 19.617
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["l"] = {
        "Run2": {
            "NonRes": -2.5053,
            "PhiPi": -2.0406,
            "KstK": -4.4109,
            "PiPiPi": -4.4100
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["fracI"] = {
        "Run2": {
            "NonRes": 0.85053,
            "PhiPi": 0.83030,
            "KstK": 0.83312,
            "PiPiPi": 0.54243
        },
        "Fixed": True
    }

    if configdict["SignalShape"]["CharmMass"][
            "type"] == "IpatiaPlusGaussianWithWidthRatio":
        configdict["SignalShape"]["CharmMass"]["sigmaG"] = {
            "Run2": {
                "NonRes": 5.4473,
                "PhiPi": 5.9455,
                "KstK": 8.6536,
                "PiPiPi": 8.5554
            },
            "Fixed": True
        }
        configdict["SignalShape"]["CharmMass"]["sigmaI"] = {
            "Run2": {
                "NonRes": 7.3839,
                "PhiPi": 7.7907,
                "KstK": 6.2577,
                "PiPiPi": 9.0294
            },
            "Fixed": True
        }
        configdict["SignalShape"]["CharmMass"]["R"] = {
            "20152016": {
                "NonRes": 1.00,
                "PhiPi": 1.0,
                "Kstk": 1.0,
                "KPiPi": 1.0,
                "PiPiPi": 1.0
            },
            "2017": {
                "NonRes": 1.00,
                "PhiPi": 1.0,
                "Kstk": 1.0,
                "KPiPi": 1.0,
                "PiPiPi": 1.0
            },
            "2018": {
                "NonRes": 1.00,
                "PhiPi": 1.0,
                "Kstk": 1.0,
                "KPiPi": 1.0,
                "PiPiPi": 1.0
            },
            "Fixed": False
        }  #
    else:
        configdict["SignalShape"]["CharmMass"]["sigmaG"] = {
            "20152016": {
                "NonRes": 6.9126,
                "PhiPi": 7.2513,
                "KstK": 8.5563,
                "PiPiPi": 9.9021
            },
            "2017": {
                "NonRes": 7.3645,
                "PhiPi": 7.2018,
                "KstK": 8.8234,
                "PiPiPi": 9.8097
            },
            "2018": {
                "NonRes": 6.6069,
                "PhiPi": 7.4532,
                "KstK": 8.5228,
                "PiPiPi": 9.6391
            },
            "Fixed": True
        }
        configdict["SignalShape"]["CharmMass"]["sigmaI"] = {
            "20152016": {
                "NonRes": 7.6673,
                "PhiPi": 8.1400,
                "KstK": 6.8288,
                "PiPiPi": 8.7199
            },
            "2017": {
                "NonRes": 7.3355,
                "PhiPi": 8.0214,
                "KstK": 6.5564,
                "PiPiPi": 8.3211
            },
            "2018": {
                "NonRes": 7.4467,
                "PhiPi": 7.8903,
                "KstK": 6.6660,
                "PiPiPi": 8.1717
            },
            "Fixed": True
        }

    configdict["SignalShape"]["CharmMass"]["fb"] = {
        "Run2": {
            "All": 0.00
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["zeta"] = {
        "Run2": {
            "All": 0.00
        },
        "Fixed": True
    }

    # combinatorial background
    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "DoubleExponential"
    configdict["CombBkgShape"]["BeautyMass"]["cB1"] = {
        "Run2": {
            "NonRes": -3.5211e-02,
            "PhiPi": -3.0873e-02,
            "KstK": -2.3392e-02,
            "KPiPi": -1.0361e-02,
            "PiPiPi": -1.5277e-02
        },
        "Fixed": False
    }
    configdict["CombBkgShape"]["BeautyMass"]["cB2"] = {
        "Run2": {
            "NonRes": -3.5211e-02,
            "PhiPi": -3.0873e-02,
            "KstK": -2.3392e-02,
            "KPiPi": -1.0361e-02,
            "PiPiPi": -1.5277e-02
        },
        "Fixed": False
    }
    #configdict["CombBkgShape"]["BeautyMass"]["cB2"]   = {"Run2": {"NonRes":0.0,       "PhiPi":0.0,       "KstK":0.0,      "KPiPi":0.0,         "PiPiPi":0.0},
    #                                                     "Fixed":True }
    configdict["CombBkgShape"]["BeautyMass"]["frac"] = {
        "Run2": {
            "NonRes": 4.3067e-01,
            "PhiPi": 6.5400e-01,
            "KstK": 3.7409e-01,
            "KPiPi": 0.5,
            "PiPiPi": 0.5
        },
        "Fixed": False
    }

    configdict["CombBkgShape"]["CharmMass"] = {}
    configdict["CombBkgShape"]["CharmMass"][
        "type"] = "ExponentialPlusSignal"  #"ExponentialPlusDoubleCrystalBallWithWidthRatioSharedMean"
    configdict["CombBkgShape"]["CharmMass"]["cD"] = {
        "20152016": {
            "NonRes": -5.7520e-03,
            "PhiPi": -5.7273e-03,
            "KstK": -8.3967e-03,
            "KPiPi": -4.9193e-03,
            "PiPiPi": -4.5455e-02
        },
        "2017": {
            "NonRes": -5.7520e-03,
            "PhiPi": -5.7273e-03,
            "KstK": -8.3967e-03,
            "KPiPi": -4.9193e-03,
            "PiPiPi": -4.5455e-02
        },
        "2018": {
            "NonRes": -5.7520e-03,
            "PhiPi": -5.7273e-03,
            "KstK": -8.3967e-03,
            "KPiPi": -4.9193e-03,
            "PiPiPi": -4.5455e-02
        },
        "Fixed": False
    }
    configdict["CombBkgShape"]["CharmMass"]["fracD"] = {
        "20152016": {
            "NonRes": 0.88620,
            "PhiPi": 0.37379,
            "KstK": 0.59093,
            "KPiPi": 0.5,
            "PiPiPi": 0.5
        },
        "2017": {
            "NonRes": 0.88620,
            "PhiPi": 0.37379,
            "KstK": 0.59093,
            "KPiPi": 0.5,
            "PiPiPi": 0.5
        },
        "2018": {
            "NonRes": 0.88620,
            "PhiPi": 0.37379,
            "KstK": 0.59093,
            "KPiPi": 0.5,
            "PiPiPi": 0.5
        },
        "Fixed": False
    }

    configdict["CombBkgShape"]["BacPIDK"] = {}
    configdict["CombBkgShape"]["BacPIDK"]["type"] = "FixedWithKaonPion"
    configdict["CombBkgShape"]["BacPIDK"]["components"] = {
        "Kaon": True,
        "Pion": True,
        "Proton": False
    }
    configdict["CombBkgShape"]["BacPIDK"]["fracPIDK1"] = {
        "Run2": {
            "NonRes": 0.9,
            "PhiPi": 0.9,
            "KstK": 0.9,
            "KPiPi": 0.8,
            "PiPiPi": 0.8
        },
        "Fixed": False
    }
    configdict["CombBkgShape"]["BacPIDK"]["fracPIDK2"] = {
        "Run2": {
            "NonRes": 0.9,
            "PhiPi": 0.9,
            "KstK": 0.9,
            "KPiPi": 0.8,
            "PiPiPi": 0.8
        },
        "Fixed": False
    }

    #Bd2Dsh background
    #shape for BeautyMass, for CharmMass as well as BacPIDK taken by default the same as signal
    configdict["Bd2DsPiShape"] = {}
    configdict["Bd2DsPiShape"]["BeautyMass"] = {}
    if configdict["SignalShape"]["BeautyMass"][
            "type"] == "IpatiaJohnsonSUWithWidthRatio":
        configdict["Bd2DsPiShape"]["BeautyMass"][
            "type"] = "ShiftedSignalIpatiaJohnsonSU"
    else:
        configdict["Bd2DsPiShape"]["BeautyMass"]["type"] = "ShiftedSignal"
    configdict["Bd2DsPiShape"]["BeautyMass"]["shift"] = {
        "Run2": {
            "All": -86.8
        },
        "Fixed": True
    }
    configdict["Bd2DsPiShape"]["BeautyMass"]["scale1"] = {
        "Run2": {
            "All": 1.00808721452
        },
        "Fixed": True
    }
    configdict["Bd2DsPiShape"]["BeautyMass"]["scale2"] = {
        "Run2": {
            "All": 1.03868673310
        },
        "Fixed": True
    }

    #
    configdict["AdditionalParameters"] = {}
    configdict["AdditionalParameters"]["g1_f1_frac"] = {
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.5,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }
    configdict["AdditionalParameters"]["g1_f2_frac"] = {
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.5,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }

    #expected yields
    configdict["Yields"] = {}
    configdict["Yields"]["CombBkg"] = {
        "2015": {
            "NonRes": 10000.0,
            "PhiPi": 20000.0,
            "KstK": 10000.0,
            "KPiPi": 10000.0,
            "PiPiPi": 10000.0
        },
        "2016": {
            "NonRes": 20000.0,
            "PhiPi": 30000.0,
            "KstK": 20000.0,
            "KPiPi": 20000.0,
            "PiPiPi": 20000.0
        },
        "2017": {
            "NonRes": 20000.0,
            "PhiPi": 30000.0,
            "KstK": 20000.0,
            "KPiPi": 20000.0,
            "PiPiPi": 20000.0
        },
        "2018": {
            "NonRes": 20000.0,
            "PhiPi": 40000.0,
            "KstK": 20000.0,
            "KPiPi": 20000.0,
            "PiPiPi": 20000.0
        },
        "Fixed": False
    }
    configdict["Yields"]["Signal"] = {
        "2015": {
            "NonRes": 10000.0,
            "PhiPi": 20000.0,
            "KstK": 10000.0,
            "KPiPi": 10000.0,
            "PiPiPi": 10000.0
        },
        "2016": {
            "NonRes": 20000.0,
            "PhiPi": 40000.0,
            "KstK": 20000.0,
            "KPiPi": 20000.0,
            "PiPiPi": 20000.0
        },
        "2017": {
            "NonRes": 40000.0,
            "PhiPi": 40000.0,
            "KstK": 30000.0,
            "KPiPi": 20000.0,
            "PiPiPi": 20000.0
        },
        "2018": {
            "NonRes": 40000.0,
            "PhiPi": 60000.0,
            "KstK": 40000.0,
            "KPiPi": 20000.0,
            "PiPiPi": 20000.0
        },
        "Fixed": False
    }

    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    ###                                                               MDfit plotting settings
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    from ROOT import *
    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = [
        "Sig", "CombBkg", "Bd2DPi", "Lb2LcPi", "Bs2DsDsstPiRho", "Bs2DsK"
    ]
    configdict["PlotSettings"]["colors"] = [
        kRed - 7, kBlue - 6, kOrange, kRed, kBlue - 10, kGreen + 3
    ]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.76, 0.9],
        "ScaleYSize": 1.2
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.9],
        "ScaleYSize": 1.7,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075
    }

    #configdict["LegendSettings"]["CharmMass"]  = {"Position":[0.20, 0.69, 0.93, 0.93], "TextSize": 0.05, "LHCbText":[0.8,0.66],
    #                                              "ScaleYSize":1.7, "SetLegendColumns":2, "LHCbTextSize":0.075 }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.20, 0.9],
        "ScaleYSize": 1.2
    }

    return configdict
