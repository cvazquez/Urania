###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import division
from past.utils import old_div
import ROOT
import sys
import os

sys.path.insert(0,
                os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))


def getconfig():
    configdict = {}

    # considered decay mode
    configdict["Decay"] = "Bs2DsPi"
    configdict["CharmModes"] = [
        "PhiPi",
        "KstK",
        "NonRes",
        "PiPiPi",
    ]
    configdict["Backgrounds"] = [
        "Bd2DPi",
        "Bs2DsRho",
        "Lb2LcPi",
        "Bs2DsK",
        "Bd2DsPi",
        "Bs2DsstPi",
    ]
    # year of data taking
    configdict["YearOfDataTaking"] = [
        "2015",
        "2016",
        "2017",
        "2018",
    ]
    # stripping (necessary in case of PIDK shapes)
    configdict["Stripping"] = {"2012": "21", "2011": "21r1"}
    # integrated luminosity in each year of data taking (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {
        "2015": {
            "Down": 0.18695,
            "Up": 0.14105
        },
        "2016": {
            "Down": 0.85996,
            "Up": 0.80504
        },
        "2017": {
            "Down": 0.87689,
            "Up": 0.83311
        },
        "2018": {
            "Down": 1.04846,
            "Up": 1.14154
        },
    }
    frac_2015 = (old_div((configdict["IntegratedLuminosity"]["2015"]["Down"] +
                          configdict["IntegratedLuminosity"]["2015"]["Up"]),
                         (configdict["IntegratedLuminosity"]["2015"]["Down"] +
                          configdict["IntegratedLuminosity"]["2015"]["Up"] +
                          configdict["IntegratedLuminosity"]["2016"]["Down"] +
                          configdict["IntegratedLuminosity"]["2016"]["Up"])))
    frac_2016 = (old_div((configdict["IntegratedLuminosity"]["2016"]["Down"] +
                          configdict["IntegratedLuminosity"]["2016"]["Up"]),
                         (configdict["IntegratedLuminosity"]["2015"]["Down"] +
                          configdict["IntegratedLuminosity"]["2015"]["Up"] +
                          configdict["IntegratedLuminosity"]["2016"]["Down"] +
                          configdict["IntegratedLuminosity"]["2016"]["Up"])))
    # file name with paths to MC/data samples
    configdict[
        "dataName"] = "../data/Bs2DsK_Run2CPV/Bs2DsPi/config_Bs2DsPi.txt"
    # settings for control plots
    configdict["ControlPlots"] = {
        "Directory": "PlotBs2DsPi_Nominal",
        "Extension": "pdf"
    }

    configdict["MoreVariables"] = False

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5300, 5800],
        "InputName": "BeautyMass"
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1930, 2015],
        "InputName": "CharmMass"
    }
    configdict["BasicVariables"]["BeautyTime"] = {
        "Range": [0.4, 15.0],
        "InputName": "BeautyTime"
    }
    configdict["BasicVariables"]["BeautyTimeErr"] = {
        "Range": [0.01, 0.1],
        "InputName": "BeautyTimeErr"
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "BacCharge"
    }
    configdict["BasicVariables"]["TagDecOS"] = {
        "Range": [-1.0, 1.0],
        "InputName": "TagDecOS"
    }
    configdict["BasicVariables"]["TagDecSS"] = {
        "Range": [-1.0, 1.0],
        "InputName": "TagDecSS"
    }
    configdict["BasicVariables"]["MistagOS"] = {
        "Range": [-3.0, 1.0],
        "InputName": "MistagOS"
    }
    configdict["BasicVariables"]["MistagSS"] = {
        "Range": [-3.0, 1.0],
        "InputName": "MistagSS"
    }

    # additional cuts applied to data sets
    configdict["AdditionalCuts"] = {}

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5"
    }

    ###########################################################################
    #                    MDfit fitting settings
    ###########################################################################

    # Bs signal shapes
    configdict["SignalShape"] = {
        'BeautyMass': {
            'a1': {
                '20152016': {
                    'KstK': 0.232789,
                    'NonRes': 0.267046,
                    'PhiPi': 0.272534,
                    'PiPiPi': 0.28194
                },
                '2017': {
                    'KstK': 0.262134,
                    'NonRes': 0.314537,
                    'PhiPi': 0.266499,
                    'PiPiPi': 0.270575
                },
                '2018': {
                    'KstK': 0.39719,
                    'NonRes': 0.380822,
                    'PhiPi': 0.23871,
                    'PiPiPi': 0.540159
                },
                'Fixed': True
            },
            'a2': {
                '20152016': {
                    'KstK': 2.77204,
                    'NonRes': 1.95297,
                    'PhiPi': 2.6426,
                    'PiPiPi': 3.02235
                },
                '2017': {
                    'KstK': 2.72509,
                    'NonRes': 2.76145,
                    'PhiPi': 2.95991,
                    'PiPiPi': 3.48439
                },
                '2018': {
                    'KstK': 2.27732,
                    'NonRes': 2.4077,
                    'PhiPi': 2.79521,
                    'PiPiPi': 2.39562
                },
                'Fixed': True
            },
            'fb': {
                'Fixed': True,
                'Run2': {
                    'All': 0.0
                }
            },
            'fracI': {
                '20152016': {
                    'KstK': 0.121322,
                    'NonRes': 0.114842,
                    'PhiPi': 0.124757,
                    'PiPiPi': 0.19915
                },
                '2017': {
                    'KstK': 0.126357,
                    'NonRes': 0.142232,
                    'PhiPi': 0.162128,
                    'PiPiPi': 0.246785
                },
                '2018': {
                    'KstK': 0.126309,
                    'NonRes': 0.16328,
                    'PhiPi': 0.14134,
                    'PiPiPi': 0.141831
                },
                'Fixed': True
            },
            'l': {
                '20152016': {
                    'KstK': -1.40369,
                    'NonRes': -1.54649,
                    'PhiPi': -1.47673,
                    'PiPiPi': -1.39036
                },
                '2017': {
                    'KstK': -1.46337,
                    'NonRes': -1.59434,
                    'PhiPi': -1.36771,
                    'PiPiPi': -1.33975
                },
                '2018': {
                    'KstK': -1.61501,
                    'NonRes': -1.76979,
                    'PhiPi': -1.33691,
                    'PiPiPi': -3.35
                },
                'Fixed': True
            },
            'mean': {
                '20152016': {
                    'All': 5367.51
                },
                '2017': {
                    'All': 5367.51
                },
                '2018': {
                    'All': 5367.51
                },
                'Fixed': False
            },
            'n1': {
                '20152016': {
                    'KstK': 50.0,
                    'NonRes': 50.0,
                    'PhiPi': 50.0,
                    'PiPiPi': 50.0
                },
                '2017': {
                    'KstK': 50.0,
                    'NonRes': 50.0,
                    'PhiPi': 50.0,
                    'PiPiPi': 50.0
                },
                '2018': {
                    'KstK': 50.0,
                    'NonRes': 50.0,
                    'PhiPi': 50.0,
                    'PiPiPi': 50.0
                },
                'Fixed': True
            },
            'n2': {
                '20152016': {
                    'KstK': 0.974427,
                    'NonRes': 2.02956,
                    'PhiPi': 1.07021,
                    'PiPiPi': 1.15776
                },
                '2017': {
                    'KstK': 0.999712,
                    'NonRes': 1.27801,
                    'PhiPi': 0.966063,
                    'PiPiPi': 0.979765
                },
                '2018': {
                    'KstK': 1.17564,
                    'NonRes': 1.6869,
                    'PhiPi': 0.986023,
                    'PiPiPi': 1.51773
                },
                'Fixed': True
            },
            'nu': {
                '20152016': {
                    'KstK': -0.140866,
                    'NonRes': -0.19274,
                    'PhiPi': -0.202706,
                    'PiPiPi': -0.0483708
                },
                '2017': {
                    'KstK': -0.168803,
                    'NonRes': -0.162521,
                    'PhiPi': -0.125343,
                    'PiPiPi': -0.035089
                },
                '2018': {
                    'KstK': -0.1796,
                    'NonRes': -0.210806,
                    'PhiPi': -0.159861,
                    'PiPiPi': -0.286881
                },
                'Fixed': True
            },
            'sigmaI': {
                '20152016': {
                    'KstK': 39.2369,
                    'NonRes': 37.4012,
                    'PhiPi': 39.6851,
                    'PiPiPi': 33.3208
                },
                '2017': {
                    'KstK': 35.7117,
                    'NonRes': 33.8186,
                    'PhiPi': 39.2647,
                    'PiPiPi': 30.8107
                },
                '2018': {
                    'KstK': 34.7219,
                    'NonRes': 29.9745,
                    'PhiPi': 39.897,
                    'PiPiPi': 28.6863
                },
                'Fixed': True
            },
            'sigmaJ': {
                '20152016': {
                    'KstK': 14.2233,
                    'NonRes': 14.1919,
                    'PhiPi': 14.393,
                    'PiPiPi': 14.5642
                },
                '2017': {
                    'KstK': 13.8814,
                    'NonRes': 13.6276,
                    'PhiPi': 13.818,
                    'PiPiPi': 13.9234
                },
                '2018': {
                    'KstK': 13.7956,
                    'NonRes': 13.4904,
                    'PhiPi': 13.9978,
                    'PiPiPi': 14.234
                },
                'Fixed': False
            },
            'tau': {
                '20152016': {
                    'KstK': 0.343189,
                    'NonRes': 0.34648,
                    'PhiPi': 0.360024,
                    'PiPiPi': 0.352791
                },
                '2017': {
                    'KstK': 0.344825,
                    'NonRes': 0.319533,
                    'PhiPi': 0.328242,
                    'PiPiPi': 0.311098
                },
                '2018': {
                    'KstK': 0.337835,
                    'NonRes': 0.294316,
                    'PhiPi': 0.345727,
                    'PiPiPi': 0.338194
                },
                'Fixed': True
            },
            'type': 'IpatiaJohnsonSU',
            'zeta': {
                'Fixed': True,
                'Run2': {
                    'All': 0.0
                }
            }
        },
        'CharmMass': {
            'a1': {
                '20152016': {
                    'KstK': 0.17253,
                    'NonRes': 0.16053,
                    'PhiPi': 0.18997,
                    'PiPiPi': 0.183346
                },
                '2017': {
                    'KstK': 0.14209,
                    'NonRes': 0.12713,
                    'PhiPi': 0.20276,
                    'PiPiPi': 0.173038
                },
                '2018': {
                    'KstK': 0.13652,
                    'NonRes': 0.16241,
                    'PhiPi': 0.1653,
                    'PiPiPi': 0.184369
                },
                'Fixed': True
            },
            'a2': {
                '20152016': {
                    'KstK': 0.37944,
                    'NonRes': 0.39485,
                    'PhiPi': 0.37501,
                    'PiPiPi': 0.844259
                },
                '2017': {
                    'KstK': 0.31042,
                    'NonRes': 0.26094,
                    'PhiPi': 0.49399,
                    'PiPiPi': 0.805209
                },
                '2018': {
                    'KstK': 0.27201,
                    'NonRes': 0.37773,
                    'PhiPi': 0.29843,
                    'PiPiPi': 0.888122
                },
                'Fixed': True
            },
            'fb': {
                'Fixed': True,
                'Run2': {
                    'All': 0.0
                }
            },
            'fracI': {
                '20152016': {
                    'KstK': 0.087039,
                    'NonRes': 0.086438,
                    'PhiPi': 0.12966,
                    'PiPiPi': 0.312702
                },
                '2017': {
                    'KstK': 0.067769,
                    'NonRes': 0.080211,
                    'PhiPi': 0.13619,
                    'PiPiPi': 0.271536
                },
                '2018': {
                    'KstK': 0.069336,
                    'NonRes': 0.094782,
                    'PhiPi': 0.12088,
                    'PiPiPi': 0.308127
                },
                'Fixed': True
            },
            'l': {
                'Fixed': True,
                'Run2': {
                    'All': -1.1
                }
            },
            'mean': {
                '20152016': {
                    'All': 1968.49
                },
                '2017': {
                    'All': 1968.49
                },
                '2018': {
                    'All': 1968.49
                },
                'Fixed': False
            },
            'n1': {
                'Fixed': True,
                'Run2': {
                    'All': 8.0
                }
            },
            'n2': {
                'Fixed': True,
                'Run2': {
                    'All': 8.0
                }
            },
            'nu': {
                'Fixed': True,
                'Run2': {
                    'All': 0.0
                }
            },
            'sigmaI': {
                '20152016': {
                    'KstK': 26.271,
                    'NonRes': 29.915,
                    'PhiPi': 26.734,
                    'PiPiPi': 30.4756
                },
                '2017': {
                    'KstK': 26.868,
                    'NonRes': 26.66,
                    'PhiPi': 27.198,
                    'PiPiPi': 30.3659
                },
                '2018': {
                    'KstK': 24.853,
                    'NonRes': 27.776,
                    'PhiPi': 24.312,
                    'PiPiPi': 29.3058
                },
                'Fixed': True
            },
            'sigmaJ': {
                '20152016': {
                    'KstK': 6.5514,
                    'NonRes': 6.4363,
                    'PhiPi': 6.4202,
                    'PiPiPi': 9.20648
                },
                '2017': {
                    'KstK': 6.4968,
                    'NonRes': 6.3217,
                    'PhiPi': 6.2182,
                    'PiPiPi': 9.03914
                },
                '2018': {
                    'KstK': 6.4939,
                    'NonRes': 6.3125,
                    'PhiPi': 6.3379,
                    'PiPiPi': 9.08361
                },
                'Fixed': False
            },
            'tau': {
                '20152016': {
                    'KstK': 0.44602,
                    'NonRes': 0.46214,
                    'PhiPi': 0.4855,
                    'PiPiPi': 0.380109
                },
                '2017': {
                    'KstK': 0.45419,
                    'NonRes': 0.45391,
                    'PhiPi': 0.46851,
                    'PiPiPi': 0.387104
                },
                '2018': {
                    'KstK': 0.44849,
                    'NonRes': 0.46527,
                    'PhiPi': 0.49029,
                    'PiPiPi': 0.387943
                },
                'Fixed': True
            },
            'type': 'IpatiaJohnsonSU',
            'zeta': {
                'Fixed': True,
                'Run2': {
                    'All': 0.0
                }
            }
        }
    }

    # combinatorial background
    configdict["CombBkgShape"] = {
        'BeautyMass': {
            'cB1': {
                '20152016': {
                    'KstK': -2e-2,
                    'NonRes': -2e-2,
                    'PhiPi': -2e-2,
                    'PiPiPi': -2e-2
                },
                '2017': {
                    'KstK': -2e-2,
                    'NonRes': -2e-2,
                    'PhiPi': -2e-2,
                    'PiPiPi': -2e-2
                },
                '2018': {
                    'KstK': -2e-2,
                    'NonRes': -2e-2,
                    'PhiPi': -2e-2,
                    'PiPiPi': -2e-2
                },
                'Fixed': False
            },
            'cB2': {
                '20152016': {
                    'KstK': -0.00064593,
                    'NonRes': -0.00055365,
                    'PhiPi': -0.00026327,
                    'PiPiPi': -0.0004751
                },
                '2017': {
                    'KstK': -0.00064593,
                    'NonRes': -0.00055365,
                    'PhiPi': -0.00026327,
                    'PiPiPi': -0.0004751
                },
                '2018': {
                    'KstK': -0.00064593,
                    'NonRes': -0.00055365,
                    'PhiPi': -0.00026327,
                    'PiPiPi': -0.0004751
                },
                'Fixed': True
            },
            'frac': {
                '20152016': {
                    'KstK': 0.37409,
                    'NonRes': 0.43067,
                    'PhiPi': 0.654,
                    'PiPiPi': 0.5
                },
                '2017': {
                    'KstK': 0.37409,
                    'NonRes': 0.43067,
                    'PhiPi': 0.654,
                    'PiPiPi': 0.5
                },
                '2018': {
                    'KstK': 0.37409,
                    'NonRes': 0.43067,
                    'PhiPi': 0.654,
                    'PiPiPi': 0.5
                },
                'Fixed': False
            },
            'type': 'DoubleExponential'
        },
        'CharmMass': {
            'cD': {
                '20152016': {
                    'KstK': -0.00645,
                    'NonRes': -0.004008,
                    'PhiPi': -0.007266,
                    'PiPiPi': -0.006082
                },
                '2017': {
                    'KstK': -0.00953,
                    'NonRes': -0.005371,
                    'PhiPi': -0.006151,
                    'PiPiPi': -0.006215
                },
                '2018': {
                    'KstK': -0.0076,
                    'NonRes': -0.005456,
                    'PhiPi': -0.007638,
                    'PiPiPi': -0.005964
                },
                'Fixed': False
            },
            'fracD': {
                '20152016': {
                    'KstK': 0.8054,
                    'NonRes': 0.9618,
                    'PhiPi': 0.6231,
                    'PiPiPi': 0.9743
                },
                '2017': {
                    'KstK': 0.8905,
                    'NonRes': 0.9765,
                    'PhiPi': 0.6018,
                    'PiPiPi': 0.99
                },
                '2018': {
                    'KstK': 0.874,
                    'NonRes': 0.9506,
                    'PhiPi': 0.6271,
                    'PiPiPi': 0.968
                },
                'Fixed': False
            },
            'type': 'ExponentialPlusSignal'
        }
    }
    # Bd2Dsh background
    # shape for BeautyMass, for CharmMass as well as BacPIDK taken by default the same as signal
    configdict["Bd2DsPiShape"] = {
        'BeautyMass': {
            'scale1': {
                'Fixed': True,
                'Run2': {
                    'All': 1.0
                }
            },
            'scale2': {
                'Fixed': True,
                'Run2': {
                    'All': 1.0
                }
            },
            'shift': {
                'Fixed': True,
                'Run2': {
                    'All': -87.38
                }
            },
            'type': 'ShiftedSignalIpatiaJohnsonSU'
        }
    }

    #
    configdict["AdditionalParameters"] = {}
    configdict["AdditionalParameters"]["g1_f1_frac"] = {
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.5,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }
    configdict["AdditionalParameters"]["g1_f2_frac"] = {
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.5,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }

    # expected yields
    configdict["Yields"] = {
        "Signal": {
            "2015": {
                "NonRes": 10000.0,
                "PhiPi": 20000.0,
                "KstK": 10000.0,
                "PiPiPi": 10000.0
            },
            "2016": {
                "NonRes": 20000.0,
                "PhiPi": 40000.0,
                "KstK": 20000.0,
                "PiPiPi": 20000.0
            },
            "2017": {
                "NonRes": 20000.0,
                "PhiPi": 40000.0,
                "KstK": 20000.0,
                "PiPiPi": 20000.0
            },
            "2018": {
                "NonRes": 20000.0,
                "PhiPi": 40000.0,
                "KstK": 20000.0,
                "PiPiPi": 20000.0
            },
            "Fixed": False
        },
        "CombBkg": {
            "2015": {
                "NonRes": 5000.0,
                "PhiPi": 1500.0,
                "KstK": 2000.0,
                "PiPiPi": 5000.0
            },
            "2016": {
                "NonRes": 10000.0,
                "PhiPi": 3500.0,
                "KstK": 2000.0,
                "PiPiPi": 10000.0
            },
            "2017": {
                "NonRes": 7500.0,
                "PhiPi": 5000.0,
                "KstK": 4000.0,
                "PiPiPi": 15000.0
            },
            "2018": {
                "NonRes": 7500.0,
                "PhiPi": 5000.0,
                "KstK": 4000.0,
                "PiPiPi": 15000.0
            },
            "Fixed": False
        },
        "Bd2DPi": {
            '20152016': {
                'KstK': 293.5,
                'NonRes': 1010.0,
                'PhiPi': 19.18,
                'PiPiPi': 0.0,
            },
            '2017': {
                'KstK': 265.2,
                'NonRes': 931.5,
                'PhiPi': 22.11,
                'PiPiPi': 0.0,
            },
            '2018': {
                'KstK': 319.4,
                'NonRes': 1123.0,
                'PhiPi': 21.59,
                'PiPiPi': 0.0,
            },
            "Fixed": True,
        },
        "Lb2LcPi": {
            '20152016': {
                'KstK': 168.8,
                'NonRes': 781.3,
                'PhiPi': 68.49,
                'PiPiPi': 0.0,
            },
            '2017': {
                'KstK': 133.1,
                'NonRes': 650.2,
                'PhiPi': 52.03,
                'PiPiPi': 0.0,
            },
            '2018': {
                'KstK': 159.3,
                'NonRes': 765.8,
                'PhiPi': 74.43,
                'PiPiPi': 0.0,
            },
            "Fixed": True
        },
        "Bs2DsK": {
            '20152016': {
                'KstK': 281.3,
                'NonRes': 181.0,
                'PhiPi': 405.3,
                'PiPiPi': 185.2
            },
            '2017': {
                'KstK': 227.0,
                'NonRes': 145.1,
                'PhiPi': 323.1,
                'PiPiPi': 148.9
            },
            '2018': {
                'KstK': 277.3,
                'NonRes': 173.5,
                'PhiPi': 391.4,
                'PiPiPi': 176.8
            },
            "Fixed": True
        },
        "Bs2DsDsstPiRho": {
            '20152016': {
                'KstK': 521.7,
                'NonRes': 299.9,
                'PhiPi': 723.5,
                'PiPiPi': 326.8
            },
            '2017': {
                'KstK': 412.1,
                'NonRes': 228.9,
                'PhiPi': 588.8,
                'PiPiPi': 311.4
            },
            '2018': {
                'KstK': 549.7,
                'NonRes': 302.9,
                'PhiPi': 553.1,
                'PiPiPi': 260.8
            },
            "Fixed": False
        },
    }

    # Now split 20152016 according to lumi, as B2DXFitter scripts expect it
    for component, component_dict in list(configdict["Yields"].items()):
        if type(component_dict) == dict and "20152016" in component_dict:
            configdict["Yields"][component]["2015"] = {
                dmode: frac_2015 * value
                for dmode, value in list(component_dict["20152016"].items())
            }
            configdict["Yields"][component]["2016"] = {
                dmode: frac_2016 * value
                for dmode, value in list(component_dict["20152016"].items())
            }
            configdict["Yields"][component].pop("20152016")

    ###########################################################################
    #                          MDfit plotting settings
    ###########################################################################
    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = [
        "Sig",
        "CombBkg",
        "Bd2DPi",
        "Lb2LcPi",
        "Bs2DsDsstPiRho",
        "Bs2DsK",
    ]
    configdict["PlotSettings"]["colors"] = [
        ROOT.kRed - 7, ROOT.kBlue - 6, ROOT.kOrange, ROOT.kRed,
        ROOT.kBlue - 10, ROOT.kGreen + 3
    ]

    configdict["LegendSettings"] = {
        "lhcbtext": "Toys",
    }
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.35, 0.9],
        "ScaleYSize": 2.5
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.7,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.20, 0.9],
        "ScaleYSize": 1.2
    }

    return configdict


if __name__ == '__main__':
    from configutils import main
    main(getconfig())
