###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import sys
import os

sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..'))  # noqa
import nominal.Generator


def getconfig():
    configdict = nominal.Generator.getconfig()

    # Adjust the generated bias
    for component in list(configdict["Components"].keys()):
        configdict["ResolutionAcceptance"][component]["Resolution"]["Bias"] = [
            -0.003
        ]

    return configdict


# just print the full config if run directly
if __name__ == '__main__':
    import pprint
    pprint.pprint(getconfig())
