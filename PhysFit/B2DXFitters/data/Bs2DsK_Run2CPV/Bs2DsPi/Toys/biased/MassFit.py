import sys
import os

sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..'))  # noqa
import nominal.MassFit


def getconfig():
    return nominal.MassFit.getconfig()


# just print the full config if run directly
if __name__ == '__main__':
    import pprint
    pprint.pprint(getconfig())
