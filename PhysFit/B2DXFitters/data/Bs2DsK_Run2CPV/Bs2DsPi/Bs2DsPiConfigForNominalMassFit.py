###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
def getconfig():

    configdict = {}

    from math import pi
    from math import log

    # considered decay mode
    configdict["Decay"] = "Bs2DsPi"
    configdict["CharmModes"] = {"NonRes", "PhiPi", "KstK", "PiPiPi"}
    configdict["Backgrounds"] = [
        "Bd2DPi", "Lb2LcPi", "Bs2DsRho", "Bs2DsstPi", "Bd2DsPi", "Bs2DsK"
    ]
    # year of data taking
    configdict["YearOfDataTaking"] = {
        "2015", "2016", "2017", "2018"
    }  #,"2016"} #, "2017","2018"} #,"2017","2018"}
    # stripping (necessary in case of PIDK shapes)
    configdict["Stripping"] = {"2012": "21", "2011": "21r1"}
    # integrated luminosity in each year of data taking (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {#"2011": {"Down": 0.56,   "Up": 0.42},
                                          #"2012": {"Down": 0.9912, "Up": 0.9988},
        "2015": {"Down": 0.18695, "Up": 0.14105},
        "2016": {"Down": 0.85996, "Up": 0.80504},
        "2017": {"Down": 0.87689, "Up": 0.83311},
        "2018": {"Down": 1.04846, "Up": 1.14154},
                                          }
    # file name with paths to MC/data samples
    configdict[
        "dataName"] = "../data/Bs2DsK_Run2CPV/Bs2DsPi/config_Bs2DsPi.txt"
    #settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {
        "Directory": "PlotBs2DsPi_Nominal",
        "Extension": "pdf"
    }

    configdict["MoreVariables"] = True

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5300, 5800],
        "InputName": "lab0_MassFitConsD_M"
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1930, 2015],
        "InputName": "lab2_MM"
    }
    configdict["BasicVariables"]["BeautyTime"] = {
        "Range": [0.4, 15.0],
        "InputName": "lab0_LifetimeFit_ctau"
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [3000.0, 650000.0],
        "InputName": "lab1_P"
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [400.0, 45000.0],
        "InputName": "lab1_PT"
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-7.0, 5.0],
        "InputName": "lab1_PIDK"
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 1000.0],
        "InputName": "nTracks"
    }
    configdict["BasicVariables"]["BeautyTimeErr"] = {
        "Range": [0.01, 0.1],
        "InputName": "lab0_LifetimeFit_ctauErr"
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID"
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.475, 1.0],
        "InputName": "BDTGResponse_XGB_1"
    }
    configdict["BasicVariables"]["TagDecOS"] = {
        "Range": [-1.0, 1.0],
        "InputName": "OS_Combination_DEC"
    }
    configdict["BasicVariables"]["TagDecSS"] = {
        "Range": [-1.0, 1.0],
        "InputName": "lab0_SSKaonLatest_TAGDEC"
    }
    configdict["BasicVariables"]["MistagOS"] = {
        "Range": [-3.0, 1.0],
        "InputName": "OS_Combination_ETA"
    }
    configdict["BasicVariables"]["MistagSS"] = {
        "Range": [-3.0, 1.0],
        "InputName": "lab0_SSKaonLatest_TAGETA"
    }

    # tagging calibration
    #configdict["TaggingCalibration"] = {}
    #configdict["TaggingCalibration"]["OS"] = {"p0": 0.375,  "p1": 0.982, "average": 0.3688}
    #configdict["TaggingCalibration"]["SS"] = {"p0": 0.4429, "p1": 0.977, "average": 0.4377}

    HLTcut = "&&(lab0_Hlt1TrackAllL0Decision_TOS && (lab0_Hlt2IncPhiDecision_TOS ==1 || lab0_Hlt2Topo2BodyBBDTDecision_TOS == 1 || lab0_Hlt2Topo3BodyBBDTDecision_TOS == 1))"
    # additional cuts applied to data sets
    #   configdict["AdditionalCuts"] = {}
    #   configdict["AdditionalCuts"]["All"]    = { "Data": "lab1_isMuon==0", # && lab5_PIDp<10",
    #                                              "MC"  : "lab1_M<200&&lab1_PIDK !=-1000.0&&lab2_FD_ORIVX > 0.&&(lab0_LifetimeFit_Dplus_ctau[0]>0)",
    #                                              "MCID":True, "MCTRUEID":True, "BKGCAT":True, "DsHypo":True}

    #   thigh = "lab3_PIDK<2.0 &&lab4_PIDK < 2. && lab5_PIDK < 2.&& lab3_PIDp < 5. && lab4_PIDp < 5. && lab5_PIDp < 5."
    #   lab13D0 = "&&fabs(sqrt(pow(sqrt(pow(493.7,2)+pow(lab1_P,2))+sqrt(pow(lab3_M,2)+pow(lab3_P,2)),2)-pow(lab1_PX+lab3_PX,2)-pow(lab1_PY+lab3_PY,2)-pow(lab1_PZ+lab3_PZ,2))-1870)>20.0";
    #   lab15D0 = "&&fabs(sqrt(pow(sqrt(pow(493.7,2)+pow(lab1_P,2))+sqrt(pow(lab5_M,2)+pow(lab5_P,2)),2)-pow(lab1_PX+lab5_PX,2)-pow(lab1_PY+lab5_PY,2)-pow(lab1_PZ+lab5_PZ,2))-1870)>20.0"

    #    configdict["AdditionalCuts"]["PiPiPi"]   = { "Data": "lab2_FDCHI2_ORIVX > 2", "MC" : "lab2_FDCHI2_ORIVX > 2"}
    #   configdict["AdditionalCuts"]["PiPiPi"]   = { "Data": thigh+lab13D0+lab15D0, "MC" : ""} #lab2_FDCHI2_ORIVX > 2"}
    #   configdict["AdditionalCuts"]["KPiPi"]  = { "Data": "lab2_FDCHI2_ORIVX > 9", "MC" : "lab2_FDCHI2_ORIVX > 9"}
    #   configdict["AdditionalCuts"]["PiPiPi"] = { "Data": "lab2_FDCHI2_ORIVX > 9", "MC" : "lab2_FDCHI2_ORIVX > 9"}

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5"
    }

    # additional variables in data sets
    if configdict["MoreVariables"] == True:
        configdict["AdditionalVariables"] = {}
        configdict["AdditionalVariables"]["lab0_OSCharm_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSCharm_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_OSElectronLatest_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSElectronLatest_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_OSKaonLatest_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSKaonLatest_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_OSMuonLatest_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSMuonLatest_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_OSVtxCh_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSVtxCh_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_SSPion_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_SSPion_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_SSProton_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_SSProton_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_OSCharm_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSCharm_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_OSElectronLatest_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSElectronLatest_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_OSKaonLatest_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSKaonLatest_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_OSMuonLatest_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSMuonLatest_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_OSVtxCh_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSVtxCh_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_SSPion_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_SSPion_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_SSProton_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_SSProton_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_P"] = {
            "Range": [0.0, 1600000.0],
            "InputName": "lab0_P"
        }
        configdict["AdditionalVariables"]["lab0_PT"] = {
            "Range": [0.0, 40000.0],
            "InputName": "lab0_PT"
        }
        configdict["AdditionalVariables"]["lab0_ETA"] = {
            "Range": [0.0, 6.0],
            "InputName": "lab0_ETA"
        }
        configdict["AdditionalVariables"]["nCandidate"] = {
            "Range": [0.0, 500.0],
            "InputName": "nCandidate"
        }
        configdict["AdditionalVariables"]["totCandidates"] = {
            "Range": [0.0, 500.0],
            "InputName": "totCandidates"
        }
        configdict["AdditionalVariables"]["runNumber"] = {
            "Range": [0.0, 1000000.0],
            "InputName": "runNumber"
        }
        configdict["AdditionalVariables"]["eventNumber"] = {
            "Range": [0.0, 10000000000.0],
            "InputName": "eventNumber"
        }
        configdict["AdditionalVariables"]["BDTGResponse_3"] = {
            "Range": [-1.0, 1.0],
            "InputName": "BDTGResponse_3"
        }
        configdict["AdditionalVariables"]["nSPDHits"] = {
            "Range": [0.0, 1400.0],
            "InputName": "nSPDHits"
        }
        configdict["AdditionalVariables"]["nTracksLinear"] = {
            "Range": [15.0, 1000.0],
            "InputName": "clone_nTracks"
        }

    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    ###                                                               MDfit fitting settings
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#

    # Bs signal shapes
    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"][
        "type"] = "IpatiaJohnsonSU"  #WithWidthRatio"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run2": {
            "All": 5367.51
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "20152016": {
            "All": 5367.51
        },
        "2017": {
            "All": 5367.51
        },
        "2018": {
            "All": 5367.51
        },
        "Fixed": False
    }
    #configdict["SignalShape"]["BeautyMass"]["sigmaI"]  = {"Run2": {"All" : 21.259},   "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["sigmaJ"]  = {"Run2": {"All" : 14.582},   "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["a1"]      = {"Run2": {"All" : 1.0886},   "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["a2"]      = {"Run2": {"All" : 1.4644},   "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["n1"]      = {"Run2": {"All" : 1.4891},   "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["n2"]      = {"Run2": {"All" : 4.2422},   "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["fb"]      = {"Run2": {"All" : 0.00000},   "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["zeta"]    = {"Run2": {"All" : 0.00000},   "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["l"]       = {"Run2": {"All" : -2.4782},   "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["nu"]      = {"Run2": {"All" : -0.23173}, "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["tau"]     = {"Run2": {"All" : 0.38464},  "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["fracI"]   = {"Run2": {"All" : 0.26563},  "Fixed" : True}

    #configdict["SignalShape"]["BeautyMass"]["sigmaI"]  = {"Run2": {"All" : 31.886},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["sigmaJ"]  = {"Run2": {"All" : 14.0607},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["a1"]      = {"Run2": {"All" : 0.464337},    "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["a2"]      = {"Run2": {"All" : 3.03669},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["n1"]      = {"Run2": {"All" : 2.56054},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["n2"]      = {"Run2": {"All" : 1.24849},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["fb"]      = {"Run2": {"All" : 0.00000},   "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["zeta"]    = {"Run2": {"All" : 0.00000},   "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["l"]       = {"Run2": {"All" : -1.41107},   "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["nu"]      = {"Run2": {"All" : -0.212432}, "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["tau"]     = {"Run2": {"All" : 0.331957}, "Fixed": True}

    #PIDCorr
    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "Run2": {
            "All": 34.7024
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "Run2": {
            "All": 14.0417
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "Run2": {
            "All": 0.608496
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "Run2": {
            "All": 2.96837
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run2": {
            "All": 7.20268
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run2": {
            "All": 1.1372
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "Run2": {
            "All": 0.00000
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "Run2": {
            "All": 0.00000
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "Run2": {
            "All": -1.39409
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "Run2": {
            "All": -0.138664
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "Run2": {
            "All": 0.339327
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "Run2": {
            "All": 0.16054
        },
        "Fixed": True
    }

    #PIDcor 2016-2019
    #configdict["SignalShape"]["BeautyMass"]["sigmaI"]  = {"Run2": {"All" : 34.779},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["sigmaJ"]  = {"Run2": {"All" : 14.048},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["a1"]      = {"Run2": {"All" : 0.61500},    "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["a2"]      = {"Run2": {"All" : 3.0667},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["n1"]      = {"Run2": {"All" : 6.5536},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["n2"]      = {"Run2": {"All" : 0.98559},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["fb"]      = {"Run2": {"All" : 0.00000},   "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["zeta"]    = {"Run2": {"All" : 0.00000},   "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["l"]       = {"Run2": {"All" : -1.3792},   "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["nu"]      = {"Run2": {"All" : -0.13991}, "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["tau"]     = {"Run2": {"All" : 0.34009}, "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["fracI"]   = {"Run2": {"All" : 0.16285},  "Fixed": True}

    #configdict["SignalShape"]["BeautyMass"]["sigmaI"]  = {"Run2": {"All" : 31.300},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["sigmaJ"]  = {"Run2": {"All" : 14.3714},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["a1"]      = {"Run2": {"All" : 0.622764},    "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["a2"]      = {"Run2": {"All" : 3.05975},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["n1"]      = {"Run2": {"All" : 33.4353},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["n2"]      = {"Run2": {"All" : 1.21081},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["fb"]      = {"Run2": {"All" : 0.00000},   "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["zeta"]    = {"Run2": {"All" : 0.00000},   "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["l"]       = {"Run2": {"All" : -1.54637},   "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["nu"]      = {"Run2": {"All" : -0.0742829}, "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["tau"]     = {"Run2": {"All" : 0.351178}, "Fixed": True}

    #configdict["SignalShape"]["BeautyMass"]["sigmaI"]  = {"Run2": {"All" : 32.7592},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["sigmaJ"]  = {"Run2": {"All" : 13.9896},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["a1"]      = {"Run2": {"All" : 0.689155},    "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["a2"]      = {"Run2": {"All" : 2.05142},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["n1"]      = {"Run2": {"All" : 12.8257},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["n2"]      = {"Run2": {"All" : 3.06025},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["fb"]      = {"Run2": {"All" : 0.00000},   "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["zeta"]    = {"Run2": {"All" : 0.00000},   "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["l"]       = {"Run2": {"All" : -1.64667},   "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["nu"]      = {"Run2": {"All" : -0.146755}, "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["tau"]     = {"Run2": {"All" : 0.337044}, "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["fracI"]   = {"Run2": {"All" :0.141661},  "Fixed": True}

    #configdict["SignalShape"]["BeautyMass"]["sigmaI"]  = {"Run2": {"NonRes" : 38.986,   "PhiPi" : 32.7592, "KstK" : 32.7592, "PiPiPi" : 32.7592},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["sigmaJ"]  = {"Run2": {"NonRes" : 14.019,   "PhiPi" : 32.7592, "KstK" : 32.7592, "PiPiPi" : 32.7592},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["a1"]      = {"Run2": {"NonRes" : 0.26225,  "PhiPi" : 32.7592, "KstK" : 32.7592, "PiPiPi" : 32.7592},    "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["a2"]      = {"Run2": {"NonRes" : 4.7480,   "PhiPi" : 32.7592, "KstK" : 32.7592, "PiPiPi" : 32.7592},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["n1"]      = {"Run2": {"NonRes" : 7.7183,   "PhiPi" : 32.7592, "KstK" : 32.7592, "PiPiPi" : 32.7592},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["n2"]      = {"Run2": {"NonRes" : 0.34970,   "PhiPi" : 32.7592, "KstK" : 32.7592, "PiPiPi" : 32.7592},     "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["fb"]      = {"Run2": {"All" : 0.00000},   "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["zeta"]    = {"Run2": {"All" : 0.00000},   "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["l"]       = {"Run2": {"NonRes" : -1.3108,  "PhiPi" : 32.7592, "KstK" : 32.7592, "PiPiPi" : 32.7592},   "Fixed" : True}
    #configdict["SignalShape"]["BeautyMass"]["nu"]      = {"Run2": {"NonRes" : -0.14406, "PhiPi" : 32.7592, "KstK" : 32.7592, "PiPiPi" : 32.7592}, "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["tau"]     = {"Run2": {"NonRes" : 0.33521,  "PhiPi" : 32.7592, "KstK" : 32.7592, "PiPiPi" : 32.7592}, "Fixed": True}
    #configdict["SignalShape"]["BeautyMass"]["fracI"]   = {"Run2": {"NonRes" :0.15032,   "PhiPi" : 32.7592, "KstK" : 32.7592, "PiPiPi" : 32.7592},  "Fixed": True}

    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "20152016": {
            "NonRes": 34.7024,
            "PhiPi": 34.7024,
            "KstK": 34.7024,
            "PiPiPi": 34.7024
        },
        "2017": {
            "NonRes": 34.7024,
            "PhiPi": 34.7024,
            "KstK": 34.7024,
            "PiPiPi": 34.7024
        },
        "2018": {
            "NonRes": 34.7024,
            "PhiPi": 34.7024,
            "KstK": 34.7024,
            "PiPiPi": 34.7024
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "20152016": {
            "NonRes": 14.0417,
            "PhiPi": 14.0417,
            "KstK": 14.0417,
            "PiPiPi": 14.0417
        },
        "2017": {
            "NonRes": 14.0417,
            "PhiPi": 14.0417,
            "KstK": 14.0417,
            "PiPiPi": 14.0417
        },
        "2018": {
            "NonRes": 14.0417,
            "PhiPi": 14.0417,
            "KstK": 14.0417,
            "PiPiPi": 14.0417
        },
        "Fixed": False
    }

    #configdict["SignalShape"]["BeautyMass"]["fracI"]   = {"Run2": {"NonRes" : 0.191413, "KstK" : 0.191413, "PhiPi" : 0.191413, "PiPiPi" : 0.191413},  "Fixed": False}
    configdict["SignalShape"]["BeautyMass"]["R"] = {
        "20152016": {
            "NonRes": 1.00,
            "PhiPi": 1.0,
            "Kstk": 1.0,
            "KPiPi": 1.0,
            "PiPiPi": 1.0
        },
        "2017": {
            "NonRes": 1.00,
            "PhiPi": 1.0,
            "Kstk": 1.0,
            "KPiPi": 1.0,
            "PiPiPi": 1.0
        },
        "2018": {
            "NonRes": 1.00,
            "PhiPi": 1.0,
            "Kstk": 1.0,
            "KPiPi": 1.0,
            "PiPiPi": 1.0
        },
        "Fixed": False
    }

    #Ds signal shapes
    configdict["SignalShape"]["CharmMass"] = {}
    configdict["SignalShape"]["CharmMass"][
        "type"] = "IpatiaPlusGaussian"  #WithWidthRatio"
    #configdict["SignalShape"]["CharmMass"]["mean"]    = {"Run2": {"All":1968.49}, "Fixed":False}
    #configdict["SignalShape"]["CharmMass"]["mean"]    = {"Run1": {"KKPi":1968.49, "PiPiPi":1968.49}, "Fixed":False}
    configdict["SignalShape"]["CharmMass"]["mean"] = {
        "20152016": {
            "All": 1968.49
        },
        "2017": {
            "All": 1968.49
        },
        "2018": {
            "All": 1968.49
        },
        "Fixed": False
    }
    #configdict["SignalShape"]["CharmMass"]["mean"]    = {"20152016": {"NonRes":1968.49, "PhiPi":1968.49, "KstK":1968.49, "PiPiPi":1968.49},
    #                                                     "2017":     {"NonRes":1968.49, "PhiPi":1968.49, "KstK":1968.49, "PiPiPi":1968.49},
    #                                                     "2018":     {"NonRes":1968.49, "PhiPi":1968.49, "KstK":1968.49, "PiPiPi":1968.49},
    #                                                     "Fixed":False}

    #pidcorr
    configdict["SignalShape"]["CharmMass"]["a1"] = {
        "Run2": {
            "NonRes": 2.1573,
            "PhiPi": 1.5177,
            "KstK": 2.2462,
            "PiPiPi": 0.77130
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["a2"] = {
        "Run2": {
            "NonRes": 3.3609,
            "PhiPi": 3.1516,
            "KstK": 2.4383,
            "PiPiPi": 0.92250
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["n1"] = {
        "Run2": {
            "NonRes": 3.7069,
            "PhiPi": 5.2263,
            "KstK": 2.7880,
            "PiPiPi": 49.930
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["n2"] = {
        "Run2": {
            "NonRes": 2.1238,
            "PhiPi": 2.7229,
            "KstK": 2.9748,
            "PiPiPi": 19.617
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["l"] = {
        "Run2": {
            "NonRes": -2.5053,
            "PhiPi": -2.0406,
            "KstK": -4.4109,
            "PiPiPi": -4.4100
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["fracI"] = {
        "Run2": {
            "NonRes": 0.85053,
            "PhiPi": 0.83030,
            "KstK": 0.83312,
            "PiPiPi": 0.54243
        },
        "Fixed": True
    }

    if configdict["SignalShape"]["CharmMass"][
            "type"] == "IpatiaPlusGaussianWithWidthRatio":
        configdict["SignalShape"]["CharmMass"]["sigmaG"] = {
            "Run2": {
                "NonRes": 5.4473,
                "PhiPi": 5.9455,
                "KstK": 8.6536,
                "PiPiPi": 8.5554
            },
            "Fixed": True
        }
        configdict["SignalShape"]["CharmMass"]["sigmaI"] = {
            "Run2": {
                "NonRes": 7.3839,
                "PhiPi": 7.7907,
                "KstK": 6.2577,
                "PiPiPi": 9.0294
            },
            "Fixed": True
        }
        configdict["SignalShape"]["CharmMass"]["R"] = {
            "20152016": {
                "NonRes": 1.00,
                "PhiPi": 1.0,
                "Kstk": 1.0,
                "KPiPi": 1.0,
                "PiPiPi": 1.0
            },
            "2017": {
                "NonRes": 1.00,
                "PhiPi": 1.0,
                "Kstk": 1.0,
                "KPiPi": 1.0,
                "PiPiPi": 1.0
            },
            "2018": {
                "NonRes": 1.00,
                "PhiPi": 1.0,
                "Kstk": 1.0,
                "KPiPi": 1.0,
                "PiPiPi": 1.0
            },
            "Fixed": False
        }  #
    else:
        configdict["SignalShape"]["CharmMass"]["sigmaG"] = {
            "20152016": {
                "NonRes": 5.4473,
                "PhiPi": 5.9455,
                "KstK": 8.6536,
                "PiPiPi": 8.5554
            },
            "2017": {
                "NonRes": 5.4473,
                "PhiPi": 5.9455,
                "KstK": 8.6536,
                "PiPiPi": 8.5554
            },
            "2018": {
                "NonRes": 5.4473,
                "PhiPi": 5.9455,
                "KstK": 8.6536,
                "PiPiPi": 8.5554
            },
            "Fixed": False
        }
        configdict["SignalShape"]["CharmMass"]["sigmaI"] = {
            "20152016": {
                "NonRes": 7.3839,
                "PhiPi": 7.7907,
                "KstK": 6.2577,
                "PiPiPi": 9.0294
            },
            "2017": {
                "NonRes": 7.3839,
                "PhiPi": 7.7907,
                "KstK": 6.2577,
                "PiPiPi": 9.0294
            },
            "2018": {
                "NonRes": 7.3839,
                "PhiPi": 7.7907,
                "KstK": 6.2577,
                "PiPiPi": 9.0294
            },
            "Fixed": False
        }

    configdict["SignalShape"]["CharmMass"]["fb"] = {
        "Run2": {
            "All": 0.00
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["zeta"] = {
        "Run2": {
            "All": 0.00
        },
        "Fixed": True
    }

    # combinatorial background
    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "DoubleExponential"
    #configdict["CombBkgShape"]["BeautyMass"]["cB2"]   = {"Run2": {"NonRes":0.0,       "PhiPi":0.0,       "KstK":0.0,      "KPiPi":0.0,         "PiPiPi":0.0},
    #                                                     "Fixed":True }
    configdict["CombBkgShape"]["BeautyMass"]["cB2"] = {
        "20152016": {
            "NonRes": -5.5365e-04,
            "PhiPi": -2.6327e-04,
            "KstK": -6.4593e-04,
            "PiPiPi": -4.7510e-04
        },
        "2017": {
            "NonRes": -5.5365e-04,
            "PhiPi": -2.6327e-04,
            "KstK": -6.4593e-04,
            "PiPiPi": -4.7510e-04
        },
        "2018": {
            "NonRes": -5.5365e-04,
            "PhiPi": -2.6327e-04,
            "KstK": -6.4593e-04,
            "PiPiPi": -4.7510e-04
        },
        "Fixed": True
    }
    configdict["CombBkgShape"]["BeautyMass"]["cB1"] = {
        "20152016": {
            "NonRes": -2.8299e-02,
            "PhiPi": -2.7999e-02,
            "KstK": -2.2682e-02,
            "PiPiPi": -2.9265e-02
        },
        "2017": {
            "NonRes": -2.8299e-02,
            "PhiPi": -2.7999e-02,
            "KstK": -2.2682e-02,
            "PiPiPi": -2.9265e-02
        },
        "2018": {
            "NonRes": -2.8299e-02,
            "PhiPi": -2.7999e-02,
            "KstK": -2.2682e-02,
            "PiPiPi": -2.9265e-02
        },
        "Fixed": False
    }

    configdict["CombBkgShape"]["BeautyMass"]["frac"] = {
        "20152016": {
            "NonRes": 4.3067e-01,
            "PhiPi": 6.5400e-01,
            "KstK": 3.7409e-01,
            "PiPiPi": 0.5
        },
        "2017": {
            "NonRes": 4.3067e-01,
            "PhiPi": 6.5400e-01,
            "KstK": 3.7409e-01,
            "PiPiPi": 0.5
        },
        "2018": {
            "NonRes": 4.3067e-01,
            "PhiPi": 6.5400e-01,
            "KstK": 3.7409e-01,
            "PiPiPi": 0.5
        },
        "Fixed": False
    }

    configdict["CombBkgShape"]["CharmMass"] = {}
    configdict["CombBkgShape"]["CharmMass"]["type"] = "ExponentialPlusSignal"
    configdict["CombBkgShape"]["CharmMass"]["cD"] = {
        "20152016": {
            "NonRes": -5.7520e-03,
            "PhiPi": -5.7273e-03,
            "KstK": -8.3967e-03,
            "KPiPi": -4.9193e-03,
            "PiPiPi": -4.5455e-02
        },
        "2017": {
            "NonRes": -5.7520e-03,
            "PhiPi": -5.7273e-03,
            "KstK": -8.3967e-03,
            "KPiPi": -4.9193e-03,
            "PiPiPi": -4.5455e-02
        },
        "2018": {
            "NonRes": -5.7520e-03,
            "PhiPi": -5.7273e-03,
            "KstK": -8.3967e-03,
            "KPiPi": -4.9193e-03,
            "PiPiPi": -4.5455e-02
        },
        "Fixed": False
    }
    configdict["CombBkgShape"]["CharmMass"]["fracD"] = {
        "20152016": {
            "NonRes": 0.88620,
            "PhiPi": 0.37379,
            "KstK": 0.59093,
            "KPiPi": 0.5,
            "PiPiPi": 0.5
        },
        "2017": {
            "NonRes": 0.88620,
            "PhiPi": 0.37379,
            "KstK": 0.59093,
            "KPiPi": 0.5,
            "PiPiPi": 0.5
        },
        "2018": {
            "NonRes": 0.88620,
            "PhiPi": 0.37379,
            "KstK": 0.59093,
            "KPiPi": 0.5,
            "PiPiPi": 0.5
        },
        "Fixed": False
    }

    configdict["CombBkgShape"]["BacPIDK"] = {}
    configdict["CombBkgShape"]["BacPIDK"]["type"] = "FixedWithKaonPion"
    configdict["CombBkgShape"]["BacPIDK"]["components"] = {
        "Kaon": True,
        "Pion": True,
        "Proton": False
    }
    configdict["CombBkgShape"]["BacPIDK"]["fracPIDK1"] = {
        "Run2": {
            "NonRes": 0.9,
            "PhiPi": 0.9,
            "KstK": 0.9,
            "KPiPi": 0.8,
            "PiPiPi": 0.8
        },
        "Fixed": False
    }
    configdict["CombBkgShape"]["BacPIDK"]["fracPIDK2"] = {
        "Run2": {
            "NonRes": 0.9,
            "PhiPi": 0.9,
            "KstK": 0.9,
            "KPiPi": 0.8,
            "PiPiPi": 0.8
        },
        "Fixed": False
    }

    #Bd2Dsh background
    #shape for BeautyMass, for CharmMass as well as BacPIDK taken by default the same as signal
    configdict["Bd2DsPiShape"] = {}
    configdict["Bd2DsPiShape"]["BeautyMass"] = {}
    if configdict["SignalShape"]["BeautyMass"][
            "type"] == "IpatiaJohnsonSUWithWidthRatio":  #WithWidthRatio":
        configdict["Bd2DsPiShape"]["BeautyMass"][
            "type"] = "ShiftedSignalIpatiaJohnsonSUWithWidthRatio"
    else:
        configdict["Bd2DsPiShape"]["BeautyMass"][
            "type"] = "ShiftedSignalIpatiaJohnsonSU"
    configdict["Bd2DsPiShape"]["BeautyMass"]["shift"] = {
        "Run2": {
            "All": -86.8
        },
        "Fixed": True
    }
    configdict["Bd2DsPiShape"]["BeautyMass"]["scale1"] = {
        "Run2": {
            "All": 1.00808721452
        },
        "Fixed": True
    }
    configdict["Bd2DsPiShape"]["BeautyMass"]["scale2"] = {
        "Run2": {
            "All": 1.03868673310
        },
        "Fixed": True
    }

    #
    configdict["AdditionalParameters"] = {}
    configdict["AdditionalParameters"]["g1_f1_frac"] = {
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.5,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }
    configdict["AdditionalParameters"]["g1_f2_frac"] = {
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.5,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }

    #expected yields
    configdict["Yields"] = {}
    #configdict["Yields"]["Bd2DPi"]          = {"2011": { "NonRes":502.0,   "PhiPi":25.5,    "KstK":366.0,   "KPiPi":0.0,     "PiPiPi":0.0},
    #                                           "2012": { "NonRes":1116.0,  "PhiPi":61.0,    "KstK":903.0,   "KPiPi":0.0,     "PiPiPi":0.0},  "Fixed":True}

    configdict["Yields"]["Bd2DPi"] = {
        "2015": {
            "NonRes": 192.0,
            "PhiPi": 3.41,
            "KstK": 56.45,
            "PiPiPi": 0.0
        },
        "2016": {
            "NonRes": 818.5,
            "PhiPi": 15.77,
            "KstK": 237.06,
            "PiPiPi": 0.0
        },
        "2017": {
            "NonRes": 931.5,
            "PhiPi": 22.11,
            "KstK": 265.23,
            "PiPiPi": 0.0
        },
        "2018": {
            "NonRes": 1122.68,
            "PhiPi": 21.59,
            "KstK": 319.38,
            "PiPiPi": 0.0
        },
        "Fixed": True,
    }

    #configdict["Yields"]["Bd2DPi"]          = {
    #                                           "2015": { "NonRes":196.60,    "PhiPi":3.82,    "KstK":57.47,   "KPiPi":0.74,      "PiPiPi":0.0},
    #                                           "2016": { "NonRes":1077.43,   "PhiPi":25.74,   "KstK":314.35,  "KPiPi":4.21,      "PiPiPi":0.0},
    #                                           "2017": { "NonRes":982.239,   "PhiPi":24.99,   "KstK":285.59,  "KPiPi":3.66,      "PiPiPi":0.0},
    #                                           "2018": { "NonRes":1275.19,   "PhiPi":31.58,   "KstK":368.34,  "KPiPi":4.25,      "PiPiPi":0.0},
    #                                           "Fixed":True,
    #                                           }
    #configdict["Yields"]["Lb2LcPi"]         = {"2011": { "NonRes":215.6,   "PhiPi":39.0,    "KstK":63.0,    "KPiPi":1.4,     "PiPiPi":0.0},
    #                                           "2012": { "NonRes":560.0,   "PhiPi":92.0,    "KstK":164.0,   "KPiPi":4.6,     "PiPiPi":0.0},  "Fixed":True}

    configdict["Yields"]["Lb2LcPi"] = {
        "2015": {
            "NonRes": 116.52,
            "PhiPi": 6.22,
            "KstK": 26.48,
            "PiPiPi": 0.0
        },
        "2016": {
            "NonRes": 664.81,
            "PhiPi": 62.27,
            "KstK": 142.3,
            "PiPiPi": 0.0
        },
        "2017": {
            "NonRes": 650.18,
            "PhiPi": 52.03,
            "KstK": 133.1,
            "PiPiPi": 0.0
        },
        "2018": {
            "NonRes": 765.80,
            "PhiPi": 74.43,
            "KstK": 159.3,
            "PiPiPi": 0.0
        },
        "Fixed": True
    }

    #configdict["Yields"]["Lb2LcPi"]         = {
    #                                           "2015": { "NonRes":138.0,   "PhiPi":13.0,    "KstK":34.0,    "KPiPi":1.30,     "PiPiPi":0.0},
    #                                           "2016": { "NonRes":736.0,   "PhiPi":69.0,    "KstK":165.0,   "KPiPi":7.0,     "PiPiPi":0.0},
    #                                           "2017": { "NonRes":762.0,   "PhiPi":70.0,    "KstK":171.0,   "KPiPi":7.0,      "PiPiPi":0.0},
    #                                           "2018": { "NonRes":887.0,   "PhiPi":87.0,    "KstK":205.0,   "KPiPi":8.0,     "PiPiPi": 0.0},
    #                                           "Fixed":True}
    #configdict["Yields"]["Lb2LcPi"]         = {
    #                                           "2015": { "NonRes":66.22,   "PhiPi":6.68,    "KstK":14.25,   "KPiPi":0.36,     "PiPiPi":0.0},
    #                                           "2016": { "NonRes":318.5,   "PhiPi":26.41,   "KstK":70.6,    "KPiPi":4.45,     "PiPiPi":0.0},
    #                                           "2017": { "NonRes":384.9,   "PhiPi":36.84,   "KstK":82.8,    "KPiPi":4.65,     "PiPiPi":0.0},
    #                                           "2018": { "NonRes":771.5,   "PhiPi":72.7,    "KstK":141.5,   "KPiPi":4.40,     "PiPiPi": 0.0},
    #                                           "Fixed":True}

    #configdict["Yields"]["Bs2DsK"]          = {"2015": { "NonRes":23.20,     "PhiPi":41.27,     "KstK":37.53,     "KPiPi":9.11,       "PiPiPi":23.20},
    #                                           "2016": { "NonRes":139.20,    "PhiPi":300.85,    "KstK":220.11,    "KPiPi":53.41,      "PiPiPi":141.55},
    #                                           "2017": { "NonRes":133.56,    "PhiPi":287.56,    "KstK":210.45,    "KPiPi":50.98,      "PiPiPi":133.81},
    #                                           "2018": { "NonRes":156.5,     "PhiPi":340.104,   "KstK":250.2,     "KPiPi":60.63,      "PiPiPi":156.00},
    #                                           "Fixed":True}

    configdict["Yields"]["Bs2DsK"] = {
        "2015": {
            "NonRes": 29.48,
            "PhiPi": 65.86,
            "KstK": 45.74,
            "PiPiPi": 30.07
        },  #32.00},
        "2016": {
            "NonRes": 151.5,
            "PhiPi": 339.4,
            "KstK": 235.56,
            "PiPiPi": 155.09
        },  #187.0},
        "2017": {
            "NonRes": 145.1,
            "PhiPi": 323.1,
            "KstK": 227.0,
            "PiPiPi": 148.946
        },  #174.0},
        "2018": {
            "NonRes": 173.5,
            "PhiPi": 391.4,
            "KstK": 277.3,
            "PiPiPi": 176.751
        },  #210.00},
        "Fixed": True
    }

    #configdict["Yields"]["Bs2DsK"]          = {"2011": { "NonRes":46.2,    "PhiPi":69.8,    "KstK":67.6,    "KPiPi":33.3,    "PiPiPi":61.6},
    #                                           "2012": { "NonRes":118.7,   "PhiPi":171.1,   "KstK":167.0,   "KPiPi":87.8,    "PiPiPi":156.0}, "Fixed":True}
    configdict["Yields"]["Bs2DsDsstPiRho"] = {
        "2015": {
            "NonRes": 1000.0,
            "PhiPi": 1000.0,
            "KstK": 1000.0,
            "KPiPi": 200.0,
            "PiPiPi": 200.0
        },
        "2016": {
            "NonRes": 2000.0,
            "PhiPi": 2000.0,
            "KstK": 2000.0,
            "KPiPi": 500.0,
            "PiPiPi": 500.0
        },
        "2017": {
            "NonRes": 2000.0,
            "PhiPi": 2000.0,
            "KstK": 2000.0,
            "KPiPi": 500.0,
            "PiPiPi": 500.0
        },
        "2018": {
            "NonRes": 2000.0,
            "PhiPi": 2000.0,
            "KstK": 2000.0,
            "KPiPi": 500.0,
            "PiPiPi": 500.0
        },
        "Fixed": False
    }
    configdict["Yields"]["CombBkg"] = {
        "2015": {
            "NonRes": 10000.0,
            "PhiPi": 20000.0,
            "KstK": 10000.0,
            "KPiPi": 10000.0,
            "PiPiPi": 10000.0
        },
        "2016": {
            "NonRes": 20000.0,
            "PhiPi": 30000.0,
            "KstK": 20000.0,
            "KPiPi": 20000.0,
            "PiPiPi": 20000.0
        },
        "2017": {
            "NonRes": 20000.0,
            "PhiPi": 30000.0,
            "KstK": 20000.0,
            "KPiPi": 20000.0,
            "PiPiPi": 20000.0
        },
        "2018": {
            "NonRes": 20000.0,
            "PhiPi": 40000.0,
            "KstK": 20000.0,
            "KPiPi": 20000.0,
            "PiPiPi": 20000.0
        },
        "Fixed": False
    }
    configdict["Yields"]["Signal"] = {
        "2015": {
            "NonRes": 10000.0,
            "PhiPi": 20000.0,
            "KstK": 10000.0,
            "KPiPi": 10000.0,
            "PiPiPi": 10000.0
        },
        "2016": {
            "NonRes": 20000.0,
            "PhiPi": 40000.0,
            "KstK": 20000.0,
            "KPiPi": 20000.0,
            "PiPiPi": 20000.0
        },
        "2017": {
            "NonRes": 40000.0,
            "PhiPi": 40000.0,
            "KstK": 30000.0,
            "KPiPi": 20000.0,
            "PiPiPi": 20000.0
        },
        "2018": {
            "NonRes": 40000.0,
            "PhiPi": 60000.0,
            "KstK": 40000.0,
            "KPiPi": 20000.0,
            "PiPiPi": 20000.0
        },
        "Fixed": False
    }

    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    ###                                                               MDfit plotting settings
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    import ROOT as R
    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = [
        "Sig", "CombBkg", "Bd2DPi", "Lb2LcPi", "Bs2DsDsstPiRho", "Bs2DsK"
    ]
    configdict["PlotSettings"]["colors"] = [
        R.kRed - 7, R.kBlue - 6, R.kOrange, R.kRed, R.kBlue - 10, R.kGreen + 3
    ]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.76, 0.9],
        "ScaleYSize": 1.2
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.9],
        "ScaleYSize": 1.7,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075
    }

    #configdict["LegendSettings"]["CharmMass"]  = {"Position":[0.20, 0.69, 0.93, 0.93], "TextSize": 0.05, "LHCbText":[0.8,0.66],
    #                                              "ScaleYSize":1.7, "SetLegendColumns":2, "LHCbTextSize":0.075 }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.20, 0.9],
        "ScaleYSize": 1.2
    }

    return configdict


if __name__ == '__main__':
    from Toys.configutils import main
    main(getconfig())
