###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
def getconfig():

    from Bs2DsPiConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    #configdict["BasicVariables"]["BDTG"]          = { "Range" : [-1.0,    1.0   ], "InputName" : "BDTGResponse_XGB_1"}
    configdict["BasicVariables"]["BeautyTime"] = {
        "Range": [0.4, 15.0],
        "InputName": "lab0_LifetimeFit_ctau"
    }
    configdict["BasicVariables"]["BeautyTimeErr"] = {
        "Range": [0.01, 0.1],
        "InputName": "lab0_LifetimeFit_ctauErr"
    }

    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {"Data": "lab1_isMuon==0"}

    pidp = "lab5_PIDp<10"
    D0kkpi = "lab34_MM<1800"
    pide3 = "lab3_PIDe<5"
    t = "&&"
    thigh = "lab3_PIDK<2.0 &&lab4_PIDK < 2. && lab5_PIDK < 2.&& lab3_PIDp < 5. && lab4_PIDp < 5. && lab5_PIDp < 5."
    lab13D0 = "fabs(sqrt(pow(sqrt(pow(493.7,2)+pow(lab1_P,2))+sqrt(pow(lab3_M,2)+pow(lab3_P,2)),2)-pow(lab1_PX+lab3_PX,2)-pow(lab1_PY+lab3_PY,2)-pow(lab1_PZ+lab3_PZ,2))-1864)>30.0"
    lab15D0 = "fabs(sqrt(pow(sqrt(pow(493.7,2)+pow(lab1_P,2))+sqrt(pow(lab5_M,2)+pow(lab5_P,2)),2)-pow(lab1_PX+lab5_PX,2)-pow(lab1_PY+lab5_PY,2)-pow(lab1_PZ+lab5_PZ,2))-1864)>30.0"

    configdict["AdditionalCuts"]["KKPi"] = {"Data": pidp + t + D0kkpi}
    #configdict["AdditionalCuts"]["NonRes"]  = { "Data": pidp+t+D0kkpi }
    #configdict["AdditionalCuts"]["KstK"]    = { "Data": pidp+t+D0kkpi }
    configdict["AdditionalCuts"]["PiPiPi"] = {
        "Data": thigh + t + lab13D0 + t + lab15D0 + t + pide3
    }

    return configdict
