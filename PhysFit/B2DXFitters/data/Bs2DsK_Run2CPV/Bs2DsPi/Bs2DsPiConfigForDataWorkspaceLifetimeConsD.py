def getconfig() :

    from Bs2DsPiConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    configdict["BasicVariables"]["BDTG"]          = { "Range" : [-1.0,    1.0   ], "InputName" : "BDTGResponse_XGB_1"}
    configdict["BasicVariables"]["BeautyTime"]    = { "Range" : [0.4,     15.0  ], "InputName" : "lab0_LifetimeFitConsD_ctau"}
    configdict["BasicVariables"]["BeautyTimeErr"] = { "Range" : [0.01,    0.1   ], "InputName" : "lab0_LifetimeFitConsD_ctauErr"}

    return configdict
