###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    configdict = {}

    # considered decay mode
    configdict["Decay"] = "Bs2DsK"
    configdict["CharmModes"] = {"NonRes", "PhiPi", "KstK", "KPiPi", "PiPiPi"}
    configdict["Backgrounds"] = [
        "Bd2DPi", "Bd2DK", "Lb2LcK", "Lb2LcPi", "Bs2DsPi", "Bs2DsRho",
        "Bs2DsstPi", "Bd2DsK", "Lb2Dsp", "Lb2Dsstp"
    ]
    # year of data taking
    configdict["YearOfDataTaking"] = {"2015", "2016", "2017", "2018"}

    # integrated luminosity in each year of data taking (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {
        "2015": {
            "Down": 0.18695,
            "Up": 0.14105
        },
        "2016": {
            "Down": 0.85996,
            "Up": 0.80504
        },
        "2017": {
            "Down": 0.87689,
            "Up": 0.83311
        },
        "2018": {
            "Down": 1.04846,
            "Up": 1.14154
        }
    }
    # file name with paths to MC/data samples
    configdict["dataName"] = "../data/Bs2DsK_Run2CPV/Bs2DsK/config_Bs2DsK.txt"
    #settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {
        "Directory": "PlotBs2DsK_Nominal",
        "Extension": "pdf"
    }

    configdict["MoreVariables"] = True

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5300, 5800],
        "InputName": "lab0_MassFitConsD_M"
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1930, 2015],
        "InputName": "lab2_MM"
    }
    configdict["BasicVariables"]["BeautyTime"] = {
        "Range": [0.4, 15.0],
        "InputName": "lab0_LifetimeFit_ctau"
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [3000.0, 650000.0],
        "InputName": "lab1_P"
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [400.0, 45000.0],
        "InputName": "lab1_PT"
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [1.61, 5.0],
        "InputName": "lab1_PIDK"
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 500.0],
        "InputName": "nTracks"
    }
    configdict["BasicVariables"]["BeautyTimeErr"] = {
        "Range": [0.01, 0.1],
        "InputName": "lab0_LifetimeFit_ctauErr"
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID"
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.475, 1.0],
        "InputName": "BDTGResponse_XGB_1"
    }
    configdict["BasicVariables"]["TagDecOS"] = {
        "Range": [-1.0, 1.0],
        "InputName": "lab0_TAGDECISION_OS"
    }
    configdict["BasicVariables"]["TagDecSS"] = {
        "Range": [-1.0, 1.0],
        "InputName": "lab0_SS_nnetKaon_DEC"
    }
    configdict["BasicVariables"]["MistagOS"] = {
        "Range": [0.0, 0.5],
        "InputName": "lab0_TAGOMEGA_OS"
    }
    configdict["BasicVariables"]["MistagSS"] = {
        "Range": [0.0, 0.5],
        "InputName": "lab0_SS_nnetKaon_PROB"
    }

    # additional cuts applied to data sets
    configdict["AdditionalCuts"] = {}

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5"
    }

    # additional variables in data sets
    if configdict["MoreVariables"] == True:
        configdict["AdditionalVariables"] = {}
        configdict["AdditionalVariables"]["lab0_OSCharm_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSCharm_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_OSElectronLatest_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSElectronLatest_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_OSKaonLatest_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSKaonLatest_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_OSMuonLatest_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSMuonLatest_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_OSVtxCh_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSVtxCh_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_SSPion_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_SSPion_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_SSProton_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_SSProton_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_OSCharm_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSCharm_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_OSElectronLatest_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSElectronLatest_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_OSKaonLatest_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSKaonLatest_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_OSMuonLatest_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSMuonLatest_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_OSVtxCh_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSVtxCh_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_SSPion_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_SSPion_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_SSProton_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_SSProton_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_P"] = {
            "Range": [0.0, 1600000.0],
            "InputName": "lab0_P"
        }
        configdict["AdditionalVariables"]["lab0_PT"] = {
            "Range": [0.0, 40000.0],
            "InputName": "lab0_PT"
        }
        configdict["AdditionalVariables"]["lab0_ETA"] = {
            "Range": [0.0, 6.0],
            "InputName": "lab0_ETA"
        }
        configdict["AdditionalVariables"]["nCandidate"] = {
            "Range": [0.0, 500.0],
            "InputName": "nCandidate"
        }
        configdict["AdditionalVariables"]["totCandidates"] = {
            "Range": [0.0, 500.0],
            "InputName": "totCandidates"
        }
        configdict["AdditionalVariables"]["runNumber"] = {
            "Range": [0.0, 1000000.0],
            "InputName": "runNumber"
        }
        configdict["AdditionalVariables"]["eventNumber"] = {
            "Range": [0.0, 10000000000.0],
            "InputName": "eventNumber"
        }
        configdict["AdditionalVariables"]["BDTGResponse_3"] = {
            "Range": [-1.0, 1.0],
            "InputName": "BDTGResponse_3"
        }
        configdict["AdditionalVariables"]["nSPDHits"] = {
            "Range": [0.0, 1400.0],
            "InputName": "nSPDHits"
        }
        configdict["AdditionalVariables"]["nTracksLinear"] = {
            "Range": [15.0, 1000.0],
            "InputName": "clone_nTracks"
        }

    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    ###                                                               MDfit fitting settings
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#

    # Bs signal shapes
    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "20152016": {
            "All": 5367.51
        },
        "20172018": {
            "All": 5367.51
        },
        "Fixed": False
    }
    # configdict["SignalShape"]["BeautyMass"]["mean"]    = { "20152016": {"phipi": 5367.93441, "kstk": 5367.88582, "nonres": 5367.83697, "kpipi": 5367.30625, "pipipi": 5367.09132},
    #                                                        "20172018": {"phipi": 5367.96347, "kstk": 5367.8441, "nonres": 5367.90132, "kpipi": 5367.24909, "pipipi": 5367.10061},
    #                                                        "Fixed": False }
    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "20152016": {
            "phipi": 20.73546,
            "kstk": 19.7633,
            "nonres": 21.86879,
            "kpipi": 21.33548,
            "pipipi": 19.97074
        },
        "20172018": {
            "phipi": 20.85894,
            "kstk": 20.11545,
            "nonres": 19.24201,
            "kpipi": 20.40957,
            "pipipi": 19.75617
        },
        "Fixed": True
    }  #False
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "20152016": {
            "phipi": 13.35251,
            "kstk": 13.21325,
            "nonres": 13.29861,
            "kpipi": 13.42541,
            "pipipi": 14.13975
        },
        "20172018": {
            "phipi": 13.16339,
            "kstk": 13.1827,
            "nonres": 13.08435,
            "kpipi": 13.08028,
            "pipipi": 13.75817
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "20152016": {
            "phipi": 1.20588,
            "kstk": 1.2978,
            "nonres": 1.0947,
            "kpipi": 1.02773,
            "pipipi": 1.2706
        },
        "20172018": {
            "phipi": 1.14102,
            "kstk": 1.26454,
            "nonres": 1.25829,
            "kpipi": 1.21514,
            "pipipi": 1.22364
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "20152016": {
            "phipi": 3.13529,
            "kstk": 3.29145,
            "nonres": 4.86273,
            "kpipi": 4.14649,
            "pipipi": 3.17661
        },
        "20172018": {
            "phipi": 3.18616,
            "kstk": 3.83724,
            "nonres": 2.8064,
            "kpipi": 3.9561,
            "pipipi": 3.20564
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "20152016": {
            "phipi": 1.37828,
            "kstk": 1.3537,
            "nonres": 1.36983,
            "kpipi": 1.51461,
            "pipipi": 1.28609
        },
        "20172018": {
            "phipi": 1.40509,
            "kstk": 1.3181,
            "nonres": 1.37558,
            "kpipi": 1.49505,
            "pipipi": 1.48981
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "20152016": {
            "phipi": 1.94936,
            "kstk": 1.78552,
            "nonres": 1.16441,
            "kpipi": 1.29876,
            "pipipi": 1.80314
        },
        "20172018": {
            "phipi": 2.01284,
            "kstk": 1.54064,
            "nonres": 2.14729,
            "kpipi": 1.58914,
            "pipipi": 1.95051
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "20152016": {
            "phipi": -1.6621,
            "kstk": -1.82136,
            "nonres": -1.47286,
            "kpipi": -1.52389,
            "pipipi": -1.74566
        },
        "20172018": {
            "phipi": -1.60272,
            "kstk": -1.66956,
            "nonres": -1.82916,
            "kpipi": -1.80447,
            "pipipi": -1.8581
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "20152016": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "20172018": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "20152016": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "20172018": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "20152016": {
            "phipi": -0.30099,
            "kstk": -0.2964,
            "nonres": -0.36133,
            "kpipi": -0.46055,
            "pipipi": -0.3198
        },
        "20172018": {
            "phipi": -0.32813,
            "kstk": -0.37267,
            "nonres": -0.33553,
            "kpipi": -0.43828,
            "pipipi": -0.3707
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "20152016": {
            "phipi": 0.32924,
            "kstk": 0.31484,
            "nonres": 0.29703,
            "kpipi": 0.28946,
            "pipipi": 0.37558
        },
        "20172018": {
            "phipi": 0.30999,
            "kstk": 0.32376,
            "nonres": 0.32808,
            "kpipi": 0.34077,
            "pipipi": 0.38447
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "Run2": {
            "All": 0.25451
        },
        "Fixed": True
    }  # 51005800: 0.26169

    #Ds signal shapes
    configdict["SignalShape"]["CharmMass"] = {}
    configdict["SignalShape"]["CharmMass"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["SignalShape"]["CharmMass"]["mean"] = {
        "20152016": {
            "All": 1968.7
        },
        "20172018": {
            "All": 1968.7
        },
        "Fixed": False
    }
    # configdict["SignalShape"]["CharmMass"]["mean"]     = { "20152016": {"phipi": 1968.73668, "kstk": 1968.84009, "nonres": 1968.76125, "kpipi": 1969.36232, "pipipi": 1969.44675},
    #                                                        "20172018": {"phipi": 1968.74468, "kstk": 1968.81854, "nonres": 1968.77198, "kpipi": 1969.31766, "pipipi": 1969.43694},
    #                                                        "Fixed": False }
    configdict["SignalShape"]["CharmMass"]["sigmaI"] = {
        "20152016": {
            "phipi": 26.58765,
            "kstk": 20.38451,
            "nonres": 25.49319,
            "kpipi": 27.04678,
            "pipipi": 30.22789
        },
        "20172018": {
            "phipi": 25.38847,
            "kstk": 28.72763,
            "nonres": 24.77959,
            "kpipi": 26.9545,
            "pipipi": 32.41699
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["sigmaJ"] = {
        "20152016": {
            "phipi": 6.34808,
            "kstk": 6.52773,
            "nonres": 6.33862,
            "kpipi": 7.97396,
            "pipipi": 9.20914
        },
        "20172018": {
            "phipi": 6.15832,
            "kstk": 6.43513,
            "nonres": 6.15912,
            "kpipi": 7.7362,
            "pipipi": 9.18997
        },
        "Fixed": False
    }
    configdict["SignalShape"]["CharmMass"]["a1"] = {
        "20152016": {
            "phipi": 0.20233,
            "kstk": 0.1186,
            "nonres": 0.16223,
            "kpipi": 0.14479,
            "pipipi": 0.20292
        },
        "20172018": {
            "phipi": 0.21165,
            "kstk": 0.1694,
            "nonres": 0.18945,
            "kpipi": 0.15539,
            "pipipi": 0.16495
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["a2"] = {
        "20152016": {
            "phipi": 0.6582,
            "kstk": 0.21311,
            "nonres": 0.33561,
            "kpipi": 0.328,
            "pipipi": 0.60521
        },
        "20172018": {
            "phipi": 0.65504,
            "kstk": 0.64867,
            "nonres": 0.44165,
            "kpipi": 0.35043,
            "pipipi": 0.5175
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["n1"] = {
        "20152016": {
            "phipi": 6.0,
            "kstk": 6.0,
            "nonres": 6.0,
            "kpipi": 6.0,
            "pipipi": 6.0
        },
        "20172018": {
            "phipi": 6.0,
            "kstk": 6.0,
            "nonres": 6.0,
            "kpipi": 6.0,
            "pipipi": 6.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["n2"] = {
        "20152016": {
            "phipi": 6.0,
            "kstk": 6.0,
            "nonres": 6.0,
            "kpipi": 6.0,
            "pipipi": 6.0
        },
        "20172018": {
            "phipi": 6.0,
            "kstk": 6.0,
            "nonres": 6.0,
            "kpipi": 6.0,
            "pipipi": 6.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["l"] = {
        "20152016": {
            "phipi": -1.1,
            "kstk": -1.1,
            "nonres": -1.1,
            "kpipi": -1.1,
            "pipipi": -1.1
        },
        "20172018": {
            "phipi": -1.1,
            "kstk": -1.1,
            "nonres": -1.1,
            "kpipi": -1.1,
            "pipipi": -1.1
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["fb"] = {
        "20152016": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "20172018": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["zeta"] = {
        "20152016": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "20172018": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["nu"] = {
        "20152016": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "20172018": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["tau"] = {
        "20152016": {
            "phipi": 0.49546,
            "kstk": 0.44741,
            "nonres": 0.44922,
            "kpipi": 0.48069,
            "pipipi": 0.39679
        },
        "20172018": {
            "phipi": 0.47359,
            "kstk": 0.46136,
            "nonres": 0.43839,
            "kpipi": 0.44298,
            "pipipi": 0.43176
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["fracI"] = {
        "20152016": {
            "phipi": 0.12377,
            "kstk": 0.06781,
            "nonres": 0.09872,
            "kpipi": 0.12241,
            "pipipi": 0.28399
        },
        "20172018": {
            "phipi": 0.13874,
            "kstk": 0.06785,
            "nonres": 0.12346,
            "kpipi": 0.13409,
            "pipipi": 0.20965
        },
        "Fixed": True
    }

    # combinatorial background
    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "DoubleExponential"
    configdict["CombBkgShape"]["BeautyMass"]["cB1"] = {
        "20152016": {
            "PhiPi": [-1.0960e-02, -0.08, 0.0],
            "KstK": [-9.0653e-03, -0.05, 0.0],
            "NonRes": [-9.2536e-03, -0.05, 0.0],
            "KPiPi": [-8.7816e-04, -0.05, 0.0],
            "PiPiPi": [-1.3254e-03, -0.05, 0.0]
        },
        "20172018": {
            "PhiPi": [-1.0960e-02, -0.08, 0.0],
            "KstK": [-9.0653e-03, -0.05, 0.0],
            "NonRes": [-9.2536e-03, -0.05, 0.0],
            "KPiPi": [-8.7816e-04, -0.05, 0.0],
            "PiPiPi": [-1.3254e-03, -0.05, 0.0]
        },
        "Fixed": False
    }
    configdict["CombBkgShape"]["BeautyMass"]["cB2"] = {
        "20152016": {
            "PhiPi": -5.2031e-04,
            "KstK": -6.2061e-04,
            "NonRes": -4.5085e-04,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "20172018": {
            "PhiPi": -5.2031e-04,
            "KstK": -6.2061e-04,
            "NonRes": -4.5085e-04,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "Fixed": True
    }
    configdict["CombBkgShape"]["BeautyMass"]["frac"] = {
        "20152016": {
            "PhiPi": 8.9264e-02,
            "KstK": 9.1456e-02,
            "NonRes": 1.3025e-01,
            "KPiPi": 5.9153e-01,
            "PiPiPi": 4.1545e-01
        },
        "20172018": {
            "PhiPi": 8.9264e-02,
            "KstK": 9.1456e-02,
            "NonRes": 1.3025e-01,
            "KPiPi": 5.9153e-01,
            "PiPiPi": 4.1545e-01
        },
        "Fixed": False
    }

    # # Run2
    # configdict["CombBkgShape"]["BeautyMass"]["cB1"]   = { "Run2": {"PhiPi":-1.0960e-02, "KstK":-9.0653e-03, "NonRes":-9.2536e-03, "KPiPi":-8.7816e-04, "PiPiPi":-1.3254e-03}, "Fixed": False}
    # configdict["CombBkgShape"]["BeautyMass"]["cB2"]   = { "Run2": {"PhiPi":-5.2031e-04, "KstK":-6.2061e-04, "NonRes":-4.5085e-04, "KPiPi":        0.0, "PiPiPi":        0.0}, "Fixed": True}
    # configdict["CombBkgShape"]["BeautyMass"]["frac"]  = { "Run2": {"PhiPi": 8.9264e-02, "KstK": 9.1456e-02, "NonRes": 1.3025e-01, "KPiPi": 5.9153e-01, "PiPiPi": 4.1545e-01}, "Fixed": False}

    configdict["CombBkgShape"]["CharmMass"] = {}
    configdict["CombBkgShape"]["CharmMass"]["type"] = "ExponentialPlusSignal"
    configdict["CombBkgShape"]["CharmMass"]["cD"] = {
        "20152016": {
            "PhiPi": [-6.6075e-03, -5.0e-2, 0.0],
            "KstK": [-3.3159e-03, -5.0e-2, 0.0],
            "NonRes": [-2.1752e-03, -5.0e-2, 0.0],
            "KPiPi": [-2.6575e-04, -5.0e-3, 0.0],
            "PiPiPi": [-1.8805e-03, -5.0e-2, 0.0]
        },
        "20172018": {
            "PhiPi": [-6.6075e-03, -5.0e-2, 0.0],
            "KstK": [-3.3159e-03, -5.0e-2, 0.0],
            "NonRes": [-2.1752e-03, -5.0e-2, 0.0],
            "KPiPi": [-2.6575e-04, -5.0e-3, 0.0],
            "PiPiPi": [-1.8805e-03, -5.0e-2, 0.0]
        },
        "Fixed": False
    }
    configdict["CombBkgShape"]["CharmMass"]["fracD"] = {
        "20152016": {
            "PhiPi": 3.5780e-01,
            "KstK": 5.4897e-01,
            "NonRes": 8.4653e-01,
            "KPiPi": 9.6697e-01,
            "PiPiPi": 9.1997e-01
        },
        "20172018": {
            "PhiPi": 3.5780e-01,
            "KstK": 5.4897e-01,
            "NonRes": 8.4653e-01,
            "KPiPi": 9.6697e-01,
            "PiPiPi": 9.1997e-01
        },
        "Fixed": False
    }

    # # Run2
    # configdict["CombBkgShape"]["CharmMass"]["cD"]    = { "Run2": {"PhiPi":[-6.6075e-03,-5.0e-2,0.0], "KstK":  [-3.3159e-03,-5.0e-2,0.0], "NonRes":[-2.1752e-03,-5.0e-2,0.0],
    #                                                               "KPiPi":[-2.6575e-04,-5.0e-3,0.0], "PiPiPi":[-1.8805e-03,-5.0e-2,0.0]}, "Fixed":False}
    # configdict["CombBkgShape"]["CharmMass"]["fracD"] =  {"Run2": {"PhiPi": 3.5780e-01, "KstK": 5.4897e-01, "NonRes": 8.4653e-01, "KPiPi": 9.6697e-01, "PiPiPi": 9.1997e-01}, "Fixed":False}

    configdict["AdditionalParameters"] = {}
    configdict["AdditionalParameters"]["g1_f1_frac"] = {
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.6,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }  #used <5300
    configdict["AdditionalParameters"]["g1_f2_frac"] = {
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.2,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }  #used <5250
    configdict["AdditionalParameters"]["g1_f3_frac"] = {
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.8,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }  #used <5150
    configdict["AdditionalParameters"]["g2_f1_frac"] = {
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.6,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }  # True
    configdict["AdditionalParameters"]["g2_f2_frac"] = {
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.5,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }
    configdict["AdditionalParameters"]["g2_f3_frac"] = {
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.8,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }  #used <5200
    configdict["AdditionalParameters"]["g3_f1_frac"] = {
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.75,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": True
    }  #0.5 for 5100
    configdict["AdditionalParameters"]["g5_f1_frac"] = {
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.92,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }

    # #Bs2DsPi background
    # #shape for BeautyMass, for CharmMass taken by default the same as signal
    # configdict["Bs2DsPiShape"] = {}
    # configdict["Bs2DsPiShape"]["BeautyMass"] = {}
    # configdict["Bs2DsPiShape"]["BeautyMass"]["type"]  = "Ipatia"
    # configdict["Bs2DsPiShape"]["BeautyMass"]["mean"]  = {"20152016": {"All": 5399.29}, "20172018": {"All": 5398.705},  "Fixed": True }
    # configdict["Bs2DsPiShape"]["BeautyMass"]["sigma"] = {"20152016": {"All": 27.621},  "20172018": {"All": 27.268},  "Fixed": True }
    # configdict["Bs2DsPiShape"]["BeautyMass"]["a1"]    = {"20152016": {"All": 1.1722},  "20172018": {"All": 1.1734}, "Fixed": True }
    # configdict["Bs2DsPiShape"]["BeautyMass"]["a2"]    = {"20152016": {"All": 1.6679},  "20172018": {"All": 1.3483}, "Fixed": True }
    # configdict["Bs2DsPiShape"]["BeautyMass"]["n1"]    = {"20152016": {"All": 1.1631},  "20172018": {"All": 1.1878}, "Fixed": True }
    # configdict["Bs2DsPiShape"]["BeautyMass"]["n2"]    = {"20152016": {"All": 2.8890},  "20172018": {"All": 2.7181},  "Fixed": True }
    # configdict["Bs2DsPiShape"]["BeautyMass"]["l"]     = {"20152016": {"All":-1.8797},  "20172018": {"All":-1.8549}, "Fixed": True }
    # configdict["Bs2DsPiShape"]["BeautyMass"]["fb"]    = {"20152016": {"All": 0.03},    "20172018": {"All": 0.03}, "Fixed": True }
    # configdict["Bs2DsPiShape"]["BeautyMass"]["zeta"]  = {"20152016": {"All": 0.0},     "20172018": {"All": 0.0},  "Fixed": True }

    #Bs2DsPi background
    #shape for BeautyMass, for CharmMass taken by default the same as signal
    configdict["Bs2DsPiShape"] = {}
    configdict["Bs2DsPiShape"]["BeautyMass"] = {}
    configdict["Bs2DsPiShape"]["BeautyMass"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["Bs2DsPiShape"]["BeautyMass"]["mean"] = {
        "20152016": {
            "All": 5415.0
        },
        "20172018": {
            "All": 5412.87
        },
        "Fixed": True
    }
    configdict["Bs2DsPiShape"]["BeautyMass"]["sigmaI"] = {
        "20152016": {
            "All": 6.8302e+01
        },
        "20172018": {
            "All": 6.6566e+01
        },
        "Fixed": True
    }
    configdict["Bs2DsPiShape"]["BeautyMass"]["sigmaJ"] = {
        "20152016": {
            "All": 2.6159e+01
        },
        "20172018": {
            "All": 2.3747e+01
        },
        "Fixed": True
    }
    configdict["Bs2DsPiShape"]["BeautyMass"]["a1"] = {
        "20152016": {
            "All": 7.5006e-01
        },
        "20172018": {
            "All": 7.9704e-01
        },
        "Fixed": True
    }
    configdict["Bs2DsPiShape"]["BeautyMass"]["a2"] = {
        "20152016": {
            "All": 1.2240e-01
        },
        "20172018": {
            "All": 9.8741e-02
        },
        "Fixed": True
    }
    configdict["Bs2DsPiShape"]["BeautyMass"]["n1"] = {
        "20152016": {
            "All": 1.5718
        },
        "20172018": {
            "All": 1.1453
        },
        "Fixed": True
    }
    configdict["Bs2DsPiShape"]["BeautyMass"]["n2"] = {
        "20152016": {
            "All": 3.4986
        },
        "20172018": {
            "All": 4.0707
        },
        "Fixed": True
    }
    configdict["Bs2DsPiShape"]["BeautyMass"]["l"] = {
        "20152016": {
            "All": -1.1
        },
        "20172018": {
            "All": -1.1
        },
        "Fixed": True
    }
    configdict["Bs2DsPiShape"]["BeautyMass"]["fb"] = {
        "20152016": {
            "All": 0.0
        },
        "20172018": {
            "All": 0.0
        },
        "Fixed": True
    }
    configdict["Bs2DsPiShape"]["BeautyMass"]["zeta"] = {
        "20152016": {
            "All": 0.0
        },
        "20172018": {
            "All": 0.0
        },
        "Fixed": True
    }
    configdict["Bs2DsPiShape"]["BeautyMass"]["tau"] = {
        "20152016": {
            "All": 5.6373e-01
        },
        "20172018": {
            "All": 5.0082e-01
        },
        "Fixed": True
    }
    configdict["Bs2DsPiShape"]["BeautyMass"]["nu"] = {
        "20152016": {
            "All": 1.4624
        },
        "20172018": {
            "All": 1.5068
        },
        "Fixed": True
    }
    configdict["Bs2DsPiShape"]["BeautyMass"]["fracI"] = {
        "20152016": {
            "All": 0.527
        },
        "20172018": {
            "All": 0.527
        },
        "Fixed": True
    }

    #Bs2DsstPi background
    #shape for BeautyMass, for CharmMass taken by default the same as signal
    configdict["Bs2DsstPiShape"] = {}
    configdict["Bs2DsstPiShape"]["BeautyMass"] = {}
    configdict["Bs2DsstPiShape"]["BeautyMass"]["type"] = "Ipatia"
    configdict["Bs2DsstPiShape"]["BeautyMass"]["mean"] = {
        "Run2": {
            "All": 5288.0
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["sigma"] = {
        "20152016": {
            "All": 63.525
        },
        "20172018": {
            "All": 64.970
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["a2"] = {
        "20152016": {
            "All": 1.0241
        },
        "20172018": {
            "All": 0.91529
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["n2"] = {
        "20152016": {
            "All": 2.7836
        },
        "20172018": {
            "All": 2.9243
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["l"] = {
        "20152016": {
            "All": -2.2
        },
        "20172018": {
            "All": -2.2
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["a1"] = {
        "20152016": {
            "All": 1.0
        },
        "20172018": {
            "All": 1.0
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["n1"] = {
        "20152016": {
            "All": 1.0
        },
        "20172018": {
            "All": 1.0
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["fb"] = {
        "20152016": {
            "All": 0.0
        },
        "20172018": {
            "All": 0.0
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["zeta"] = {
        "20152016": {
            "All": 0.0
        },
        "20172018": {
            "All": 0.0
        },
        "Fixed": True
    }

    #Bd2DsRho background
    #shape for BeautyMass, for CharmMass taken by default the same as signal
    configdict["Bs2DsRhoShape"] = {}
    configdict["Bs2DsRhoShape"]["BeautyMass"] = {}
    configdict["Bs2DsRhoShape"]["BeautyMass"]["type"] = "DoubleExponential"
    configdict["Bs2DsRhoShape"]["BeautyMass"]["cB1"] = {
        "20152016": {
            "All": -7.8776e-03
        },
        "20172018": {
            "All": -6.9675e-03
        },
        "Fixed": True
    }
    configdict["Bs2DsRhoShape"]["BeautyMass"]["cB2"] = {
        "20152016": {
            "All": -2.3968e-02
        },
        "20172018": {
            "All": -2.5049e-02
        },
        "Fixed": True
    }
    configdict["Bs2DsRhoShape"]["BeautyMass"]["frac"] = {
        "20152016": {
            "All": 6.3723e-01
        },
        "20172018": {
            "All": 6.0639e-01
        },
        "Fixed": True
    }

    #Bd2DsK background
    #shape for BeautyMass, for CharmMass taken by default the same as signal
    configdict["Bd2DsKShape"] = {}
    configdict["Bd2DsKShape"]["BeautyMass"] = {}
    configdict["Bd2DsKShape"]["BeautyMass"]["type"] = "Ipatia"
    configdict["Bd2DsKShape"]["BeautyMass"]["mean"] = {
        "Run2": {
            "All": 5279.2
        },
        "Fixed": True
    }  #5280.7 - 1.5
    configdict["Bd2DsKShape"]["BeautyMass"]["sigma"] = {
        "20152016": {
            "All": 12.223
        },
        "20172018": {
            "All": 13.109
        },
        "Fixed": True
    }
    configdict["Bd2DsKShape"]["BeautyMass"]["a2"] = {
        "20152016": {
            "All": 4.1855
        },
        "20172018": {
            "All": 3.0653
        },
        "Fixed": True
    }
    configdict["Bd2DsKShape"]["BeautyMass"]["n2"] = {
        "20152016": {
            "All": 1.5637
        },
        "20172018": {
            "All": 1.7036
        },
        "Fixed": True
    }
    configdict["Bd2DsKShape"]["BeautyMass"]["l"] = {
        "20152016": {
            "All": -2.1052
        },
        "20172018": {
            "All": -3.5275
        },
        "Fixed": True
    }
    configdict["Bd2DsKShape"]["BeautyMass"]["a1"] = {
        "20152016": {
            "All": 1.0
        },
        "20172018": {
            "All": 1.0
        },
        "Fixed": True
    }
    configdict["Bd2DsKShape"]["BeautyMass"]["n1"] = {
        "20152016": {
            "All": 1.0
        },
        "20172018": {
            "All": 1.0
        },
        "Fixed": True
    }
    configdict["Bd2DsKShape"]["BeautyMass"]["fb"] = {
        "20152016": {
            "All": 0.0
        },
        "20172018": {
            "All": 0.0
        },
        "Fixed": True
    }
    configdict["Bd2DsKShape"]["BeautyMass"]["zeta"] = {
        "20152016": {
            "All": 0.0
        },
        "20172018": {
            "All": 0.0
        },
        "Fixed": True
    }

    # if configdict["SignalShape"]["BeautyMass"]["type"]   == "IpatiaJohnsonSUWithWidthRatio": #WithWidthRatio":
    #     configdict["Bd2DsKShape"]["BeautyMass"]["type"]    = "ShiftedSignalIpatiaJohnsonSUWithWidthRatio"
    # else:
    #     configdict["Bd2DsKShape"]["BeautyMass"]["type"]    = "ShiftedSignalIpatiaJohnsonSU"
    # configdict["Bd2DsKShape"]["BeautyMass"]["shift"]   = {"Run2": {"All": -87.38}, "Fixed":True}
    # configdict["Bd2DsKShape"]["BeautyMass"]["scale1"]  = {"Run2": {"All": 1.0 },  "Fixed":True} # scale1 = scaleI
    # configdict["Bd2DsKShape"]["BeautyMass"]["scale2"]  = {"20152016": {"All": 0.975},
    #                                                       "20172018": {"All": 0.969},
    #                                                       "Fixed":True} # scale2 = scaleJ

    configdict["Yields"] = {}
    configdict["Yields"]["Bd2DK"] = {
        "2015": {
            "NonRes": 12.8,
            "PhiPi": 0.22,
            "KstK": 3.76,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2016": {
            "NonRes": 54.56,
            "PhiPi": 1.02,
            "KstK": 15.8,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2017": {
            "NonRes": 62.1,
            "PhiPi": 1.474,
            "KstK": 17.68,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2018": {
            "NonRes": 74.84,
            "PhiPi": 1.439,
            "KstK": 21.29,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "Fixed": True,
    }

    configdict["Yields"]["Bd2DPi"] = {
        "2015": {
            "NonRes": 5.12,
            "PhiPi": 0.09,
            "KstK": 1.51,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2016": {
            "NonRes": 21.824,
            "PhiPi": 0.42,
            "KstK": 6.32,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2017": {
            "NonRes": 24.84,
            "PhiPi": 0.59,
            "KstK": 7.07,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2018": {
            "NonRes": 29.936,
            "PhiPi": 0.57,
            "KstK": 8.5,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "Fixed": True,
    }

    configdict["Yields"]["Lb2LcK"] = {
        "2015": {
            "NonRes": 7.77,
            "PhiPi": 0.41,
            "KstK": 1.76,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2016": {
            "NonRes": 44.32,
            "PhiPi": 4.15,
            "KstK": 9.49,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2017": {
            "NonRes": 43.33,
            "PhiPi": 3.48,
            "KstK": 8.86,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2018": {
            "NonRes": 51.00,
            "PhiPi": 4.96,
            "KstK": 10.62,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "Fixed": True
    }

    configdict["Yields"]["Lb2LcPi"] = {
        "2015": {
            "NonRes": 3.08,
            "PhiPi": 0.16,
            "KstK": 0.7,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2016": {
            "NonRes": 17.72,
            "PhiPi": 1.66,
            "KstK": 3.80,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2017": {
            "NonRes": 17.32,
            "PhiPi": 1.39,
            "KstK": 3.55,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2018": {
            "NonRes": 20.40,
            "PhiPi": 1.98,
            "KstK": 4.25,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "Fixed": True
    }

    # configdict["Yields"]["Bs2DsDsstKKst"]  = { "2015": { "NonRes":2000.0, "PhiPi":2000.0, "KstK":2000.0, "KPiPi":1000.0,  "PiPiPi":1000.0},
    #                                            "2016": { "NonRes":4000.0, "PhiPi":4000.0, "KstK":4000.0, "KPiPi":2000.0, "PiPiPi":2000.0},
    #                                            "2017": { "NonRes":4000.0, "PhiPi":4000.0, "KstK":4000.0, "KPiPi":2000.0, "PiPiPi":2000.0},
    #                                            "2018": { "NonRes":4000.0, "PhiPi":4000.0, "KstK":4000.0, "KPiPi":2000.0, "PiPiPi":2000.0},
    #                                            "Fixed":False
    #                                            }

    # configdict["Yields"]["BsLb2DsDsstPPiRho"]  = {"2015": { "NonRes":1500.0, "PhiPi":1500.0, "KstK":1500.0, "KPiPi":400.0, "PiPiPi":400.0},
    #                                               "2016": { "NonRes":2000.0, "PhiPi":2000.0, "KstK":2000.0, "KPiPi":1000.0, "PiPiPi":1000.0},
    #                                               "2017": { "NonRes":3000.0, "PhiPi":3000.0, "KstK":3000.0, "KPiPi":1000.0, "PiPiPi":1000.0},
    #                                               "2018": { "NonRes":3000.0, "PhiPi":3000.0, "KstK":3000.0, "KPiPi":1000.0, "PiPiPi":1000.0},
    #                                               "Fixed":False
    #                                               }

    # configdict["Yields"]["CombBkg"]         = {"2015": { "NonRes":10000.0, "PhiPi":20000.0, "KstK":10000.0, "KPiPi":10000.0, "PiPiPi":10000.0},
    #                                            "2016": { "NonRes":20000.0, "PhiPi":30000.0, "KstK":20000.0, "KPiPi":20000.0, "PiPiPi":20000.0},
    #                                            "2017": { "NonRes":20000.0, "PhiPi":30000.0, "KstK":20000.0, "KPiPi":20000.0, "PiPiPi":20000.0},
    #                                            "2018": { "NonRes":20000.0, "PhiPi":40000.0, "KstK":20000.0, "KPiPi":20000.0, "PiPiPi":20000.0},
    #                                            "Fixed":False
    #                                            }

    # configdict["Yields"]["Signal"]          = {"2015": { "NonRes":10000.0, "PhiPi":20000.0, "KstK":10000.0, "KPiPi":10000.0, "PiPiPi":10000.0},
    #                                            "2016": { "NonRes":20000.0, "PhiPi":40000.0, "KstK":20000.0, "KPiPi":20000.0, "PiPiPi":20000.0},
    #                                            "2017": { "NonRes":40000.0, "PhiPi":40000.0, "KstK":30000.0, "KPiPi":20000.0, "PiPiPi":20000.0},
    #                                            "2018": { "NonRes":40000.0, "PhiPi":60000.0, "KstK":40000.0, "KPiPi":20000.0, "PiPiPi":20000.0},
    #                                            "Fixed":False
    #                                            }

    configdict["Yields"]["Bs2DsDsstKKst"] = {
        "20152016": {
            "PhiPi": 100.0,
            "KstK": 100.0,
            "NonRes": 100.0,
            "KPiPi": 100.0,
            "PiPiPi": 100.0
        },
        "20172018": {
            "PhiPi": 200.0,
            "KstK": 200.0,
            "NonRes": 200.0,
            "KPiPi": 200.0,
            "PiPiPi": 200.0
        },
        "Fixed": False
    }

    configdict["Yields"]["BsLb2DsDsstPPiRho"] = {
        "20152016": {
            "PhiPi": 2000.0,
            "KstK": 1500.0,
            "NonRes": 1200.0,
            "KPiPi": 500.0,
            "PiPiPi": 800.0
        },
        "20172018": {
            "PhiPi": 4000.0,
            "KstK": 2500.0,
            "NonRes": 2000.0,
            "KPiPi": 900.0,
            "PiPiPi": 1500.0
        },
        "Fixed": False
    }

    configdict["Yields"]["CombBkg"] = {
        "20152016": {
            "PhiPi": 900.0,
            "KstK": 900.0,
            "NonRes": 1600.0,
            "KPiPi": 1600.0,
            "PiPiPi": 2300.0
        },
        "20172018": {
            "PhiPi": 1400.0,
            "KstK": 1600.0,
            "NonRes": 3000.0,
            "KPiPi": 2700.0,
            "PiPiPi": 4000.0
        },
        "Fixed": False
    }

    configdict["Yields"]["Signal"] = {
        "20152016": {
            "PhiPi": 2900.0,
            "KstK": 2100.0,
            "NonRes": 1500.0,
            "KPiPi": 500.0,
            "PiPiPi": 1000.0
        },
        "20172018": {
            "PhiPi": 5500.0,
            "KstK": 4100.0,
            "NonRes": 3000.0,
            "KPiPi": 1000.0,
            "PiPiPi": 2000.0
        },
        "Fixed": False
    }

    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    ###                                                               MDfit plotting settings
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#

    import ROOT as R
    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = {
        "EPDF": [
            "Sig", "CombBkg", "Lb2LcK", "Lb2LcPi", "Bd2DK", "Bd2DPi",
            "BsLb2DsDsstPPiRho", "Bs2DsDsstKKst"
        ],
        "PDF": [
            "Sig", "CombBkg", "Lb2LcK", "Lb2LcPi", "Lb2DsDsstP",
            "Bs2DsDsstPiRho", "Bd2DK", "Bd2DPi", "Bs2DsDsstKKst"
        ],
        "Legend": [
            "Sig", "CombBkg", "Lb2LcKPi", "Lb2DsDsstP", "Bs2DsDsstPiRho",
            "Bd2DKPi", "Bs2DsDsstKKst"
        ]
    }
    configdict["PlotSettings"]["colors"] = {
        "PDF": [
            R.kRed - 7, R.kMagenta - 2, R.kGreen - 3, R.kGreen - 3,
            R.kYellow - 9, R.kBlue - 6, R.kRed, R.kRed, R.kBlue - 10
        ],
        "Legend": [
            R.kRed - 7, R.kMagenta - 2, R.kGreen - 3, R.kYellow - 9,
            R.kBlue - 6, R.kRed, R.kBlue - 10
        ]
    }

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.03,
        "LHCbText": [0.75, 0.9]
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.03,
        "LHCbText": [0.75, 0.9]
    }
    return configdict
