def getconfig() :

    configdict = {}
    
    from math import pi
    from math import log

    # considered decay mode                                                       
    configdict["Decay"] = "Bd2DPi"
    configdict["CharmModes"] = {"KPiPi"}
    configdict["Backgrounds"] = ["Bd2DK","Bd2DRho","Bd2DstPi","Lb2LcPi","Bs2DsPi"]
    # year of data taking
    configdict["YearOfDataTaking"] = {"2016"} # {"2015","2016","2017","2018"}
    # stripping (necessary in case of PIDK shapes)
    #configdict["Stripping"] = {"2011":"21r1", "2012":"21"}
    configdict["Stripping"] = {"2015":"24r1","2016":"28r1","2017":"29r2","2018":"34"}
    # integrated luminosity in each year of data taking (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {"2011": {"Down": 0.5600, "Up": 0.4200},
                                          "2012": {"Down": 0.9912, "Up": 0.9988},
                                          "2015": {"Down": 0.18695, "Up": 0.14105},
                                          "2016": {"Down": 0.85996, "Up": 0.80504},
                                          "2017": {"Down": 0.87689, "Up": 0.83311},
                                          "2018": {"Down": 1.04846, "Up": 1.14154}
                                         }
    # file name with paths to MC/data samples
    configdict["dataName"]   = "../data/Bs2DsK_Run2CPV/Bd2DPi/config_Bd2DPi.txt"
    #settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = { "Directory": "PlotBd2DPi", "Extension":"pdf"}

    # basic variables                                                                                        
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"]    = { "Range" : [5000,    6000    ], "InputName" : "lab0_MassFitConsD_M"}
    configdict["BasicVariables"]["CharmMass"]     = { "Range" : [1830,    1920    ], "InputName" : "lab2_MM"}
    configdict["BasicVariables"]["BeautyTime"]    = { "Range" : [0.4,     15.0    ], "InputName" : "lab0_LifetimeFitConsD_ctau"}
    configdict["BasicVariables"]["BacP"]          = { "Range" : [3000.0,  650000.0], "InputName" : "lab1_P"}
    configdict["BasicVariables"]["BacPT"]         = { "Range" : [400.0,   45000.0 ], "InputName" : "lab1_PT"}
    configdict["BasicVariables"]["BacPIDK"]       = { "Range" : [-150.0,  150.0   ], "InputName" : "lab1_PIDK"}
    configdict["BasicVariables"]["nTracks"]       = { "Range" : [15.0,    1000.0  ], "InputName" : "nTracks"}
    configdict["BasicVariables"]["BeautyTimeErr"] = { "Range" : [0.01,    0.1     ], "InputName" : "lab0_LifetimeFitConsD_ctauErr"}
    configdict["BasicVariables"]["BacCharge"]     = { "Range" : [-1000.0, 1000.0  ], "InputName" : "lab1_ID"}
    configdict["BasicVariables"]["BDTG"]          = { "Range" : [0.475,     1.0   ], "InputName" : "BDTGResponse_XGB_1"}
 
     # additional cuts applied to data sets                                                                                    
    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"]    = { "Data": "(lab0_LifetimeFit_Dplus_ctau[0]/0.299*1000)>0&&lab1_M<200&&lab1_PIDK!=-1000.0&&lab1_isMuon==0",
                                               "MC"  : "(lab0_LifetimeFit_Dplus_ctau[0]/0.299*1000)>0&&lab1_M<200&&lab1_PIDK!=-1000.0", "MCID":True, "MCTRUEID":True, "BKGCAT":True, "DsHypo":True }
    configdict["AdditionalCuts"]["KPiPi"]  = { "Data": "lab2_FD_ORIVX>0&&lab2_FDCHI2_ORIVX>9&&(lab3_M>200&&lab4_M<200&&lab5_M<200)",
                                               "MC"  : "lab2_FD_ORIVX>0&&lab2_FDCHI2_ORIVX>9&&(lab3_M>200&&lab4_M<200&&lab5_M<200)" }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {"Child1":"lab3","Child2":"lab4","Child3": "lab5"} #lab3 = K, lab4, lab5 = pi

    # weighting templates by PID eff/misID
    configdict["WeightingMassTemplates"] = {"Shift": { "2015": {"BeautyMass": -1.6, "CharmMass": 0.0},
                                                       "2016": {"BeautyMass": -1.6, "CharmMass": 0.0},
                                                       "2017": {"BeautyMass": -1.6, "CharmMass": 0.0},
                                                       "2018": {"BeautyMass": -1.6, "CharmMass": 0.0}}}
    #configdict["WeightingMassTemplates"] = {"RatioDataMC": { "2015":{"FileLabel":"#DataMC 2015", "Var":["lab1_P","nTracks"], "HistName":"histRatio"},
    #                                                         "2016":{"FileLabel":"#DataMC 2016", "Var":["lab1_P","nTracks"], "HistName":"histRatio"},
    #                                                         "2017":{"FileLabel":"#DataMC 2017", "Var":["lab1_P","nTracks"], "HistName":"histRatio"},
    #                                                         "2018":{"FileLabel":"#DataMC 2017", "Var":["lab1_P","nTracks"], "HistName":"histRatio"}}
    #                                       }
    #configdict["WeightingMassTemplates"] = { "Variables":["lab4_P","lab3_P"], "PIDBach": 0, "PIDChild": 0, "PIDProton": 5, "RatioDataMC":True }

    if False:  # switch more FT variables on/off
        configdict["AdditionalVariables"] = {}
        configdict["AdditionalVariables"]["lab0_OSCharm_TAGDEC"]             =  { "Range" : [ -2.0, 2.0 ], "InputName" : "lab0_OSCharm_TAGDEC"}
        configdict["AdditionalVariables"]["lab0_OSElectronLatest_TAGDEC"]    =  { "Range" : [ -2.0, 2.0 ], "InputName" : "lab0_OSElectronLatest_TAGDEC"}
        configdict["AdditionalVariables"]["lab0_OSKaonLatest_TAGDEC"]        =  { "Range" : [ -2.0, 2.0 ], "InputName" : "lab0_OSKaonLatest_TAGDEC"}
        configdict["AdditionalVariables"]["lab0_OSMuonLatest_TAGDEC"]        =  { "Range" : [ -2.0, 2.0 ], "InputName" : "lab0_OSMuonLatest_TAGDEC"}
        configdict["AdditionalVariables"]["lab0_OSVtxCh_TAGDEC"]             =  { "Range" : [ -2.0, 2.0 ], "InputName" : "lab0_OSVtxCh_TAGDEC"}
        configdict["AdditionalVariables"]["lab0_SSKaonLatest_TAGDEC"]        =  { "Range" : [ -2.0, 2.0 ], "InputName" : "lab0_SSKaonLatest_TAGDEC"}
        configdict["AdditionalVariables"]["lab0_SSPion_TAGDEC"]              =  { "Range" : [ -2.0, 2.0 ], "InputName" : "lab0_SSPion_TAGDEC"}
        configdict["AdditionalVariables"]["lab0_SSProton_TAGDEC"]            =  { "Range" : [ -2.0, 2.0 ], "InputName" : "lab0_SSProton_TAGDEC"}
        configdict["AdditionalVariables"]["lab0_OSCharm_TAGETA"]             =  { "Range" : [ -3.0, 1.0 ], "InputName" : "lab0_OSCharm_TAGETA"}
        configdict["AdditionalVariables"]["lab0_OSElectronLatest_TAGETA"]    =  { "Range" : [ -3.0, 1.0 ], "InputName" : "lab0_OSElectronLatest_TAGETA"}
        configdict["AdditionalVariables"]["lab0_OSKaonLatest_TAGETA"]        =  { "Range" : [ -3.0, 1.0 ], "InputName" : "lab0_OSKaonLatest_TAGETA"}
        configdict["AdditionalVariables"]["lab0_OSMuonLatest_TAGETA"]        =  { "Range" : [ -3.0, 1.0 ], "InputName" : "lab0_OSMuonLatest_TAGETA"}
        configdict["AdditionalVariables"]["lab0_OSVtxCh_TAGETA"]             =  { "Range" : [ -3.0, 1.0 ], "InputName" : "lab0_OSVtxCh_TAGETA"}
        configdict["AdditionalVariables"]["lab0_SSKaonLatest_TAGETA"]        =  { "Range" : [ -3.0, 1.0 ], "InputName" : "lab0_SSKaonLatest_TAGETA"}
        configdict["AdditionalVariables"]["lab0_SSPion_TAGETA"]              =  { "Range" : [ -3.0, 1.0 ], "InputName" : "lab0_SSPion_TAGETA"}
        configdict["AdditionalVariables"]["lab0_SSProton_TAGETA"]            =  { "Range" : [ -3.0, 1.0 ], "InputName" : "lab0_SSProton_TAGETA"}
        configdict["AdditionalVariables"]["lab0_P"]                          =  { "Range" : [ 0.0, 1600000.0 ],     "InputName" : "lab0_P"}
        configdict["AdditionalVariables"]["lab0_PT"]                         =  { "Range" : [ 0.0, 40000.0 ],       "InputName" : "lab0_PT"}
        configdict["AdditionalVariables"]["runNumber"]                       =  { "Range" : [ 0.0, 1000000.0 ],     "InputName" : "runNumber"}
        configdict["AdditionalVariables"]["eventNumber"]                     =  { "Range" : [ 0.0, 10000000000.0 ], "InputName" : "eventNumber"}

    if True: # DeltaGamma_s variables on/off
        configdict["AdditionalVariables"] = {}
        configdict["AdditionalVariables"]["lab0_P"]                          =  { "Range" : [ 0.0, 1600000.0 ],     "InputName" : "lab0_P"}
        configdict["AdditionalVariables"]["lab0_PT"]                         =  { "Range" : [ 0.0, 40000.0 ],       "InputName" : "lab0_PT"}
        configdict["AdditionalVariables"]["lab0_IPCHI2_OWNPV"]               =  { "Range" : [ 0.0, 10000.0 ],       "InputName" : "lab0_IPCHI2_OWNPV"}
        configdict["AdditionalVariables"]["lab1_IPCHI2_OWNPV"]               =  { "Range" : [ 0.0, 180000.0 ],      "InputName" : "lab1_IPCHI2_OWNPV"}
        configdict["AdditionalVariables"]["lab2_P"]                          =  { "Range" : [ 0.0, 1800000.0 ],     "InputName" : "lab2_P"}
        configdict["AdditionalVariables"]["lab2_PT"]                         =  { "Range" : [ 0.0, 90000.0 ],       "InputName" : "lab2_PT"}
        configdict["AdditionalVariables"]["lab2_IPCHI2_OWNPV"]               =  { "Range" : [ 0.0, 180000.0 ],      "InputName" : "lab2_IPCHI2_OWNPV"}
        configdict["AdditionalVariables"]["lab3_P"]                          =  { "Range" : [ 0.0, 1800000.0 ],     "InputName" : "lab3_P"}
        configdict["AdditionalVariables"]["lab3_PT"]                         =  { "Range" : [ 0.0, 90000.0 ],       "InputName" : "lab3_PT"}
        configdict["AdditionalVariables"]["lab3_IPCHI2_OWNPV"]               =  { "Range" : [ 0.0, 180000.0 ],      "InputName" : "lab3_IPCHI2_OWNPV"}
        configdict["AdditionalVariables"]["lab4_P"]                          =  { "Range" : [ 0.0, 1800000.0 ],     "InputName" : "lab4_P"}
        configdict["AdditionalVariables"]["lab4_PT"]                         =  { "Range" : [ 0.0, 90000.0 ],       "InputName" : "lab4_PT"}
        configdict["AdditionalVariables"]["lab4_IPCHI2_OWNPV"]               =  { "Range" : [ 0.0, 180000.0 ],      "InputName" : "lab4_IPCHI2_OWNPV"}
        configdict["AdditionalVariables"]["lab5_P"]                          =  { "Range" : [ 0.0, 1800000.0 ],     "InputName" : "lab5_P"}
        configdict["AdditionalVariables"]["lab5_PT"]                         =  { "Range" : [ 0.0, 90000.0 ],       "InputName" : "lab5_PT"}
        configdict["AdditionalVariables"]["lab5_IPCHI2_OWNPV"]               =  { "Range" : [ 0.0, 180000.0 ],      "InputName" : "lab5_IPCHI2_OWNPV"}
        configdict["AdditionalVariables"]["lab0_DIRA_OWNPV"]                 =  { "Range" : [ 0.0, 1.1 ],           "InputName" : "lab0_DIRA_OWNPV"}
        configdict["AdditionalVariables"]["lab0_LifetimeFit_Dplus_ctau"]           =  { "Range" : [ -50000.0, 400000.0 ], "InputName" : "lab0_LifetimeFit_Dplus_ctau"}
        configdict["AdditionalVariables"]["lab0_LifetimeFit_Dplus_ctauErr"]        =  { "Range" : [ 0.0, 10000.0 ],       "InputName" : "lab0_LifetimeFit_Dplus_ctauErr"}
        configdict["AdditionalVariables"]["lab0_LifetimeFit_Dplus_decayLength"]    =  { "Range" : [ -50000.0, 50000.0 ],  "InputName" : "lab0_LifetimeFit_Dplus_decayLength"}
        configdict["AdditionalVariables"]["lab0_LifetimeFit_Dplus_decayLengthErr"] =  { "Range" : [ 0.0, 2000.0 ],        "InputName" : "lab0_LifetimeFit_Dplus_decayLengthErr"}
        configdict["AdditionalVariables"]["lab0_LifetimeFitConsD_chi2"]      =  { "Range" : [ 0.0, 60000000.0 ],    "InputName" : "lab0_LifetimeFitConsD_chi2"}
        configdict["AdditionalVariables"]["lab0_LifetimeFitConsD_nDOF"]      =  { "Range" : [ -50.0, 50.0 ],        "InputName" : "lab0_LifetimeFitConsD_nDOF"}
        configdict["AdditionalVariables"]["lab0_ENDVERTEX_CHI2"]             =  { "Range" : [ 0.0, 50.0 ],      "InputName" : "lab0_ENDVERTEX_CHI2"}
        configdict["AdditionalVariables"]["lab2_ENDVERTEX_CHI2"]             =  { "Range" : [ 0.0, 50.0 ],      "InputName" : "lab2_ENDVERTEX_CHI2"}
        configdict["AdditionalVariables"]["lab0_LifetimeFit_chi2"]           =  { "Range" : [ 0.0, 10000000.0 ],      "InputName" : "lab0_LifetimeFit_chi2"}
        configdict["AdditionalVariables"]["lab0_LifetimeFit_nDOF"]           =  { "Range" : [ -50.0, 50.0 ],      "InputName" : "lab0_LifetimeFit_nDOF"}

    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    ###                                                               MDfit fitting settings
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#

    # Bs signal shapes
    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {} 
    configdict["SignalShape"]["BeautyMass"]["type"]   = "IpatiaPlusJohnsonSU"
    configdict["SignalShape"]["BeautyMass"]["mean"]   = {"Run2": {"All":5280.},       "Fixed":False}
    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {"Run2": {"All":3.3741e+01},  "Fixed":False}
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {"Run2": {"All":1.5968e+01},  "Fixed":False}
    configdict["SignalShape"]["BeautyMass"]["zeta"]   = {"Run2": {"All":0.},          "Fixed":True}
    configdict["SignalShape"]["BeautyMass"]["fb"]     = {"Run2": {"All":0.},          "Fixed":True}
    configdict["SignalShape"]["BeautyMass"]["l"]      = {"Run2": {"All":-1.2175e+00}, "Fixed":True}
    configdict["SignalShape"]["BeautyMass"]["a1"]     = {"Run2": {"All":7.1997e-01},  "Fixed":False}
    configdict["SignalShape"]["BeautyMass"]["a2"]     = {"Run2": {"All":3.6947e+00},  "Fixed":True}
    configdict["SignalShape"]["BeautyMass"]["n1"]     = {"Run2": {"All":1.4308e+00},  "Fixed":True}
    configdict["SignalShape"]["BeautyMass"]["n2"]     = {"Run2": {"All":1.0353e+00},  "Fixed":True}
    configdict["SignalShape"]["BeautyMass"]["nu"]     = {"Run2": {"All":-4.1310e-01}, "Fixed":True}
    configdict["SignalShape"]["BeautyMass"]["tau"]    = {"Run2": {"All":3.8621e-01},  "Fixed":True}
    configdict["SignalShape"]["BeautyMass"]["fracI"]  = {"Run2": {"All":3.6361e-01},  "Fixed":True}
    
    # Ds signal shapes (NOT USED, 1 DIM FIT)
    configdict["SignalShape"]["CharmMass"] = {}
    configdict["SignalShape"]["CharmMass"]["type"]    = "DoubleCrystalBallWithWidthRatio"
    configdict["SignalShape"]["CharmMass"]["mean"]    = {"Run2": {"All":1869.8},       "Fixed": False}
    configdict["SignalShape"]["CharmMass"]["sigma1"]  = {"Run2": {"KPiPi":11.501},     "Fixed": True}
    configdict["SignalShape"]["CharmMass"]["sigma2"]  = {"Run2": {"KPiPi":6.1237},     "Fixed": True}
    configdict["SignalShape"]["CharmMass"]["alpha1"]  = {"Run2": {"KPiPi":1.6382},     "Fixed": True}
    configdict["SignalShape"]["CharmMass"]["alpha2"]  = {"Run2": {"KPiPi":-3.4683},    "Fixed": True}
    configdict["SignalShape"]["CharmMass"]["n1"]      = {"Run2": {"KPiPi":4.8678},     "Fixed": True}
    configdict["SignalShape"]["CharmMass"]["n2"]      = {"Run2": {"KPiPi":4.3285e-06}, "Fixed": True}
    configdict["SignalShape"]["CharmMass"]["frac"]    = {"Run2": {"KPiPi":0.38916},    "Fixed": True}
    configdict["SignalShape"]["CharmMass"]["R"]       = {"Run2": {"KPiPi":1.0 },       "Fixed": False}

    # combinatorial background
    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"]  = "DoubleExponential"
    configdict["CombBkgShape"]["BeautyMass"]["cB1"]   = {"Run2": {"KPiPi":-5.e-03}, "Fixed": False}
    configdict["CombBkgShape"]["BeautyMass"]["cB2"]   = {"Run2": {"KPiPi":-2.e-06}, "Fixed": False}
    configdict["CombBkgShape"]["BeautyMass"]["frac"]  = {"Run2": {"KPiPi":0.75   }, "Fixed": False}
    # (NOT USED, 1 DIM FIT)
    configdict["CombBkgShape"]["CharmMass"] = {}
    configdict["CombBkgShape"]["CharmMass"]["type"]  = "ExponentialPlusDoubleCrystalBallWithWidthRatioSharedMean"
    configdict["CombBkgShape"]["CharmMass"]["sigma1"]  = {"Run2": {"KPiPi":11.501},          "Fixed":True}
    configdict["CombBkgShape"]["CharmMass"]["sigma2"]  = {"Run2": {"KPiPi":6.1237},          "Fixed":True}
    configdict["CombBkgShape"]["CharmMass"]["alpha1"]  = {"Run2": {"KPiPi":1.6382},          "Fixed":True}
    configdict["CombBkgShape"]["CharmMass"]["alpha2"]  = {"Run2": {"KPiPi":-3.4683},         "Fixed":True}
    configdict["CombBkgShape"]["CharmMass"]["n1"]      = {"Run2": {"KPiPi":4.8678},          "Fixed":True}
    configdict["CombBkgShape"]["CharmMass"]["n2"]      = {"Run2": {"KPiPi":4.3285e-06},      "Fixed":True}
    configdict["CombBkgShape"]["CharmMass"]["frac"]    = {"Run2": {"KPiPi":0.38916},         "Fixed":True}
    configdict["CombBkgShape"]["CharmMass"]["R"]       = {"Run2": {"KPiPi":1.5},             "Fixed":False}
    configdict["CombBkgShape"]["CharmMass"]["cB"]      = {"Run2": {"KPiPi":-1.9193e-03},     "Fixed":False}
    configdict["CombBkgShape"]["CharmMass"]["fracD"]   = {"Run2": {"KPiPi":0.5},             "Fixed":False}

    configdict["Bd2DstPiShape"] = {}
    configdict["Bd2DstPiShape"]["BeautyMass"]= {}
    configdict["Bd2DstPiShape"]["BeautyMass"]["type"]    = "DoubleGaussianSeparatedMean"                                   
    configdict["Bd2DstPiShape"]["BeautyMass"]["sigma1"]  = {"Run2": {"KPiPi":15.0},          "Fixed":False}
    configdict["Bd2DstPiShape"]["BeautyMass"]["sigma2"]  = {"Run2": {"KPiPi":15.0},          "Fixed":False}            
    configdict["Bd2DstPiShape"]["BeautyMass"]["mean"]    = {"Run2": {"KPiPi":5030.0},        "Fixed":False}   
    configdict["Bd2DstPiShape"]["BeautyMass"]["mean2"]   = {"Run2": {"KPiPi":5110.0},        "Fixed":False}   
    configdict["Bd2DstPiShape"]["BeautyMass"]["frac"]    = {"Run2": {"KPiPi":0.5},           "Fixed":False} 

    #configdict["DsCombinatorialShape"]["type"]  = "ExponentialPlusSignal" 
    #configdict["DsCombinatorialShape"]["type"]  = "ExponentialPlusSignal" 
    #configdict["DsCombinatorialShape"]["cD"]    = {"Run1": {"KPiPi":-3e-03},      "Fixed": False}
    #configdict["DsCombinatorialShape"]["fracD"] = {"Run1": {"KPiPi":0.5},         "Fixed": False}

    configdict["AdditionalParameters"] = {}
    configdict["AdditionalParameters"]["g1_f1_frac"] = {"Run2":{"All":{"Both":{"CentralValue":0.5, "Range":[0.0,1.0]}}}, "Fixed": False} 
    configdict["AdditionalParameters"]["g1_f2_frac"] = {"Run2":{"All":{"Both":{"CentralValue":0.5, "Range":[0.0,1.0]}}}, "Fixed": False}

    # expected yields: 
    # keep Bs2DsPi & Lb2LcPi fixed as for Run1 but scale yields according to luminosity 
    configdict["Yields"] = {}
    configdict["Yields"]["Bd2DK"]     = {"2015": {"KPiPi":  4000.0}, "2016": {"KPiPi": 20000.0}, "2017": {"KPiPi": 22000.0}, "Fixed": False} 
    configdict["Yields"]["Bd2DRho"]   = {"2015": {"KPiPi": 20000.0}, "2016": {"KPiPi":125000.0}, "2017": {"KPiPi":129000.0}, "Fixed": False}
    configdict["Yields"]["Bd2DstPi"]  = {"2015": {"KPiPi": 20000.0}, "2016": {"KPiPi": 80000.0}, "2017": {"KPiPi": 77000.0}, "Fixed": False}
    configdict["Yields"]["Bs2DsPi"]   = {"2015": {"KPiPi":   465.0}, "2016": {"KPiPi":  2855.0}, "2017": {"KPiPi":  2885.0}, "Fixed": True}
    configdict["Yields"]["Lb2LcPi"]   = {"2015": {"KPiPi":   110.0}, "2016": {"KPiPi":   680.0}, "2017": {"KPiPi":   685.0}, "Fixed": True}
    configdict["Yields"]["CombBkg"]   = {"2015": {"KPiPi": 20000.0}, "2016": {"KPiPi": 90000.0}, "2017": {"KPiPi": 82000.0}, "Fixed": False}
    configdict["Yields"]["Signal"]    = {"2015": {"KPiPi": 70000.0}, "2016": {"KPiPi":300000.0}, "2017": {"KPiPi":300000.0}, "Fixed": False}

    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#    
    ###                                                               MDfit plotting settings                                                             
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#               
    from ROOT import *
    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = ["Sig", "CombBkg", "Bd2DK", "Lb2LcPi", "Bs2DsPi", "Bd2DRho", "Bd2DstPi"] 
    configdict["PlotSettings"]["colors"] = [kRed-7, kBlue-6, kOrange, kRed, kBlue-10, kYellow, kBlue+2]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {"Position":[0.53, 0.45, 0.90, 0.91], "TextSize": 0.05, "LHCbText":[0.35,0.9], "ScaleYSize":1.2} # ScaleYSize was 2.5
    configdict["LegendSettings"]["CharmMass"]  = {"Position":[0.20, 0.69, 0.93, 0.93], "TextSize": 0.05, "LHCbText":[0.8,0.66],
                                                  "ScaleYSize":1.7, "SetLegendColumns":2, "LHCbTextSize":0.075 }
    configdict["LegendSettings"]["BacPIDK"]    = {"Position":[0.53, 0.45, 0.90, 0.91], "TextSize": 0.05, "LHCbText":[0.35,0.9], "ScaleYSize":1.2}

    return configdict
