def getconfig() :

    from Bs2DsstPiConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    configdict["BasicVariables"]["CharmMass"]     = { "Range" : [1080,    3150    ], "InputName" : "FDsst_DeltaM_M"}

    configdict["AdditionalCuts"]["All"]    = {"Data": "FBs_DsstMC_M>5100.&&FBs_DsstMC_M<5900.&&FDs_M>1950.&&FDs_M<1990.&&(FDsst_M-FDs_M)>195.&&(FDsst_M-FDs_M)<205.&&((FDsBac_M-FDs_M<3350||FDsBac_M-FDs_M>3500))&&FDelta_R<1.&&FBDT_Var>0.01&&FBac_P>1000.&&FBac_P<650000.&&FBac_Ptr>500.&&FBac_Ptr<45000.&&FBac_PIDK<0"}


    #----------------------------------------------------------------------------------------------------------------------------------------------------------------# 
    ###                                                               MDfit fitting settings                                                                           
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------# 

    configdict["SignalShape"]["BeautyMass"]["mean"]    = {"Run2": {"All": 5.3679e+03}, "Fixed":True}
    configdict["SignalShape"]["BeautyMass"]["R"]    =    {"Run2": {"All": 1.2},        "Fixed":True}

    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"]    = "RooHILLidni"
    configdict["CombBkgShape"]["BeautyMass"]["a"]       = {"Run2": {"All": 4.5557e+03}, "Fixed":True}
    configdict["CombBkgShape"]["BeautyMass"]["b"]       = {"Run2": {"All": 5.5201e+03}, "Fixed":True}
    configdict["CombBkgShape"]["BeautyMass"]["csi"]     = {"Run2": {"NonRes": 1.7093e+00,  "PhiPi": 1.7093e+00, "KstK":1.7093e+00, "KPiPi": 1.7093e+00, "PiPiPi": 1.7093e+00}, "Fixed":False}
    configdict["CombBkgShape"]["BeautyMass"]["shift"]   = {"Run2": {"All": 0.0},         "Fixed":True}
    configdict["CombBkgShape"]["BeautyMass"]["sigma"]   = {"Run2": {"NonRes": 4.0000e+01,  "PhiPi": 4.0000e+01, "KstK": 4.0000e+01, "KPiPi":3.13119e+01, "PiPiPi":3.13119e+01}, "Fixed":False}
    configdict["CombBkgShape"]["BeautyMass"]["R"]       = {"Run2": {"NonRes": 6.0821e+00,  "PhiPi": 6.0821e+00, "KstK": 6.0821e+00, "KPiPi":6.84048e+00, "PiPiPi":6.84048e+00},"Fixed":False}
    configdict["CombBkgShape"]["BeautyMass"]["frac"]    = {"Run2": {"NonRes": 3.9837e-01,  "PhiPi": 3.9837e-01, "KstK": 3.9837e-01, "KPiPi": 3.9837e-01, "PiPiPi": 3.9837e-01}, "Fixed":False}

    configdict["Yields"] = {}
    
    configdict["Yields"]["CombBkg"]    = {"2016": { "NonRes":20000.0, "PhiPi":20000.0, "KstK":20000.0, "KPiPi":20000.0, "PiPiPi":20000.0},
                                          "2015": { "NonRes":10000.0, "PhiPi":10000.0, "KstK":10000.0, "KPiPi":10000.0, "PiPiPi":10000.0}, "Fixed":False}
    #configdict["Yields"]["Signal"]     = {"2016": { "NonRes":0.0, "PhiPi":50000.0, "KstK":20000.0, "KPiPi":5000.0,  "PiPiPi":5000.0},
    #                                      "2015": { "NonRes":0.0, "PhiPi":50000.0, "KstK":20000.0, "KPiPi":2500.0,  "PiPiPi":2500.0}, "Fixed":False}
    
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------# 
    ###                                                               MDfit plotting settings                                                                         
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    
    from ROOT import *
    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = ["Sig","CombBkg"] 
    configdict["PlotSettings"]["colors"] = [kRed-7,kBlue-6] 

    return configdict
