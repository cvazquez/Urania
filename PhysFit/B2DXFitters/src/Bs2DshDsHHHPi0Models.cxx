// ROOT and RooFit includes
#include "RooFormulaVar.h"
#include "RooAddPdf.h"
#include "RooKeysPdf.h"
#include "RooExtendPdf.h"
#include "RooEffProd.h"
#include "RooGaussian.h"
#include "RooDecay.h"
#include "RooBDecay.h"
#include "RooCBShape.h"
#include "RooWorkspace.h"
#include "RooRealVar.h"
#include "RooExponential.h"
#include "RooProdPdf.h"
#include "TFile.h"
#include "TTree.h"
#include "RooDataSet.h"
#include "RooArgSet.h"
#include "RooHistPdf.h"
#include <string>
#include <vector>
#include <fstream>


#include "B2DXFitters/Bs2Dsh2011TDAnaModels.h"
#include "B2DXFitters/Bs2DshDsHHHPi0Models.h"
#include "B2DXFitters/GeneralUtils.h"
#include "B2DXFitters/RooBinned1DQuinticBase.h"

using namespace std;
using namespace GeneralUtils;
using namespace Bs2Dsh2011TDAnaModels;

namespace Bs2DshDsHHHPi0Models {

//Lorenzo
  //===============================================================================
  // Background 2D model for Bs->DsPi (Ds--> HHHPi0) mass fitter.
  //===============================================================================
  RooAbsPdf* build_Bs2DsPi_BKG_HHHPi0( RooWorkspace* work,
				       RooWorkspace* workInt,
				       std::vector <RooAbsReal*> obs,
				       std::vector <TString> types,
				       TString &samplemode,
				       TString merge,
				       bool debug)
  {
    if (debug == true)
      {
        cout<<"---------------------------------------------"<<endl;
	cout<<"[ERROR] Model not supported. Please create it"<<endl;
        cout<<"---------------------------------------------"<<endl;
      }

    if ( work != NULL ) {}
    if ( workInt != NULL ) {}
    if ( obs.size() <5 ) {} 
    if ( types.size() <5 ) {}
    if ( samplemode == "" ) {}
    if ( merge == "") {} 

    return NULL; 
  }


 //===============================================================================
  // Background 3D model for Bs->DsK (Ds--> HHHPi0) mass fitter.
  //===============================================================================
  RooAbsPdf* build_Bs2DsK_BKG_HHHPi0( RooWorkspace* work,
				      RooWorkspace* workInt,
				      std::vector <RooAbsReal*> obs,
				      std::vector <TString> types,
				      TString &samplemode,
				      TString merge,
				      bool debug)
  {
 
    if (debug == true)
      {
        cout<<"---------------------------------------------"<<endl;
        cout<<"[ERROR] Model not supported. Please create it"<<endl;
        cout<<"---------------------------------------------"<<endl;
      }

    if ( work != NULL ) {}
    if ( workInt != NULL ) {}
    if ( obs.size()<5 ) {}
    if ( types.size() <5 ) {}
    if ( samplemode == "" ) {}
    if ( merge == "") {} 

    return NULL;
  }

}
