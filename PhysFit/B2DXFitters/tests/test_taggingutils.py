#!/usr/bin/env python
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# --------------------------------------------------------------------------- #
#                                                                             #
#   Python script to test the taggingutils module                             #
#                                                                             #
#   Example usage:                                                            #
#      ./test_taggingutils.py                                                 #
#                                                                             #
#   Author: Eduardo Rodrigues                                                 #
#   Date  : 09 / 06 / 2011                                                    #
#                                                                             #
# --------------------------------------------------------------------------- #

from __future__ import print_function
from ROOT import RooCategory, RooRealVar
from B2DXFitters.taggingutils import tagEfficiencyWeight

mixState = RooCategory('mixstate', 'B/Bbar -> D pi mixing state')
mixState.defineType("unmixed", 1)
mixState.defineType("mixed", -1)
mixState.defineType("untagged", 0)

SigTagEff = 0.25
sigTagEff = RooRealVar("sigTagEff", "Signal tagging efficiency", SigTagEff, 0.,
                       1.)

sigTagWeight = tagEfficiencyWeight(mixState, sigTagEff, 'Bd2DPi')

sigTagWeight.Print('v')

print('\ntafEff =', sigTagEff.getVal())
mixState.setIndex(0)
print('mixState = %2d  =>  tagWeight = %f' % \
      ( mixState.getIndex(), sigTagWeight.getVal() ))
mixState.setIndex(1)
print('mixState = %2d  =>  tagWeight = %f' % \
      ( mixState.getIndex(), sigTagWeight.getVal() ))
mixState.setIndex(-1)
print('mixState = %2d  =>  tagWeight = %f' % \
      ( mixState.getIndex(), sigTagWeight.getVal() ))
