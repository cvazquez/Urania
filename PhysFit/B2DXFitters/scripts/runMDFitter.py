#!/usr/bin/env python
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# --------------------------------------------------------------------------- #
#                                                                             #
#   Python script to run a mass fit on data for B -> DX                       #
#   with the FitMeTool fitter                                                 #
#                                                                             #
#   Example usage:                                                            #
#      python runMDFitter.py --fileName work_bsdspi.root --merge -m both      #
#                            --configName Bs2DsPiConfigForNominalMassFitBDT   #
#                            --debug --year 2011 --dim 3 --mode all           #
#                            --save WS_Mass_DsPi.root                         #
#                                                                             #
#   Author: Agnieszka Dziurda                                                 #
#   mail: agnieszka.dziurda@cern.ch                                           #
#   date: 28.06.2015                                                          #
#                                                                             #
# --------------------------------------------------------------------------- #
# -----------------------------------------------------------------------------
# settings for running without GaudiPython
# -----------------------------------------------------------------------------
""":"
# This part is run by the shell. It does some setup which is convenient to save
# work in common use cases.

# make sure the environment is set up properly
if test -n "$CMTCONFIG" \
         -a -f $B2DXFITTERSROOT/$CMTCONFIG/libB2DXFittersDict.so \
         -a -f $B2DXFITTERSROOT/$CMTCONFIG/libB2DXFittersLib.so; then
    # all ok, software environment set up correctly, so don't need to do
    # anything
    true
else
    if test -n "$CMTCONFIG"; then
        # clean up incomplete LHCb software environment so we can run
        # standalone
        echo Cleaning up incomplete LHCb software environment.
        PYTHONPATH=`echo $PYTHONPATH | tr ':' '\n' | \
            egrep -v "^($User_release_area|$MYSITEROOT/lhcb)" | \
            tr '\n' ':' | sed -e 's/:$//'`
        export PYTHONPATH
        LD_LIBRARY_PATH=`echo $LD_LIBRARY_PATH | tr ':' '\n' | \
            egrep -v "^($User_release_area|$MYSITEROOT/lhcb)" | \
            tr '\n' ':' | sed -e 's/:$//'`
	export LD_LIBRARY_PATH
        exec env -u CMTCONFIG -u B2DXFITTERSROOT "$0" "$@"
    fi
    # automatic set up in standalone build mode
    if test -z "$B2DXFITTERSROOT"; then
        cwd="$(pwd)"
        if test -z "$(dirname $0)"; then
            # have to guess location of setup.sh
            cd ../standalone
            . ./setup.sh
            cd "$cwd"
        else
            # know where to look for setup.sh
            cd "$(dirname $0)"/../standalone
            . ./setup.sh
            cd "$cwd"
        fi
        unset cwd
    fi
fi
# figure out which custom allocators are available
# prefer jemalloc over tcmalloc
for i in libjemalloc libtcmalloc; do
    for j in `echo "$LD_LIBRARY_PATH" | tr ':' ' '` \
            /usr/local/lib /usr/lib /lib; do
        for k in `find "$j" -name "$i"'*.so.?' | sort -r`; do
            if test \! -e "$k"; then
                continue
            fi
            echo adding $k to LD_PRELOAD
            if test -z "$LD_PRELOAD"; then
                export LD_PRELOAD="$k"
                break 3
            else
                export LD_PRELOAD="$LD_PRELOAD":"$k"
                break 3
            fi
        done
    done
done
# set batch scheduling (if schedtool is available)
schedtool="`which schedtool 2>/dev/zero`"
if test -n "$schedtool" -a -x "$schedtool"; then
    echo "enabling batch scheduling for this job (schedtool -B)"
    schedtool="$schedtool -B -e"
else
    schedtool=""
fi

# set ulimit to protect against bugs which crash the machine: 2G vmem max,
# no more then 8M stack
ulimit -v $((2048 * 1024))
ulimit -s $((   8 * 1024))

# trampoline into python
exec $schedtool /usr/bin/time -v env python -O -- "$0" "$@"
"""
from __future__ import print_function

__doc__ = """ real docstring """
#"
# -----------------------------------------------------------------------------
# Load necessary libraries
# -----------------------------------------------------------------------------
#from B2DXFitters import *
#from B2DXFitters import GeneralUtils
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True  # noqa
from B2DXFitters.WS import WS as WS
from ROOT import *
from optparse import OptionParser
from math import pi, log
from os.path import exists
import os, sys, gc
gROOT.SetBatch()

# -----------------------------------------------------------------------------
# Configuration settings
# -----------------------------------------------------------------------------

# MISCELLANEOUS
bName = 'Bs'
dName = 'Ds'
bdName = 'Bd'

#-----------------------------------------------------------------------------


def getTotalBkgPDF(myconfigfile, obs, types, workspace, workInt, merge, bound,
                   sm, bkgs, bkgShapes, debug):

    bkgPDF = []
    cdm = ["NonRes", "PhiPi", "KstK", "KPiPi", "PiPiPi", "KKPi", "pKPi"]
    ot = [True]

    for i in range(0, bound):
        mm = GeneralUtils.GetModeCapital(sm[i], debug)
        if (myconfigfile["Decay"] == "Bs2DsPi"):
            if (mm in cdm):
                bkgPDF.append(
                    Bs2DshModels.build_Bs2DsPi_BKG_MDFitter(
                        workspace, workInt, obs, bkgShapes, sm[i], merge,
                        debug))
        elif (myconfigfile["Decay"] == "Bs2DsK"):
            if (mm in cdm):
                bkgPDF.append(
                    Bs2DshModels.build_Bs2DsK_BKG_MDFitter(
                        workspace, workInt, obs, bkgShapes, sm[i], merge,
                        debug))
        elif (myconfigfile["Decay"] == "Bs2DsstK"):
            if (mm in cdm):
                bkgPDF.append(
                    Bs2DssthModels.build_Bs2DsstK_BKG(workspace, workInt, obs,
                                                      bkgShapes, sm[i], merge,
                                                      debug))
        elif (myconfigfile["Decay"] == "Lb2Dsp"):
            if (mm in cdm):
                bkgPDF.append(
                    Lb2XhModels.build_Lb2Dsp_BKG(workspace, workInt, obs,
                                                 bkgShapes, sm[i], merge,
                                                 debug))
        else:
            if (mm in cdm):
                bkgPDF.append(
                    Bs2Dsh2011TDAnaModels.buildGeneralPdfMDFit(
                        workspace, workInt, bkgs, obs, bkgShapes, sm[i], merge,
                        debug))
                # if you need specific declaration there are still functions:
                #      Bs2DssthModels.build_Bs2DsstPi_BKG
                #      Bd2DhModels.build_Bd2DPi_BKG_MDFitter
                #      Bd2DhModels.build_Bd2DK_BKG_MDFitter

    return bkgPDF


#------------------------------------------------------------------------------
def runMDFitter(debug,
                sample,
                mode,
                sweight,
                fileNameAll,
                workName,
                sweightName,
                configName,
                wider,
                merge,
                dim,
                fileDataName,
                year,
                binned,
                numCPU=4):

    # Get the configuration file
    myconfigfilegrabber = __import__(
        configName, fromlist=['getconfig']).getconfig
    myconfigfile = myconfigfilegrabber()

    print("==========================================================")
    print("RUN MD FITTER IS RUNNING WITH THE FOLLOWING CONFIGURATION OPTIONS")
    for option in myconfigfile:
        if option == "constParams":
            for param in myconfigfile[option]:
                print(param, "is constant in the fit")
        else:
            print(option, " = ", myconfigfile[option])
    print("==========================================================")

    treeStorageFile = TFile(
        args.wsname.replace('.root', '_tree.root'), 'RECREATE')
    RooAbsData.setDefaultStorageType(RooAbsData.Tree)

    config = TString("../data/") + TString(configName) + TString(".py")
    from B2DXFitters.MDFitSettingTranslator import Translator

    from B2DXFitters.mdfitutils import getExpectedValue as getExpectedValue
    from B2DXFitters.mdfitutils import getExpectedYield as getExpectedYield
    from B2DXFitters.mdfitutils import setConstantIfSoConfigured as setConstantIfSoConfigured
    from B2DXFitters.mdfitutils import getObservables as getObservables
    from B2DXFitters.mdfitutils import readVariables as readVariables
    from B2DXFitters.mdfitutils import getSigOrCombPDF as getSigOrCombPDF
    from B2DXFitters.mdfitutils import getType as getType
    from B2DXFitters.mdfitutils import getPDFNameFromConfig as getPDFNameFromConfig
    from B2DXFitters.mdfitutils import getPIDKComponents as getPIDKComponents
    from B2DXFitters.mdfitutils import getShapeTypes as getShapeTypes
    from B2DXFitters.mdfitutils import readVariablesForShapes as readVariablesForShapes
    from B2DXFitters.mdfitutils import getBkgTypes as getBkgTypes
    from B2DXFitters.mdfitutils import checkMerge as checkMerge
    from B2DXFitters.mdfitutils import getSampleModeYear as getSampleModeYear
    from B2DXFitters.mdfitutils import getStdVector as getStdVector
    from B2DXFitters.mdfitutils import checkDuplications as checkDuplications
    from B2DXFitters.mdfitutils import getConstrainedIfSoConfigured as getConstrainedIfSoConfigured
    from B2DXFitters.mdfitutils import getErrorConstrained as getErrorConstrained
    from B2DXFitters.mdfitutils import collectGaussianConstraints as collectGaussianConstraints

    mdt = Translator(myconfigfile, "MDSettings", False)

    MDSettings = mdt.getConfig()
    MDSettings.Print("v")

    workNameTS = TString(workName)
    workspace = []
    workspace.append(
        GeneralUtils.LoadWorkspace(TString(fileNameAll), workNameTS, debug))
    if fileDataName == "":
        workData = workspace[0]
    else:
        workData = GeneralUtils.LoadWorkspace(
            TString(fileDataName), workNameTS, debug)

    configNameTS = TString(configName)

    observables = getObservables(MDSettings, workData, debug)
    beautyMass = observables.find(MDSettings.GetMassBVarOutName().Data())
    charmMass = observables.find(MDSettings.GetMassDVarOutName().Data())
    bacPIDK = observables.find(MDSettings.GetPIDKVarOutName().Data())
    obs = [beautyMass, charmMass, bacPIDK]
    obsName = [beautyMass.GetName()]
    dim = int(dim)
    if dim == 1:
        obsList = GeneralUtils.GetList(beautyMass)
    elif dim == 2:
        obsList = GeneralUtils.GetList(beautyMass, charmMass)
        obsName.append(charmMass.GetName())
    elif dim > 2:
        obsList = GeneralUtils.GetList(beautyMass, charmMass, bacPIDK)
        obsName.append(charmMass.GetName())
        obsName.append(bacPIDK.GetName())

    if debug:
        print("[INFO] Observables in the fit are: ")
        for obs in obsList:
            print(obs.GetName())
        print("")

###------------------------------------------------------------------------------------------------------------------------------------###
###------------------------------------------------------------------------------------------------------------------------------###
###------------------------------------------------------------------------------------------------------------------------------------###
    dim = int(dim)
    t = TString('_')

    decayTS = myconfigfile["Decay"]
    datasetTS = TString("dataSet") + decayTS + t
    if checkDuplications(year, merge):
        print(
            "----------------------------------------------------------------------------------------"
        )
        print(
            "[ERROR]: You try to merge twice the same data sample, please check --merge options: ",
            merge)
        print(
            "----------------------------------------------------------------------------------------"
        )
        exit(0)
    s, m, y = checkMerge(mode, sample, year, merge)

    if debug:
        print("-----------------------------")
        print("[INFO] Initial samples: ")
        print("-----------------------------")
    sm_init = getSampleModeYear(mode, sample, year, debug)
    if debug:
        print("-----------------------------")
        print("[INFO] Target samples: ")
        print("-----------------------------")
    sm = getSampleModeYear(m, s, y, debug)

    if debug:
        print("-----------------------------")
        print("[INFO] Merging options: ")
        print("-----------------------------")

    my = ""
    if merge != "":
        my = merge + year
        mergeList = getStdVector(my, debug)
    else:
        my = year
        mergeList = getStdVector(["none"], debug)
    sampleList = getStdVector(sample, False)
    modeList = getStdVector(mode, False)
    yearList = getStdVector(year, False)

    sam = RooCategory("sample", "sample")
    #print yearTS
    #    exit(0)

    data = []
    nEntries = []

    ### Obtain data set ###
    combData = GeneralUtils.GetDataSet(workData, observables, sam, datasetTS,
                                       sampleList, modeList, yearList, sm_init,
                                       sm, mergeList, debug)

    combData.Print('v')

    ranmode = m.__len__() * y.__len__()
    ransample = s.__len__()

    ###------------------------------------------------------------------------------------------------------------------------------------###
    ###-------------------------   Create the signal PDF in Bs mass, Ds mass, PIDK   ------------------------------------------###
    ###------------------------------------------------------------------------------------------------------------------------------------###

    from B2DXFitters.WS import WS
    workInt = RooWorkspace("workInt", "workInt")

    lumRatio = []
    lumY = ["2011", "2012", "2015", "2016", "2017", "2018"]
    MDSettings.DisableLum(yearList)
    lumi = MDSettings.GetLumRatio("year", debug)

    for i, y in enumerate(lumY):
        if (lumi[i] > 0.0  # only > 0 if lumi is present
                and y in
                year  # only add lumi variable for years given in command line
            ):
            name = "lumRatio_pol_" + y
            print("[INFO]", y, lumi[i])
            lumRatio.append(WS(workInt, RooRealVar(name, name, lumi[i])))

    if "run1" or "runs" in merge:
        lumi = MDSettings.GetLumRatio("run1", debug)
        for i, y in enumerate(["2011", "2012"]):
            if lumi[i] > 0.0:
                name = "lumRatio_run_" + y
                lumRatio.append(WS(workInt, RooRealVar(name, name, lumi[i])))

    if "run2" or "runs" in merge:
        lumi = MDSettings.GetLumRatio("run2", debug)
        lumi[0] = 0.164576  #ugly temporary fix
        for i, y in enumerate(["2015", "2016", "2017", "2018"]):
            if lumi[i] > 0.0:
                name = "lumRatio_run_" + y
                lumRatio.append(WS(workInt, RooRealVar(name, name, lumi[i])))

    if "runs" in merge:
        lumi = MDSettings.GetLumRatio("runs", debug)
        name = "lumRatio_runs"
        lumRatio.append(WS(workInt, RooRealVar(name, name, lumi[0])))

    workInt.Print("v")

    ###------------------------------------------------------------------------------------------------------------------------------------###
    ###-------------------------------   Create yields of all components     --------------------------------------###
    ###------------------------------------------------------------------------------------------------------------------------------------###
    evts = TString("Evts")

    nYields = []
    other = False
    signal = False
    combo = False

    for i in range(0, len(sm)):
        print(sm[i])
        yr = GeneralUtils.CheckDataYear(sm[i])
        mm = GeneralUtils.GetModeCapital(sm[i], debug)
        dmode = GeneralUtils.GetModeCapital(sm[i], debug)
        backgrounds = myconfigfile["Yields"]
        pol = GeneralUtils.CheckPolarityCapital(sm[i], debug)

        for bkg in backgrounds:
            if bkg != "Signal":
                nameBkg = TString("n") + bkg + t + sm[i] + t + evts
            else:
                nameBkg = TString("nSig") + t + sm[i] + t + evts
                signal = True
            if bkg == "CombBkg" or bkg == "Combinatorial":
                combo = True
            if bkg != "Signal" and bkg != "Combinatorial" and bkg != "CombBkg":
                other = True

            expectedYield = getExpectedYield(bkg, yr, dmode, pol, my,
                                             myconfigfile)
            nYields.append(
                RooRealVar(nameBkg.Data(), nameBkg.Data(), expectedYield, 0.0,
                           expectedYield * 2.0))
            setConstantIfSoConfigured(nYields[-1], "Yields", bkg, mm, pol,
                                      myconfigfile)
            getattr(workInt, 'import')(nYields[-1])

    ###------------------------------------------------------------------------------------------------------------------------------------###
    ###------------------   Create variables for all analytical shapes and get their types     -------------------------###
    ###------------------------------------------------------------------------------------------------------------------------------------###

    workInt = readVariablesForShapes(myconfigfile, workInt, obsName, merge,
                                     len(sm), sm, debug)
    types = getShapeTypes(myconfigfile, obsName, debug)

    ###------------------------------------------------------------------------------------------------------------------------------------###
    ###-------------------------------   Create the combo and signal PDF in Bs mass, Ds mass, PIDK --------------------------------------###
    ###------------------------------------------------------------------------------------------------------------------------------------###

    sigEPDF = []
    sig = "Signal_" + myconfigfile["Decay"]
    if signal:
        for i in range(0, len(sm)):
            mode = GeneralUtils.CheckDMode(sm[i], debug)
            if mode == "":
                mode = GeneralUtils.CheckKKPiMode(sm[i], debug)
            sigEPDF.append(
                Bs2Dsh2011TDAnaModels.buildExtendPdfMDFit(
                    workInt, workspace[0], obsList, types, sm[i], sig, mode,
                    mergeList, debug))

    combEPDF = []
    if combo:
        for i in range(0, len(sm)):
            mode = GeneralUtils.CheckDMode(sm[i], debug)
            if mode == "":
                mode = GeneralUtils.CheckKKPiMode(sm[i], debug)
            combEPDF.append(
                Bs2Dsh2011TDAnaModels.buildExtendPdfMDFit(
                    workInt, workspace[0], obsList, types, sm[i], "CombBkg",
                    mode, mergeList, debug))

    ###------------------------------------------------------------------------------------------------------------------------------------###
    ###-------------------------------   Gather Gaussian Constraints   --------------------------------------###
    ###------------------------------------------------------------------------------------------------------------------------------------###

    constraintPars = []
    if signal:
        constraintPars += collectGaussianConstraints(
            myconfigfile, "SignalShape", TString("Signal_BeautyMass"), workInt,
            merge, len(sm), obsName)
    if combo:
        constraintPars += collectGaussianConstraints(
            myconfigfile, "CombBkgShape", TString("CombBkg_BeautyMass"),
            workInt, merge, len(sm), obsName)

    constraintPars += collectGaussianConstraints(myconfigfile, "Yields",
                                                 TString("n"), workInt, merge,
                                                 len(sm), obsName)

    ###------------------------------------------------------------------------------------------------------------------------------------###
    ###-------------------------------   Create the total background PDF in Bs mass, Ds mass, PIDK ------------------------------###
    ###------------------------------------------------------------------------------------------------------------------------------------###

    addVar = []
    for a, year in list(myconfigfile.get('AdditionalParameters', {}).items()):
        for y, mode in list(year.items()):
            if y != "Fixed":
                for m, pol in list(mode.items()):
                    for p, param in list(pol.items()):
                        pp = GeneralUtils.CheckPolarity(TString(p), False)
                        yy = GeneralUtils.CheckDataYear(TString(y), False)
                        mm = GeneralUtils.CheckDMode(TString(m), False)
                        if mm == "":
                            mm = GeneralUtils.CheckKKPiMode(TString(m), debug)
                        if m == "All":
                            mm = "all"
                        t = TString("_")
                        ssmm = t + pp + t + mm + t + yy
                        name = TString(a) + ssmm
                        if "Range" in param:
                            addVar.append(
                                RooRealVar(name.Data(), name.Data(),
                                           param["CentralValue"],
                                           param["Range"][0],
                                           param["Range"][1]))
                        else:
                            addVar.append(
                                RooRealVar(name.Data(), name.Data(),
                                           param["CentralValue"]))
                        if year.get(
                                "Fixed"):  # == False if fixed is not present
                            addVar[-1].setConstant()
                            if debug:
                                print(
                                    "Parameter: {} set to be constant with value {}"
                                    .format(addVar[-1].GetName(),
                                            addVar[-1].getValV()))
                        getattr(workInt, 'import')(addVar[-1])

    if other == True:
        bkgShapes, bkgs = getBkgTypes(myconfigfile, decayTS, obsName, debug)
        bkgPDF = getTotalBkgPDF(
            myconfigfile, obsList, types, workspace[0], workInt, mergeList,
            len(sm), sm, bkgs, bkgShapes, debug)
    ###------------------------------------------------------------------------------------------------------------------------------------###
    ###---------------------------------   Create the total PDF in Bs mass, Ds mass, PIDK --------------------------------------###
    ###------------------------------------------------------------------------------------------------------------------------------------###

    N_Bkg_Tot = []
    listPDF = []
    totPDFp = []
    totPDFa = []
    for i in range(0, len(sm)):
        listPDF.append(RooArgList())
        if signal:
            listPDF[i].add(sigEPDF[i])
        if combo:
            listPDF[i].add(combEPDF[i])
        if other:
            listPDF[i].add(bkgPDF[i])
        name = TString("TotEPDF_m_") + sm[i]
        totPDFp.append(
            RooAddPdf(name.Data(), 'Model (signal & background) EPDF in mass',
                      listPDF[i]))

    totPDF = RooSimultaneous("simPdf", "simultaneous pdf", sam)
    for i in range(0, len(sm)):
        print(totPDFp[i].GetName())
        print(sm[i].Data())
        totPDF.addPdf(totPDFp[i], sm[i].Data())
    totPDF.Print("v")

    ###------------------------------------------------------------------------------------------------------###
    ###-----------------------------  Creating Gaussian Constraints  ----------------------------###
    ###------------------------------------------------------------------------------------------------------###

    import sys
    tempModel = RooWorkspace("tempModel", "tempModel")
    getattr(tempModel, 'import')(totPDF)
    tempModel.Print('v')

    constraintsSet = RooArgSet()
    for var in constraintPars:
        # Check if variable is being fitted
        modelVar = tempModel.var(str(var.GetName()))
        if modelVar != None:
            print(modelVar.GetName())
            if debug:
                print(
                    "[INFO] Constructing Gaussian Constraint for:   {}".format(
                        var.GetName()))
            gaussianName = var.GetName() + "_Gaussian"
            gaussianMean = RooConstVar(gaussianName + "Mean",
                                       gaussianName + "Mean", var.getValV())
            gaussianErr = RooConstVar(gaussianName + "Sigma",
                                      gaussianName + "Sigma", var.getError())
            Gaussian = RooGaussian(gaussianName, gaussianName, var,
                                   gaussianMean, gaussianErr)
            getattr(workInt, 'import')(Gaussian)
            constraintsSet.add(workInt.pdf(gaussianName))
    if debug and constraintsSet.getSize() > 0:
        print("[INFO] All GC variables: ")
        constraintsSet.Print("v")

    ###------------------------------------------------------------------------------------------------------------------------------------###
    ###--------------------------------------------  Instantiate and run the fitter  -------------------------------------------###
    ###------------------------------------------------------------------------------------------------------------------------------------###

    fitter = FitMeTool(debug)

    fitter.setObservables(observables)

    fitter.setModelPDF(totPDF)
    if binned:
        print("[INFO] Binned data does not work yet")
        #beautyMass.setBins(250)
        #charmMass.setBins(250)
        #bacPIDK.setBins(250)
        #combData_binned = RooDataHist("combData_binned","combData_binned",observables,combData)

    fitter.setData(combData)

    # Give the Gaussian contraints to the fitter
    fitter.setExternalConstraints(constraintsSet)

    plot_init = args.initvars and (args.wsname != None)
    plot_fitted = (not args.initvars) and (args.wsname != None)

    if plot_init:
        fitter.saveModelPDF(args.wsname)
        fitter.saveData(args.wsname)

    import sys
    import random

    fitter.fit(True, RooFit.Extended(True), RooFit.NumCPU(int(numCPU)),
               RooFit.Minimizer(args.minimizer),
               RooFit.ExternalConstraints(constraintsSet)
               # RooFit.Verbose(True)),
               # RooFit.InitialHesse(True),
               )
    #fitter.setData(combData)
    result = fitter.getFitResult()
    result.Print("v")
    floatpar = result.floatParsFinal()
    fitter.printTotalYields("*Evts")

    if plot_fitted:
        fitter.saveModelPDF(args.wsname)
        fitter.saveData(args.wsname)

    resultfile = TFile(
        args.wsname.replace('.root', '_fitresult.root'), 'RECREATE')
    result.Write()
    resultfile.Close()

    name = TString(sweightName)
    if sweight:
        RooMsgService.instance().Print('v')
        RooMsgService.instance().deleteStream(RooFit.Eval)
        fitter.savesWeights(
            beautyMass.GetName(),
            combData,
            name,
            False,  # do not save the refitted result to disk (?)
            False,  # do not explicitly set params to constant
            False,  # do not refit the already fitted PDF
            RooFit.NumCPU(int(numCPU)),
        )
        RooMsgService.instance().reset()

    rangeBeauty = []
    if TString(decayTS).Contains("Lb"):
        rangeBeauty.append(5619.58 - 50.0)
        rangeBeauty.append(5619.58 + 50.0)
    elif TString(decayTS).Contains("Bs"):
        rangeBeauty.append(5366.89 - 50.0)
        rangeBeauty.append(5366.89 + 50.0)
    elif TString(decayTS).Contains("Bd"):
        rangeBeauty.append(5279.63 - 50.0)
        rangeBeauty.append(5279.63 + 50.0)

    if dim == 1:
        fitter.printYieldsInRange('*Evts',
                                  MDSettings.GetMassBVarOutName().Data(),
                                  rangeBeauty[0], rangeBeauty[1])
    elif dim == 2:
        fitter.printYieldsInRange('*Evts',
                                  MDSettings.GetMassBVarOutName().Data(),
                                  rangeBeauty[0], rangeBeauty[1],
                                  "SignalRegion", charmMass.GetName(),
                                  charmMass.getMin(), charmMass.getMax())
    else:
        fitter.printYieldsInRange('*Evts',
                                  MDSettings.GetMassBVarOutName().Data(),
                                  rangeBeauty[0], rangeBeauty[1],
                                  "SignalRegion", charmMass.GetName(),
                                  charmMass.getMin(), charmMass.getMax(),
                                  bacPIDK.GetName(), bacPIDK.getMin(),
                                  bacPIDK.getMax())

    del fitter


#------------------------------------------------------------------------------
import argparse
parser = argparse.ArgumentParser()

parser.add_argument(
    '-d',
    '--debug',
    action='store_true',
    dest='debug',
    default=False,
    help='print debug information while processing')

parser.add_argument(
    '-s',
    '--save',
    dest='wsname',
    metavar='WSNAME',
    default='WS_MDFit_Results.root',
    help='save the model PDF and generated dataset to file "WS_WSNAME.root"')

parser.add_argument(
    '--sweightName',
    dest='sweightName',
    default='sWeights_Results.root',
    help='save the model PDF and generated dataset to file "WS_WSNAME.root"')

parser.add_argument(
    '--logoutputname',
    dest='logoutputname',
    default=
    '/afs/cern.ch/work/g/gligorov/public/Bs2DsKToys/sWeightToys/DsPi_Toys_Full_MassFitResult_0.log'
)

parser.add_argument(
    '-i',
    '--initial-vars',
    dest='initvars',
    action='store_true',
    default=False,
    help='save the model PDF parameters before the fit (default: after the fit)'
)
parser.add_argument(
    '-p',
    '--pol',
    '--polarity',
    dest='pol',
    nargs='+',
    default='down',
    help='Polarity can be: up, down, both ')
parser.add_argument(
    '-m',
    '--mode',
    dest='mode',
    nargs='+',
    default='kkpi',
    help='Mode can be: all, kkpi, kpipi, pipipi, nonres, kstk, phipi')

parser.add_argument(
    '-w',
    '--sweight',
    dest='sweight',
    action='store_true',
    default=False,
    help='create and save sWeights')

parser.add_argument(
    '--fileName',
    dest='fileNameAll',
    default='work_dsstk.root',
    help='name of the inputfile')

parser.add_argument(
    '--workName',
    dest='workName',
    default='workspace',
    help='name of the workspace')
parser.add_argument(
    '--configName',
    dest='configName',
    default='../data/Bs2DsK_3fbCPV/Bs2DsK/Bs2DsKConfigForNominalMassFit.py',
    help=
    "name of the configuration file, the full path to the file is mandatory")
parser.add_argument(
    '--wider',
    dest='wider',
    action='store_true',
    default=False,
    help='create and save sWeights')
parser.add_argument(
    '--merge',
    nargs='+',
    dest='merge',
    default="",
    help='merge can be: pol, run1, run2, runs')

parser.add_argument(
    '--dim',
    dest='dim',
    default=1,
    help=
    "Number of dimensionl of fit: 1 dim = beauty meson mass, 2 dim = beauty and charm meson mass, 3 dim == beauty and charm meson mass and pidk of bachelor"
)

parser.add_argument(
    '--fileData',
    dest='fileData',
    default='',
    help='you can use it if you have separate files with templates and data')
parser.add_argument(
    '--year',
    nargs='+',
    dest='year',
    default="",
    help='year of data taking can be: 2011, 2012, 2015, 2016, 2017, 2018')

parser.add_argument(
    '--binned',
    dest='binned',
    default=False,
    action='store_true',
    help='binned data Set')

parser.add_argument(
    '--jobs',
    default=4,
    type=int,
    help='''Set number of CPUs
                    to use for the MDFit.''')
parser.add_argument(
    '--minimizer',
    default='Minuit',
    type=str,
    help='''Set the
                    RooFit minimizer to use. See RooAbsPdf->fitTo()
                    Documentation for possible values. Defaults to
                    `Minuit`.''')

# -----------------------------------------------------------------------------

if __name__ == '__main__':
    try:
        args = parser.parse_args()
    except:
        parser.print_help()
        sys.exit(-1)

    if args.pol[0] == "both":
        args.pol = ["up", "down"]

    if args.year[0] == "run1":
        args.year = ["2011", "2012"]
    if args.year[0] == "run2":
        args.year = ["2015", "2016", "2017", "2018"]
    if args.year[0] == "all":
        args.year = ["2011", "2012", "2015", "2016", "2017", "2018"]

    print(vars(args))

    config = args.configName
    last = config.rfind("/")
    directory = config[:last + 1]
    configName = config[last + 1:]
    p = configName.rfind(".")
    configName = configName[:p]

    import sys
    sys.path.append(directory)

    runMDFitter(args.debug, args.pol, args.mode, args.sweight,
                args.fileNameAll, args.workName, args.sweightName, configName,
                args.wider, args.merge, args.dim, args.fileData, args.year,
                args.binned, args.jobs)

# -----------------------------------------------------------------------------
