###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# -----------------------------------------------------------------------------
# settings for running without GaudiPython
# -----------------------------------------------------------------------------
""":"
# This part is run by the shell. It does some setup which is convenient to save
# work in common use cases.

# make sure the environment is set up properly
if test -n "$CMTCONFIG" \
         -a -f $B2DXFITTERSROOT/$CMTCONFIG/libB2DXFittersDict.so \
         -a -f $B2DXFITTERSROOT/$CMTCONFIG/libB2DXFittersLib.so; then
    # all ok, software environment set up correctly, so don't need to do
    # anything
    true
else
    if test -n "$CMTCONFIG"; then
        # clean up incomplete LHCb software environment so we can run
        # standalone
        echo Cleaning up incomplete LHCb software environment.
        PYTHONPATH=`echo $PYTHONPATH | tr ':' '\n' | \
            egrep -v "^($User_release_area|$MYSITEROOT/lhcb)" | \
            tr '\n' ':' | sed -e 's/:$//'`
        export PYTHONPATH
        LD_LIBRARY_PATH=`echo $LD_LIBRARY_PATH | tr ':' '\n' | \
            egrep -v "^($User_release_area|$MYSITEROOT/lhcb)" | \
            tr '\n' ':' | sed -e 's/:$//'`
	export LD_LIBRARY_PATH
        exec env -u CMTCONFIG -u B2DXFITTERSROOT "$0" "$@"
    fi
    # automatic set up in standalone build mode
    if test -z "$B2DXFITTERSROOT"; then
        cwd="$(pwd)"
        if test -z "$(dirname $0)"; then
            # have to guess location of setup.sh
            cd ../standalone
            . ./setup.sh
            cd "$cwd"
        else
            # know where to look for setup.sh
            cd "$(dirname $0)"/../standalone
            . ./setup.sh
            cd "$cwd"
        fi
        unset cwd
    fi
fi
# figure out which custom allocators are available
# prefer jemalloc over tcmalloc
for i in libjemalloc libtcmalloc; do
    for j in `echo "$LD_LIBRARY_PATH" | tr ':' ' '` \
            /usr/local/lib /usr/lib /lib; do
        for k in `find "$j" -name "$i"'*.so.?' | sort -r`; do
            if test \! -e "$k"; then
                continue
            fi
            echo adding $k to LD_PRELOAD
            if test -z "$LD_PRELOAD"; then
                export LD_PRELOAD="$k"
                break 3
            else
                export LD_PRELOAD="$LD_PRELOAD":"$k"
                break 3
            fi
        done
    done
done
# set batch scheduling (if schedtool is available)
schedtool="`which schedtool 2>/dev/zero`"
if test -n "$schedtool" -a -x "$schedtool"; then
    echo "enabling batch scheduling for this job (schedtool -B)"
    schedtool="$schedtool -B -e"
else
    schedtool=""
fi

# set ulimit to protect against bugs which crash the machine: 2G vmem max,
# no more then 8M stack
ulimit -v $((2048 * 1024))
ulimit -s $((   8 * 1024))

# trampoline into python
exec $schedtool /usr/bin/time -v env python -O -- "$0" "$@"
"""
from __future__ import print_function
from __future__ import division

from past.utils import old_div
__doc__ = """ real docstring """
# -----------------------------------------------------------------------------
# Load necessary libraries
# -----------------------------------------------------------------------------
#"
from B2DXFitters import *
from ROOT import *

from ROOT import RooFit
from optparse import OptionParser

import math
from math import pi, log, sqrt

import os, sys, gc

import uncertainties
from uncertainties import ufloat

gROOT.SetBatch()

# -----------------------------------------------------------------------------
# Common input stuff and options
massfitdescr = ''
timefitdescr = ''
nickname = 'Bd2DPiMCFilteredS21RunIBothTaggedOnlyNominalStat'
corrplots = False  #produce 2D scatter plots
compareWithUnselected = False  #make plots for failed fits as well
logScale = False  #put y-axis in log scale
doFit = True  #run gaussian fit of distributions

if doFit:
    gStyle.SetOptStat(0)
    gStyle.SetOptFit(1011)

#Uncomment this for mass fit
#massfitdescr='FitB_FullMDFit'
#inputfile = '/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/Toys/'+nickname+'/'+'MDFit/PullTree'+massfitdescr+'_'+nickname+'.root'
#outputdir = '/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/Toys/'+nickname+'/MassPullsB/'+massfitdescr+'/'
#selection = 'CovQual == 3 && MINUITStatus == 0 && edm!=0'

#Uncomment this for time fit
#massfitdescr="NoMDFit"
#timefitdescr='SSbarAccAsymmFTFloatDMGammaConstrAllSamples'
#inputfile = '/eos/lhcb/wg/b2oc/TD_DPi_3fb/Toys/'+nickname+'/TimeFit/'+timefitdescr+'/PullTreeTimeFit_'+nickname+'_'+timefitdescr+'_'+massfitdescr+'.root'
#outputdir = '/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/Toys/'+nickname+'/TimePulls/'
#selection = 'MINUITStatus == 0 && edm!=0 && CovQual==3' #selection for converging fits

#Uncomment this for Bootstrap MC
timefitdescr = 'SSbarAccAsymmFTFloatDMGammaConstrAllSamples'
inputfile = '/eos/lhcb/wg/b2oc/TD_DPi_3fb/MCBootstrap/' + nickname + '/TimeFit/' + timefitdescr + '/PullTreeTimeFit_' + nickname + '_' + timefitdescr + '.root'
outputdir = '/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/MCBootstrap/' + nickname + '/TimePulls/' + timefitdescr + '/'
selection = 'MINUITStatus == 0 && edm!=0 && CovQual==3'

#Leave as it is
inputFile = TFile.Open(inputfile, "READ")
PullTree = inputFile.Get("PullTree")
LeafList = PullTree.GetListOfLeaves()

# -----------------------------------------------------------------------------
plotlabel = ''
if massfitdescr == "" and timefitdescr == "":
    print("ERROR: choose nicknames for mass or time fit!")
    exit(-1)
elif massfitdescr != "" and timefitdescr == "":
    plotlabel = massfitdescr
elif massfitdescr == "" and timefitdescr != "":
    plotlabel = timefitdescr
else:
    plotlabel = massfitdescr + "_" + timefitdescr

print("Plot label: " + plotlabel)

os.system("rm -r " + outputdir)  #be careful...
os.system("mkdir -p " + outputdir)


# -----------------------------------------------------------------------------
def makeprintout(canvas, name):
    # Set all the different types of plots to make
    plottypestomake = [".pdf"]
    for plottype in plottypestomake:
        canvas.Print(name + plottype)


# -----------------------------------------------------------------------------

GenList = []
FitList = []
ErrList = []
NameList = []
nLeaves = LeafList.GetEntries()
print("Leaf list size: ", nLeaves)
print("Loop over leaves:")
for leaf in range(0, nLeaves):
    name = TString(LeafList[leaf].GetName())
    print("Leaf: ", name.Data())
    if name.EndsWith("_gen"):
        GenList.append(name.Data())
    if name.EndsWith("_fit"):
        FitList.append(name.Data())
    if name.EndsWith("_err"):
        ErrList.append(name.Data())

# 3 leaves per observables, excluding the CovQual/MINUITStatus/edm leaves
nObs = (nLeaves - 3) / 3.0

NameList = []
for name in GenList:
    nametmp = TString(name)
    nametmp.ReplaceAll("_gen", "")
    NameList.append(nametmp.Data())

# Fill the histograms
for obs in range(0, int(nObs)):
    selection_string = selection
    print("Selection:")
    print(selection_string)
    plottitle = str(GenList[obs]).replace("_gen", "")

    print("Plotting:")
    print(FitList[obs])
    PullTree.Draw(FitList[obs] + ">>fitted_selected" + str(obs),
                  selection_string, "goff")
    fitted_selected = gDirectory.Get("fitted_selected" + str(obs))
    fitted_selected.SetTitle("")
    fitted_selected.GetXaxis().SetTitle("Fitted Value")
    #fitted_selected.SetLineColor(kBlack)

    print("Plotting:")
    print(ErrList[obs])
    PullTree.Draw(ErrList[obs] + ">>errf_selected" + str(obs),
                  selection_string, "goff")
    errf_selected = gDirectory.Get("errf_selected" + str(obs))
    errf_selected.SetTitle(plottitle)
    errf_selected.GetXaxis().SetTitle("Fitted Error")
    #errf_selected.SetLineColor(kBlack)

    print("Plotting:")
    print("(" + FitList[obs] + "-" + GenList[obs] + ")" + "/" + ErrList[obs])
    PullTree.Draw(
        "(" + FitList[obs] + "-" + GenList[obs] + ")" + "/" + ErrList[obs] +
        ">>pull_selected" + str(obs), selection_string, "goff")
    pull_selected = gDirectory.Get("pull_selected" + str(obs))
    pull_selected.SetTitle("")
    pull_selected.GetXaxis().SetTitle("Fitted Pull")
    #pull_selected.SetLineColor(kBlack)

    print("Plotting:")
    print("(" + FitList[obs] + "-" + GenList[obs] + ")")

    PullTree.Draw(
        "(" + FitList[obs] + "-" + GenList[obs] + ")" + ">>residual_selected" +
        str(obs), selection_string, "goff")
    residual_selected = gDirectory.Get("residual_selected" + str(obs))
    residual_selected.SetTitle("")
    residual_selected.GetXaxis().SetTitle("Fitted Residual")
    #residual_selected.SetLineColor(kBlack)

    if compareWithUnselected:

        #Normalise previous plots
        fitted_selected.Scale(1.0 / fitted_selected.Integral("width"))
        errf_selected.Scale(1.0 / errf_selected.Integral("width"))
        pull_selected.Scale(1.0 / pull_selected.Integral("width"))
        residual_selected.Scale(1.0 / residual_selected.Integral("width"))

        #Produce "uselected" (failed) plots as well
        unselection_string = "!(" + selection + ")"
        print("(Anti)selection:")
        print(unselection_string)
        plottitle = str(GenList[obs]).replace("_gen", "")

        print("Plotting:")
        print(FitList[obs])
        PullTree.Draw(FitList[obs] + ">>fitted_unselected" + str(obs),
                      unselection_string, "goff")
        fitted_unselected = gDirectory.Get("fitted_unselected" + str(obs))
        fitted_unselected.SetTitle("")
        fitted_unselected.GetXaxis().SetTitle("Fitted Value")
        fitted_unselected.SetLineColor(kBlack)
        fitted_min = min(fitted_selected.GetXaxis().GetBinLowEdge(1),
                         fitted_unselected.GetXaxis().GetBinLowEdge(1))
        fitted_max = max(
            fitted_selected.GetXaxis().GetBinLowEdge(
                fitted_selected.GetNbinsX()) +
            fitted_selected.GetXaxis().GetBinWidth(
                fitted_selected.GetNbinsX()),
            fitted_selected.GetXaxis().GetBinLowEdge(
                fitted_unselected.GetNbinsX()) +
            fitted_unselected.GetXaxis().GetBinWidth(
                fitted_unselected.GetNbinsX()))

        print("Plotting:")
        print(ErrList[obs])
        PullTree.Draw(ErrList[obs] + ">>errf_unselected" + str(obs),
                      unselection_string, "goff")
        errf_unselected = gDirectory.Get("errf_unselected" + str(obs))
        errf_unselected.SetTitle(plottitle)
        errf_unselected.GetXaxis().SetTitle("Fitted Error")
        errf_unselected.SetLineColor(kBlack)
        errf_min = min(errf_selected.GetXaxis().GetBinLowEdge(1),
                       errf_unselected.GetXaxis().GetBinLowEdge(1))
        errf_max = max(
            errf_selected.GetXaxis().GetBinLowEdge(errf_selected.GetNbinsX()) +
            errf_selected.GetXaxis().GetBinWidth(errf_selected.GetNbinsX()),
            errf_selected.GetXaxis().GetBinLowEdge(errf_unselected.GetNbinsX())
            + errf_unselected.GetXaxis().GetBinWidth(
                errf_unselected.GetNbinsX()))

        print("Plotting:")
        print("(" + FitList[obs] + "-" + GenList[obs] + ")" + "/" +
              ErrList[obs])
        PullTree.Draw(
            "(" + FitList[obs] + "-" + GenList[obs] + ")" + "/" + ErrList[obs]
            + ">>pull_unselected" + str(obs), unselection_string, "goff")
        pull_unselected = gDirectory.Get("pull_unselected" + str(obs))
        pull_unselected.SetTitle("")
        pull_unselected.GetXaxis().SetTitle("Fitted Pull")
        pull_unselected.SetLineColor(kBlack)
        pull_min = min(pull_selected.GetXaxis().GetBinLowEdge(1),
                       pull_unselected.GetXaxis().GetBinLowEdge(1))
        pull_max = max(
            pull_selected.GetXaxis().GetBinLowEdge(pull_selected.GetNbinsX()) +
            pull_selected.GetXaxis().GetBinWidth(pull_selected.GetNbinsX()),
            pull_selected.GetXaxis().GetBinLowEdge(pull_unselected.GetNbinsX())
            + pull_unselected.GetXaxis().GetBinWidth(
                pull_unselected.GetNbinsX()))

        print("Plotting:")
        print("(" + FitList[obs] + "-" + GenList[obs] + ")")
        PullTree.Draw(
            "(" + FitList[obs] + "-" + GenList[obs] + ")" +
            ">>residual_unselected" + str(obs), unselection_string, "goff")
        residual_unselected = gDirectory.Get("residual_unselected" + str(obs))
        residual_unselected.SetTitle("")
        residual_unselected.GetXaxis().SetTitle("Fitted Residual")
        residual_unselected.SetLineColor(kBlack)
        residual_min = min(residual_selected.GetXaxis().GetBinLowEdge(1),
                           residual_unselected.GetXaxis().GetBinLowEdge(1))
        residual_max = max(
            residual_selected.GetXaxis().GetBinLowEdge(
                residual_selected.GetNbinsX()) +
            residual_selected.GetXaxis().GetBinWidth(
                residual_selected.GetNbinsX()),
            residual_selected.GetXaxis().GetBinLowEdge(
                residual_unselected.GetNbinsX()) +
            residual_unselected.GetXaxis().GetBinWidth(
                residual_unselected.GetNbinsX()))

        #Normalise new plots as well
        fitted_unselected.Scale(1.0 / fitted_unselected.Integral("width"))
        errf_unselected.Scale(1.0 / errf_unselected.Integral("width"))
        pull_unselected.Scale(1.0 / pull_unselected.Integral("width"))
        residual_unselected.Scale(1.0 / residual_unselected.Integral("width"))

    #gStyle.SetStatX(0.95)
    #gStyle.SetStatY(0.95)
    #gStyle.SetStatW(0.15)
    #gStyle.SetStatH(0.15)

    pullcanvas = TCanvas("pullcanvas" + str(obs), plottitle, 5000, 1500)
    pullcanvas.Divide(4, 1)

    pullcanvas.cd(1)
    if logScale:
        pullcanvas.cd(1).SetLogy()
    if doFit:
        fitted_selected.Fit("gaus", "LM")
        #fitted_selected.FindObject("gaus").SetLineColor(kBlack)
    fitted_selected.Draw("PE")
    if compareWithUnselected:
        fitted_selected.GetXaxis().SetRangeUser(fitted_min, fitted_max)
        fitted_unselected.Draw("PESAME")

    pullcanvas.cd(2)
    if logScale:
        pullcanvas.cd(2).SetLogy()
    if doFit:
        errf_selected.Fit("gaus", "LM")
        #errf_selected.FindObject("gaus").SetLineColor(kBlack)
    errf_selected.Draw("PE")
    if compareWithUnselected:
        errf_selected.GetXaxis().SetRangeUser(errf_min, errf_max)
        errf_unselected.Draw("PESAME")

    pullcanvas.cd(3)
    if logScale:
        pullcanvas.cd(3).SetLogy()
    if doFit:
        pull_selected.Fit("gaus", "LM")
        #pull_selected.FindObject("gaus").SetLineColor(kBlack)
    pull_selected.Draw("PE")
    if compareWithUnselected:
        pull_selected.GetXaxis().SetRangeUser(pull_min, pull_max)
        pull_unselected.Draw("PESAME")

    pullcanvas.cd(4)
    if logScale:
        pullcanvas.cd(4).SetLogy()
    if doFit:
        residual_selected.Fit("gaus", "LM")
        #residual_selected.FindObject("gaus").SetLineColor(kBlack)
    residual_selected.Draw("PE")
    if compareWithUnselected:
        residual_selected.GetXaxis().SetRangeUser(residual_min, residual_max)
        residual_unselected.Draw("PESAME")

    makeprintout(pullcanvas,
                 outputdir + "1DPullPlot_" + NameList[obs] + "_" + plotlabel)

if corrplots:

    gStyle.SetOptStat(0)

    #Now plots all 2D correlation plots between pairs of fitted variables
    AllList = FitList + ErrList

    for obs1 in range(0, AllList.__len__()):

        if obs1 == AllList.__len__() - 1:
            break

        for obs2 in range(obs1 + 1, AllList.__len__()):
            selection_string = selection
            print("Selection:")
            print(selection_string)

            print("Plotting " + AllList[obs1] + " vs " + AllList[obs2])
            PullTree.Draw(
                AllList[obs1] + ":" + AllList[obs2] + ">>corr" + str(obs1) +
                str(obs2), selection_string, "goff")
            corrplot = gDirectory.Get("corr" + str(obs1) + str(obs2))
            corrplot.SetName("")
            corrplot.SetTitle("Correlation = " + str("{0:.3f}".format(
                corrplot.GetCorrelationFactor())))
            corrplot.GetXaxis().SetTitle(AllList[obs2])
            corrplot.GetYaxis().SetTitle(AllList[obs1])

            pullcanvas = TCanvas("pullcanvas" + str(obs), "pullcanvas")
            pullcanvas.cd()
            corrplot.Draw("CONTZ")
            makeprintout(
                pullcanvas, outputdir + "CorrPlot_" + AllList[obs1] + "_vs_" +
                AllList[obs2] + "_" + plotlabel)

ntoys = ufloat(PullTree.GetEntries(), sqrt(PullTree.GetEntries()))
nfailed = ufloat(
    PullTree.GetEntries("!(" + selection + ")"),
    sqrt(PullTree.GetEntries("!(" + selection + ")")))
print("Selection for failed toys:")
print("!(" + selection + ")")
print("Number of toys:")
print(ntoys)
print("Number of failed toys:")
print(nfailed)
print("Fraction of failed toys:")
print(old_div(nfailed, ntoys))

inputFile.Close()
